package com.fashion.fashionhouse.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.ImagesModel;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;

public class ViewPagerImages extends PagerAdapter {
    Context mContext;
    private final OnItemClickListener listener;
    private ArrayList<ImagesModel> imagesModelArrayList;

    public ViewPagerImages(Context context, ArrayList<ImagesModel> imagesModelArrayList, OnItemClickListener listener) {
        this.mContext = context;
        this.imagesModelArrayList = imagesModelArrayList;
        this.listener = listener;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.view_pager_images, container, false);

        ImageView image = layout.findViewById(R.id.image);
        ImageView download = layout.findViewById(R.id.download);

        container.addView(layout);

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });





        Glide.with(mContext).load(imagesModelArrayList.get(position).getProductImageName()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(image);


        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imagesModelArrayList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}
