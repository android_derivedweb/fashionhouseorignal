package com.fashion.fashionhouse.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouse.R;


public class AdapterRecommended extends RecyclerView.Adapter<AdapterRecommended.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    public AdapterRecommended(Context mContext, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_recommended, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public class Viewholder extends RecyclerView.ViewHolder {



        public Viewholder(@NonNull View itemView) {
            super(itemView);


        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}