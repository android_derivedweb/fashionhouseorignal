package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterNewIn extends RecyclerView.Adapter<AdapterNewIn.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<ProductListModel> productListModelArrayListTrending;

    public AdapterNewIn(Context mContext, ArrayList<ProductListModel> productListModelArrayListTrending, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.productListModelArrayListTrending = productListModelArrayListTrending;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_new_in, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        Glide.with(mContext).load(productListModelArrayListTrending.get(position).getProductImage()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(holder.image);

    //    holder.brandName.setText(productListModelArrayListTrending.get(position).getBrandName());
        holder.subName.setText(productListModelArrayListTrending.get(position).getProductTitle());




    }

    @Override
    public int getItemCount() {
        return productListModelArrayListTrending.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {


        ImageView image;
        TextView brandName, subName;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            brandName = itemView.findViewById(R.id.brandName);
            subName = itemView.findViewById(R.id.subName);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}