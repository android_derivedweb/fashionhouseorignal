package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterWishlist extends RecyclerView.Adapter<AdapterWishlist.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<ProductListModel> productListModelArrayList;


    public AdapterWishlist(Context mContext, ArrayList<ProductListModel> productListModelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.productListModelArrayList = productListModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_wishlist, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClickRemove(position);
            }
        });


        Glide.with(mContext).load(productListModelArrayList.get(position).getProductImage()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(holder.image);

        holder.productTitle.setText(productListModelArrayList.get(position).getProductTitle());
        holder.price.setText(mContext.getResources().getString(R.string.currency_symbol) + productListModelArrayList.get(position).getPrice());


        if (productListModelArrayList.get(position).getDisCount().equals("0")){
            holder.discountText.setVisibility(View.GONE);
        } else {
            holder.discountText.setVisibility(View.VISIBLE);
            holder.discountText.setTextColor(mContext.getResources().getColor(R.color.red));
        }
        holder.discountText.setText("(" + productListModelArrayList.get(position).getDisCount() + "% OFF)");



        if (productListModelArrayList.get(position).getIsNewIn().equals("1") && productListModelArrayList.get(position).getIsSale().equals("1")){
            holder.statusLayout.setVisibility(View.VISIBLE);
            holder.newIn.setVisibility(View.VISIBLE);
            holder.Sale.setVisibility(View.VISIBLE);
            holder.as.setVisibility(View.VISIBLE);
        } else if (productListModelArrayList.get(position).getIsNewIn().equals("1")){
            holder.statusLayout.setVisibility(View.VISIBLE);
            holder.newIn.setVisibility(View.VISIBLE);
            holder.Sale.setVisibility(View.GONE);
            holder.as.setVisibility(View.GONE);
        } else if (productListModelArrayList.get(position).getIsSale().equals("1")){
            holder.statusLayout.setVisibility(View.VISIBLE);
            holder.newIn.setVisibility(View.GONE);
            holder.Sale.setVisibility(View.VISIBLE);
            holder.as.setVisibility(View.GONE);
        } else if (productListModelArrayList.get(position).getIsNewIn().equals("0") && productListModelArrayList.get(position).getIsSale().equals("0")){
            holder.statusLayout.setVisibility(View.INVISIBLE);
            holder.newIn.setVisibility(View.INVISIBLE);
            holder.Sale.setVisibility(View.INVISIBLE);
            holder.as.setVisibility(View.INVISIBLE);
        }


    }

    @Override
    public int getItemCount() {
        return productListModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView image, close;
        TextView productTitle, price, discountText;
        RelativeLayout statusLayout;
        TextView newIn, Sale;
        View as;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            productTitle = itemView.findViewById(R.id.productTitle);
            price = itemView.findViewById(R.id.price);
            discountText = itemView.findViewById(R.id.discountText);
            close = itemView.findViewById(R.id.close);
            statusLayout = itemView.findViewById(R.id.statusLayout);
            newIn = itemView.findViewById(R.id.newIn);
            Sale = itemView.findViewById(R.id.Sale);
            as = itemView.findViewById(R.id.as);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
        void onItemClickRemove(int item);
    }



}