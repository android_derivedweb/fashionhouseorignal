package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouse.Model.InterestedModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterSearchInner extends RecyclerView.Adapter<AdapterSearchInner.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<InterestedModel> interestedModelArrayList;


    public AdapterSearchInner(Context mContext, ArrayList<InterestedModel> interestedModelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.interestedModelArrayList = interestedModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_search_inner, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.textSearch.setText(interestedModelArrayList.get(position).getCategoryName());

    }

    @Override
    public int getItemCount() {
        return interestedModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView textSearch;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            textSearch = itemView.findViewById(R.id.textSearch);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}