package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouse.R;


public class AdapterDesignerSub extends RecyclerView.Adapter<AdapterDesignerSub.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private int positionMain;

    private String[] a = {"A BATHING APE", "A Angelo", "A Kind of Guide", "Acne Studios", "Adidas", "Adam Lippes"};
    private String[] b = {"Backes & Strauss", "Balenciaga", "Ballantyne", "Bally", "Bang & Olufsen", "Barbour"};
    private String[] c = {"Caban", "Calvin Klein", "Camper", "Canali", "Car Shoe", "Carrera"};
    private String[] d = {"Damir Doma", "Daniel Patrick", "Danner", "Danton", "Desa 1972", "Destin"};
    private String[] e = {"Eden Power Corp", "Eight & Bob", "Elie Saab", "Enterprise Japan", "Eton", "Ewing"};
    private String[] f = {"Favre Leuba", "Fay Kids", "Fendi", "Fila", "Filson", "Folk"};


    public AdapterDesignerSub(Context mContext, int positionMain, OnItemClickListener listener) {
        this.listener = listener;
        this.positionMain = positionMain;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_designers_sub, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        if (positionMain == 0) {
            holder.subDesigner.setText(a[position]);
        } else if (positionMain == 1){
            holder.subDesigner.setText(b[position]);
        } else if (positionMain == 2){
            holder.subDesigner.setText(c[position]);
        } else if (positionMain == 3){
            holder.subDesigner.setText(d[position]);
        } else if (positionMain == 4){
            holder.subDesigner.setText(e[position]);
        } else if (positionMain == 5){
            holder.subDesigner.setText(f[position]);
        }


    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView subDesigner;

        public Viewholder(@NonNull View itemView) {
            super(itemView);


            subDesigner = itemView.findViewById(R.id.subDesigner);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}