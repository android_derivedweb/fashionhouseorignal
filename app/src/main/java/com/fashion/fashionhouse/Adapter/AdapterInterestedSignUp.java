package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouse.Model.InterestedModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterInterestedSignUp extends RecyclerView.Adapter<AdapterInterestedSignUp.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    private ArrayList<InterestedModel> modelArrayList;

    private RadioButton lastCheckedRB = null;

    private int lastSelectedPosition = -1;


    public AdapterInterestedSignUp(Context mContext, ArrayList<InterestedModel> modelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.modelArrayList = modelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_interested_sign_up, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


      /*  holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });*/

       holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               listener.onItemClick(position,isChecked);
           }
       });


      //  holder.radioButton.setChecked(lastSelectedPosition == position);

        holder.typeText.setText(modelArrayList.get(position).getCategoryName());


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        CheckBox radioButton;
        TextView typeText;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            radioButton = itemView.findViewById(R.id.isintrestedCheck);
            typeText = itemView.findViewById(R.id.typeText);


        }
    }


    public interface OnItemClickListener {
        void onItemClick(int item, boolean isChecked);
    }
}