package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouse.Model.ShippingAddressModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterShippingAddress extends RecyclerView.Adapter<AdapterShippingAddress.Viewholder> {

    private final OnItemClickListener mListener;
    private Context mContext;
    private ArrayList<ShippingAddressModel> shippingAddressModelArrayList;
    private int selectedPosition = -1;



    public AdapterShippingAddress(Context mContext, ArrayList<ShippingAddressModel> shippingAddressModelArrayList, OnItemClickListener mListener) {
        this.mListener = mListener;
        this.mContext = mContext;
        this.shippingAddressModelArrayList = shippingAddressModelArrayList;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_shipping_address, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        String nearBy = "";

        if (shippingAddressModelArrayList.get(position).getShippingPinCode().equals("null")){
            nearBy = "";
        } else {
            nearBy = ", " + shippingAddressModelArrayList.get(position).getShippingPinCode();
        }

        holder.shippingAddress.setText(shippingAddressModelArrayList.get(position).getFirstName()+" "
                + shippingAddressModelArrayList.get(position).getLastName()
                + ", \n" + shippingAddressModelArrayList.get(position).getShippingHouseNo()
                + ", " + shippingAddressModelArrayList.get(position).getShippingAddress()
                + ", \n" + shippingAddressModelArrayList.get(position).getShippingCity()
                + nearBy
                + ", \n"+ shippingAddressModelArrayList.get(position).getShippingState()
                + ", " + shippingAddressModelArrayList.get(position).getShippingCountry());


        holder.shippingPhoneNo.setText(shippingAddressModelArrayList.get(position).getMobileNumber());


        holder.removeShippingAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClickRemove(position,Integer.parseInt(shippingAddressModelArrayList.get(position).getShippingAddressID()));
            }
        });



        holder.radioMyAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClickRadio(Integer.parseInt(shippingAddressModelArrayList.get(position).getShippingAddressID()));
                notifyDataSetChanged();

            }
        });


        holder.editShippingAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClickEdit(Integer.parseInt(shippingAddressModelArrayList.get(position).getShippingAddressID()));

            }
        });


        if (shippingAddressModelArrayList.get(position).getIsDefault().equals("1")){
            holder.radioMyAddress.setChecked(true);
        } else if (shippingAddressModelArrayList.get(position).getIsDefault().equals("0")){
            holder.radioMyAddress.setChecked(false);
        }




    }



    @Override
    public int getItemCount() {
        return shippingAddressModelArrayList.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder {


        TextView shippingPhoneNo, shippingAddress, removeShippingAddress, editShippingAddress;
        AppCompatRadioButton radioMyAddress;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            shippingAddress = itemView.findViewById(R.id.shippingAddress);
            shippingPhoneNo = itemView.findViewById(R.id.shippingPhoneNo);
            removeShippingAddress = itemView.findViewById(R.id.removeShippingAddress);
            editShippingAddress = itemView.findViewById(R.id.editShippingAddress);
            radioMyAddress = itemView.findViewById(R.id.radioMyAddress);

        }
    }



    public interface OnItemClickListener {
        void onItemClickRemove(int position, int item);
        void onItemClickEdit(int item);
        void onItemClickRadio(int item);
    }





}