package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.Product;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;


import java.util.ArrayList;

public class SubProductAdapter extends RecyclerView.Adapter<SubProductAdapter.MyViewHolder> {

    private final OnItemClickListener listener;
    private ArrayList<Product> mDataset;
    private UserSession userSession;
    private Context context;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView img;
        public TextView title,date,price, Qty;


        public MyViewHolder(View v) {
            super(v);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.date = (TextView) itemView.findViewById(R.id.date);
            this.price = (TextView) itemView.findViewById(R.id.price);
            this.Qty = (TextView) itemView.findViewById(R.id.Qty);
            this.img = (ImageView) itemView.findViewById(R.id.img);
        }
    }

    public SubProductAdapter(Context context, ArrayList<Product> categoryModels, OnItemClickListener listener) {
        mDataset = categoryModels;
        this.listener = listener;
        this.context = context;
        this.userSession = new UserSession(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.item_categories_order, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.title.setText(mDataset.get(position).getProductTitleEn());
        holder.Qty.setText("Quantity : " + mDataset.get(position).getQty());
        holder.price.setText(context.getResources().getString(R.string.currency_symbol) +" " +mDataset.get(position).getPrice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });

        Glide.with(holder.img.getContext()).load(mDataset.get(position).getProductImage()).placeholder(context.getResources().getDrawable(R.drawable.fashionhouse)).into(holder.img);

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
