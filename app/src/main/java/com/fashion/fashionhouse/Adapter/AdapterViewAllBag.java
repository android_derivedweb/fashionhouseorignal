package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.CategoryDetailsModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterViewAllBag extends RecyclerView.Adapter<AdapterViewAllBag.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<CategoryDetailsModel> imageArray;


    public AdapterViewAllBag(Context mContext, ArrayList<CategoryDetailsModel> imageArray, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.imageArray = imageArray;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_view_bag_all, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        Glide.with(mContext).load(imageArray.get(position).getDeal_image()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(holder.image);



    }

    @Override
    public int getItemCount() {
        return imageArray.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView image;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}