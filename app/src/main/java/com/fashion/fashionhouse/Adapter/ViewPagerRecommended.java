package com.fashion.fashionhouse.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.Model.SizeColorModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;

public class ViewPagerRecommended extends PagerAdapter {
    Context mContext;
    private final OnItemClickListener listener;
    private ArrayList<ProductListModel> productListModelArrayList;

    public ViewPagerRecommended(Context context, ArrayList<ProductListModel> productListModelArrayList, OnItemClickListener listener) {
        this.mContext = context;
        this.productListModelArrayList = productListModelArrayList;
        this.listener = listener;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.view_pager_home, container, false);


        container.addView(layout);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });



        TextView title = layout.findViewById(R.id.title);
        ImageView image = layout.findViewById(R.id.image);

        title.setText(productListModelArrayList.get(position).getProductTitle());
        Glide.with(mContext).load(productListModelArrayList.get(position).getProductImage()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(image);


        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return productListModelArrayList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}
