package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouse.Model.InterestedModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterMainCategory extends RecyclerView.Adapter<AdapterMainCategory.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<InterestedModel> modelArrayList;

    private int index = 0;


    public AdapterMainCategory(Context mContext, ArrayList<InterestedModel> modelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.modelArrayList = modelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_main_category, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        if(index==position){
            holder.categoryLayout.setBackground(mContext.getResources().getDrawable(R.drawable.darkgray_border_1dp));
        }else {
            holder.categoryLayout.setBackground(null);
        }



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.categoryName.setText(modelArrayList.get(position).getCategoryName());


    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView categoryName;
        LinearLayout categoryLayout;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            categoryName = itemView.findViewById(R.id.categoryName);
            categoryLayout = itemView.findViewById(R.id.categoryLayout);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


    public void filterList(int pos) {
        index = pos;
        notifyDataSetChanged();
    }


}