package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterHorizontalHome extends RecyclerView.Adapter<AdapterHorizontalHome.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<ProductListModel> productListModelArrayListRecent;


    public AdapterHorizontalHome(Context mContext, ArrayList<ProductListModel> productListModelArrayListRecent, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.productListModelArrayListRecent = productListModelArrayListRecent;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_horizontal_home, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


    //    holder.brandName.setText(productListModelArrayListRecent.get(position).getBrandName());
        holder.productTitle.setText(productListModelArrayListRecent.get(position).getProductTitle());

        Glide.with(mContext).load(productListModelArrayListRecent.get(position).getProductImage()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return productListModelArrayListRecent.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder {


        ImageView image;
        TextView brandName, productTitle;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            brandName = itemView.findViewById(R.id.brandName);
            productTitle = itemView.findViewById(R.id.productTitle);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}