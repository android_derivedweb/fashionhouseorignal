package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterSearchList extends RecyclerView.Adapter<AdapterSearchList.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<String> TitleArray;


    public AdapterSearchList(Context mContext, ArrayList<String> TitleArray, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.TitleArray = TitleArray;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_search_list, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        
        holder.title.setText(TitleArray.get(position));




    }

    @Override
    public int getItemCount() {
        return TitleArray.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        
        TextView title;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            
            title = itemView.findViewById(R.id.title);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}