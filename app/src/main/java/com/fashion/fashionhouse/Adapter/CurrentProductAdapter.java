package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouse.Model.OrderModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;


import java.util.ArrayList;

public class CurrentProductAdapter extends RecyclerView.Adapter<CurrentProductAdapter.MyViewHolder> {

    private final OnItemClickListener listener;
    private final Context mContext;
    private final UserSession userSession;
    private ArrayList<OrderModel> mDataset;
    private SubProductAdapter mAdapter;
    private int Pos = 0;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView order_num,price,date, status_order;
        RecyclerView category_view;
        ImageView info;

        public MyViewHolder(View v) {
            super(v);
            this.order_num = (TextView) itemView.findViewById(R.id.order_num);
            this.price = (TextView) itemView.findViewById(R.id.price);
            this.date = (TextView) itemView.findViewById(R.id.date);
            this.status_order = (TextView) itemView.findViewById(R.id.status_order);
            this.info = (ImageView) itemView.findViewById(R.id.info);
            category_view =(RecyclerView) itemView.findViewById(R.id.category_view);
        }

    }

    public CurrentProductAdapter(Context ProductContext, ArrayList<OrderModel> categoryModels, OnItemClickListener listener) {
        mDataset = categoryModels;
        this.listener = listener;
        this.mContext = ProductContext;
        userSession = new UserSession(ProductContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.item_mangeorder_time, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        Pos = position;
        holder.order_num.setText("Order Code : "+mDataset.get(position).getOrderCode());
        holder.price.setText(mContext.getResources().getString(R.string.currency_symbol)+" "+mDataset.get(position).getTotalAmount());
        holder.date.setText(mDataset.get(position).getDate());
        holder.status_order.setText("Status : " + mDataset.get(position).getOrderStatus());


        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onItemClick(position, v);

            }
        });



        mAdapter = new SubProductAdapter(mContext,mDataset.get(position).getProducts(), new SubProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                listener.onItemClickSUB(position,item);

            }
        });
        holder.category_view.setHasFixedSize(true);
        holder.category_view.setLayoutManager(new LinearLayoutManager(holder.category_view.getContext(), LinearLayoutManager.VERTICAL, false));
        holder.category_view.setAdapter(mAdapter);
        holder.category_view.setNestedScrollingEnabled(false);


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item, View v);
        void onItemClickSUB(int position, int item);
    }


}
