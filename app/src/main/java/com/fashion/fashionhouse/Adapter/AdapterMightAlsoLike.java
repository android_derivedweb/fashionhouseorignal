package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterMightAlsoLike extends RecyclerView.Adapter<AdapterMightAlsoLike.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<ProductListModel> productListModelArrayList;

    public AdapterMightAlsoLike(Context mContext, ArrayList<ProductListModel> productListModelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.productListModelArrayList = productListModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_might_also_like, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.price.setText(mContext.getResources().getString(R.string.currency_symbol) + productListModelArrayList.get(position).getPrice());
        holder.productTitle.setText(productListModelArrayList.get(position).getProductTitle());

        Glide.with(mContext).load(productListModelArrayList.get(position).getProductImage()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(holder.image);


        if (productListModelArrayList.get(position).isWishlist()){
            holder.wishList.setBackgroundResource(R.drawable.heart_fill);
        } else {
            holder.wishList.setBackgroundResource(R.drawable.heart_blank);
        }

        holder.wishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (productListModelArrayList.get(position).isWishlist()){
                    holder.wishList.setBackgroundResource(R.drawable.heart_blank);
                    productListModelArrayList.get(position).setWishlist(false);
                } else {
                    holder.wishList.setBackgroundResource(R.drawable.heart_fill);
                    productListModelArrayList.get(position).setWishlist(true);
                }

                listener.onItemClickWishList(position);

            }
        });




    }

    @Override
    public int getItemCount() {
        return productListModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView image, wishList;
        TextView price, productTitle;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            wishList = itemView.findViewById(R.id.wishList);
            price = itemView.findViewById(R.id.Price);
            productTitle = itemView.findViewById(R.id.ProductTitle);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
        void onItemClickWishList(int item);

    }
}