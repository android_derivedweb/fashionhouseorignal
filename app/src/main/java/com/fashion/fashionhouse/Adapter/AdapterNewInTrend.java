package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterNewInTrend extends RecyclerView.Adapter<AdapterNewInTrend.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<ProductListModel> productListModelArrayListTrending;


    public AdapterNewInTrend(Context mContext, ArrayList<ProductListModel> productListModelArrayListTrending, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.productListModelArrayListTrending = productListModelArrayListTrending;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_new_in_rec, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });



        if(productListModelArrayListTrending.get(position).getProductType().equals("video")){
            holder.video_view.setVideoURI(Uri.parse(productListModelArrayListTrending.get(position).getProductImage()));
            holder.video_view.start();
            holder.video_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setVolume(0f, 0f);
                    mp.setLooping(true);
                }
            });            holder.Image_view.setVisibility(View.GONE);
            holder.video_view.setVisibility(View.VISIBLE);
        }else {
            holder.Image_view.setVisibility(View.VISIBLE);
            holder.video_view.setVisibility(View.GONE);
            Glide.with(mContext).load(productListModelArrayListTrending.get(position).getProductImage()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(holder.Image_view);

        }





    }

    @Override
    public int getItemCount() {
        return productListModelArrayListTrending.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {


        VideoView video_view;
        ImageView Image_view;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            video_view = itemView.findViewById(R.id.video_view);
            Image_view = itemView.findViewById(R.id.image_view);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}