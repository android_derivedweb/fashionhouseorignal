package com.fashion.fashionhouse.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Model.CategoryDetailsModel;
import com.fashion.fashionhouse.R;

import java.util.ArrayList;


public class AdapterBag extends RecyclerView.Adapter<AdapterBag.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<CategoryDetailsModel> detailsModelArrayList;

    private int quantity = 1;


    public AdapterBag(Context mContext, ArrayList<CategoryDetailsModel> detailsModelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.detailsModelArrayList = detailsModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_bag, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.title.setText(detailsModelArrayList.get(position).getTitle());
        holder.price.setText(mContext.getResources().getString(R.string.currency_symbol) + detailsModelArrayList.get(position).getSell_price());

        if (detailsModelArrayList.get(position).getSizeItem().equals("") && detailsModelArrayList.get(position).getColorItem().equals("")){
            holder.typeOfProduct.setVisibility(View.GONE);
        } else if (detailsModelArrayList.get(position).getSizeItem().equals("")){
            holder.typeOfProduct.setText(detailsModelArrayList.get(position).getColorItem());
        } else {
            holder.typeOfProduct.setText(detailsModelArrayList.get(position).getSizeItem() + " / " +
                    detailsModelArrayList.get(position).getColorItem());
        }

        Glide.with(mContext).load(detailsModelArrayList.get(position).getDeal_image()).placeholder(mContext.getResources().getDrawable(R.drawable.fashionhouse)).into(holder.image);


        holder.txtCountProducts.setText(detailsModelArrayList.get(position).getBought());

    //    quantity = Integer.parseInt(detailsModelArrayList.get(position).getBought());



        holder.btnMinusProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                decrement(holder);
                listener.onItemClickMinus(position, holder.txtCountProducts.getText().toString());

            }
        });

        holder.btnPlusProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (quantity < 10) {
                    increment(holder, position);
                    listener.onItemClickPlus(position, holder.txtCountProducts.getText().toString());
                }

            }

        });


        holder.deleteBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemDelete(detailsModelArrayList.get(position).getIdUnique(), position);

                detailsModelArrayList.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.moveToSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClickSaveLater(detailsModelArrayList.get(position).getDeal_id(), position);
                detailsModelArrayList.remove(position);
                notifyDataSetChanged();
            }
        });



    }


    @Override
    public int getItemCount() {
        return detailsModelArrayList.size();
    }


    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView image, btnMinusProduct, btnPlusProduct;
        TextView title, price, typeOfProduct, moveToSaved;
        TextView txtCountProducts;
        RelativeLayout deleteBag;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);
            txtCountProducts = itemView.findViewById(R.id.txtCountProducts);
            btnMinusProduct = itemView.findViewById(R.id.btnMinusProduct);
            btnPlusProduct = itemView.findViewById(R.id.btnPlusProduct);
            deleteBag = itemView.findViewById(R.id.deleteBag);
            moveToSaved = itemView.findViewById(R.id.moveToSaved);
            typeOfProduct = itemView.findViewById(R.id.typeOfProduct);

        }
    }



    public interface OnItemClickListener {
        void onItemClickSaveLater(String dealId, int pos);
        void onItemClickPlus(int position, String quantity);
        void onItemClickMinus(int position, String quantity);
        void onItemDelete(String dealId, int pos);
        void onItemClick(int pos);

    }


    public void increment(Viewholder myViewHolder, int position) {
        quantity = Integer.parseInt(myViewHolder.txtCountProducts.getText().toString());
            if (Integer.parseInt(myViewHolder.txtCountProducts.getText().toString()) < Integer.parseInt(detailsModelArrayList.get(position).getQuantity())) {
            quantity++;
        }
        myViewHolder.txtCountProducts.setText(String.valueOf(quantity));

        Log.e("check", myViewHolder.txtCountProducts.getText().toString() + "--" + detailsModelArrayList.get(position).getQuantity());
    }


    public void decrement(Viewholder myViewHolder) {
        if(!myViewHolder.txtCountProducts.getText().toString().equals("1")){
            quantity = Integer.parseInt(myViewHolder.txtCountProducts.getText().toString());
            quantity--;
            myViewHolder.txtCountProducts.setText(String.valueOf(quantity));
        }
    }


}