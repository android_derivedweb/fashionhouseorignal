package com.fashion.fashionhouse.Fragments;

import static com.fashion.fashionhouse.Activity.MainActivity.image1;
import static com.fashion.fashionhouse.Activity.MainActivity.image2;
import static com.fashion.fashionhouse.Activity.MainActivity.image3;
import static com.fashion.fashionhouse.Activity.MainActivity.image4;
import static com.fashion.fashionhouse.Activity.MainActivity.image5;
import static com.fashion.fashionhouse.Activity.MainActivity.tagAppName;
import static com.fashion.fashionhouse.Activity.MainActivity.text1;
import static com.fashion.fashionhouse.Activity.MainActivity.text2;
import static com.fashion.fashionhouse.Activity.MainActivity.text3;
import static com.fashion.fashionhouse.Activity.MainActivity.text4;
import static com.fashion.fashionhouse.Activity.MainActivity.text5;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.fashion.fashionhouse.Activity.AboutPeperempe;
import com.fashion.fashionhouse.Activity.AddressList;
import com.fashion.fashionhouse.Activity.ChangePassword;
import com.fashion.fashionhouse.Activity.HowToReturn;
import com.fashion.fashionhouse.Activity.Order_History;
import com.fashion.fashionhouse.Activity.PersonalProfile;
import com.fashion.fashionhouse.Activity.PrivacyPolicy;
import com.fashion.fashionhouse.Activity.ReturnNow;
import com.fashion.fashionhouse.Activity.ShippingNotice;
import com.fashion.fashionhouse.Activity.TermsCondition;
import com.fashion.fashionhouse.Activity.WelcomeScreen;
import com.fashion.fashionhouse.Model.CategoryDetailsModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.Database;
import com.fashion.fashionhouse.Utils.UserSession;

import java.util.ArrayList;

public class Profile extends Fragment {


    private UserSession session;
    private RequestQueue requestQueue;
    private TextView profile_name;
    private TextView sign_out;

    private RelativeLayout shippingLayout;
    private LinearLayout shippingInner;

    private boolean condition = true;

    private ImageView arrRight, arrBottom;


    private TextView bagCount;
    private Database database;
    private ArrayList<CategoryDetailsModel> categoryDetailsModels = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_profile, container, false);

        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        database = new Database(getContext());

        categoryDetailsModels = database.getAllUser();


        view.findViewById(R.id.order_history).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Order_History.class));
            }
        });



        view.findViewById(R.id.callLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDialog();

            }
        });



        view.findViewById(R.id.emailSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "support@fashionhouse.ng"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject");
                    intent.putExtra(Intent.EXTRA_TEXT, "your_text");
                    startActivity(intent);
                }catch(ActivityNotFoundException e){

                }
            }
        });


        shippingLayout = view.findViewById(R.id.shippingLayout);
        shippingInner = view.findViewById(R.id.shippingInner);
        arrRight = view.findViewById(R.id.arrRight);
        arrBottom = view.findViewById(R.id.arrBottom);
        bagCount = view.findViewById(R.id.bagCount);


        bagCount.setText(String.valueOf(categoryDetailsModels.size()));


        shippingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (condition) {
                    shippingInner.setVisibility(View.VISIBLE);
                    arrRight.setVisibility(View.GONE);
                    arrBottom.setVisibility(View.VISIBLE);
                    condition = false;
                } else {
                    shippingInner.setVisibility(View.GONE);
                    arrRight.setVisibility(View.VISIBLE);
                    arrBottom.setVisibility(View.GONE);
                    condition = true;
                }

            }
        });




        view.findViewById(R.id.addressBook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddressList.class));
            }
        });

        view.findViewById(R.id.howToReturn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), HowToReturn.class));
            }
        });

        view.findViewById(R.id.changePass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), ChangePassword.class));
            }
        });

        view.findViewById(R.id.personalInformation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), PersonalProfile.class));
            }
        });

       view.findViewById(R.id.termsAndCondition).setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               startActivity(new Intent(getContext(), TermsCondition.class));
           }
       });

        view.findViewById(R.id.aboutPeperempe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), AboutPeperempe.class));
            }
        });

        view.findViewById(R.id.privacyPolicy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), PrivacyPolicy.class));
            }
        });


        view.findViewById(R.id.shippingInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ShippingNotice.class));

            }
        });


        view.findViewById(R.id.returnNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ReturnNow.class));
            }
        });



        profile_name = view.findViewById(R.id.profile_name);
        sign_out = view.findViewById(R.id.sign_out);
        profile_name.setText(session.getKeyFirstName() + " " + session.getKeyLastName());
        sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logout();
                LoginManager.getInstance().logOut();
                database.deleteAll();
                Intent intent = new Intent(getActivity(), WelcomeScreen.class);
                getActivity().startActivity(intent);
                getActivity().finish();

            }
        });
        view.findViewById(R.id.cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShoppingBag shoppingBag = new ShoppingBag();
                replaceFragment(R.id.fragmentLinearHome, shoppingBag, "shipping_bag", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag_active);

                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.pink));

                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Shopping Bag");

            }
        });


        return view;

    }


    private void openDialog() {

        Dialog dialogForCity;
        dialogForCity = new Dialog(getContext());
        dialogForCity.setContentView(R.layout.custom_dailog_call);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        RelativeLayout call1 = dialogForCity.findViewById(R.id.call1);
        RelativeLayout call2 = dialogForCity.findViewById(R.id.call2);


        call1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDialActivity("+12545451733");
                dialogForCity.dismiss();
            }
        });


        call2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDialActivity("+2349035804359");
                dialogForCity.dismiss();
            }
        });



        dialogForCity.show();
    }



    private void startDialActivity(String phone){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phone));
        startActivity(intent);
    }






    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


}