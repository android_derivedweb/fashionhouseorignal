package com.fashion.fashionhouse.Fragments;

import static android.os.Environment.DIRECTORY_DOWNLOADS;
import static com.fashion.fashionhouse.Activity.MainActivity.image1;
import static com.fashion.fashionhouse.Activity.MainActivity.image2;
import static com.fashion.fashionhouse.Activity.MainActivity.image3;
import static com.fashion.fashionhouse.Activity.MainActivity.image4;
import static com.fashion.fashionhouse.Activity.MainActivity.image5;
import static com.fashion.fashionhouse.Activity.MainActivity.text1;
import static com.fashion.fashionhouse.Activity.MainActivity.text2;
import static com.fashion.fashionhouse.Activity.MainActivity.text3;
import static com.fashion.fashionhouse.Activity.MainActivity.text4;
import static com.fashion.fashionhouse.Activity.MainActivity.text5;
import static com.fashion.fashionhouse.Activity.ReturnNow.REQUEST_ID_MULTIPLE_PERMISSIONS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Activity.MainActivity;
import com.fashion.fashionhouse.Adapter.AdapterMightAlsoLike;
import com.fashion.fashionhouse.Adapter.AdapterNewIn;
import com.fashion.fashionhouse.Adapter.ViewPagerImages;
import com.fashion.fashionhouse.Model.CategoryDetailsModel;
import com.fashion.fashionhouse.Model.ImagesModel;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.Model.SelectCitySpinner;
import com.fashion.fashionhouse.Model.SizeColorModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.Database;
import com.fashion.fashionhouse.Utils.FileDownloader;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ItemsDetails extends Fragment {


    private ArrayList<SizeColorModel> mDatasetServices = new ArrayList<>();


    private RecyclerView recMightAlsoLike, recBuyTheLook;
    private AdapterNewIn adapterNewIn;
    private AdapterMightAlsoLike adapterMightAlsoLike;


    private ImageView imageProduct, imageWishlist;
    private TextView Description, ProductDetails, DeliveryReturnInfo, title, discountPrice, actualPrice, discount, addToBag, productTitle;


    private UserSession session;
    private RequestQueue requestQueue;

    private int currentStatusWishlist = 0;


    private Database dbHelper;
    private ArrayList<CategoryDetailsModel> detailsModelArrayList = new ArrayList<>();

    private ArrayList<SizeColorModel> arraySize = new ArrayList<>();
    private ArrayList<SizeColorModel> arrayColor = new ArrayList<>();


    private String imageCart;
    private String titleCart;
    private String priceCart;
    private int availQuantity = 0 ;
    private String color = "";
    private String size = "";
    private String colorId = "";
    private String sizeId = "";

    private String imageUrl, titleStr;


    private int colorInt = 0;
    private int sizeInt = 0;

    private Spinner colorSpinner, sizeSpinner;

    private ArrayList<ProductListModel> productListModelArrayList = new ArrayList<>();


    private TextView noStock;
    private String ProductID = "";

    private WebView webViewProductDetails;
    private LinearLayout disCountLayOut, detailsLayout, linearVarient;

    private RelativeLayout colorRelative, sizeRelative, layoutDwnld, youAlsoLayout;

    private String ProductVariantType = "";

    private String VariantID = "";

    private TextView SKU;
    private TextView product_code;

    private String ColorProductVariantID = "";
    private String SizeProductVariantID = "";


    private ArrayList<String> ColorStringArray = new ArrayList<>();
    private ArrayList<String> VarientColorArray = new ArrayList<>();

    private Bitmap imageBit;
    private int arraySize_pos = 0;


    private KProgressHUD progressDialogDwnld;
    private ArrayList<ImagesModel> imagesModelArrayList = new ArrayList<>();

    private DotsIndicator worm_dots_indicator;

    private ViewPagerImages viewPagerImages;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_items_details, container, false);

        session = new UserSession(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue


        imageProduct = view.findViewById(R.id.imageProduct);
        title = view.findViewById(R.id.title);
        discountPrice = view.findViewById(R.id.discountPrice);
        actualPrice = view.findViewById(R.id.actualPrice);
        discount = view.findViewById(R.id.discount);
        product_code = view.findViewById(R.id.product_code);
        imageWishlist = view.findViewById(R.id.imageWishlist);
        addToBag = view.findViewById(R.id.addToBag);
        colorSpinner = view.findViewById(R.id.colorSpinner);
        sizeSpinner = view.findViewById(R.id.sizeSpinner);
        noStock = view.findViewById(R.id.noStock);
        disCountLayOut = view.findViewById(R.id.disCountLayOut);
        colorRelative = view.findViewById(R.id.colorRelative);
        sizeRelative = view.findViewById(R.id.sizeRelative);
        linearVarient = view.findViewById(R.id.linearVarient);
        productTitle = view.findViewById(R.id.productTitle);
        layoutDwnld = view.findViewById(R.id.layoutDwnld);
        youAlsoLayout = view.findViewById(R.id.youAlsoLayout);

        SKU = view.findViewById(R.id.SKU);


        Description = view.findViewById(R.id.Description);
        ProductDetails = view.findViewById(R.id.ProductDetails);
        DeliveryReturnInfo = view.findViewById(R.id.DeliveryReturnInfo);

        webViewProductDetails = view.findViewById(R.id.webViewProductDetails);
        detailsLayout = view.findViewById(R.id.detailsLayout);
        worm_dots_indicator = view.findViewById(R.id.worm_dots_indicator);


        try {
            ProductID = getArguments().getString("ProductID");
            Log.e("ProductID", ProductID +"--");
        } catch (Exception e) {

        }


        dbHelper = new Database(getActivity());
        detailsModelArrayList = dbHelper.getAllUser();


        view.findViewById(R.id.download).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkAndRequestPermissions()){
                    progressDialogDwnld = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                            .setLabel("Please wait")
                            .setCancellable(false)
                            .setAnimationSpeed(2)
                            .setDimAmount(0.5f)
                            .show();


                    new DownloadFileSecond().execute(imageUrl, titleStr+".jpeg");
                }else {
                    Toast.makeText(getContext(), "Go to Setting and Approve Storage Permission", Toast.LENGTH_SHORT).show();
                }

            }
        });


        //for product images viewpage
        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        worm_dots_indicator = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator);
        viewPagerImages = new ViewPagerImages(getContext(), imagesModelArrayList, new ViewPagerImages.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                if(checkAndRequestPermissions()){

                    progressDialogDwnld = KProgressHUD.create(getActivity())
                            .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                            .setLabel("Please wait")
                            .setCancellable(false)
                            .setAnimationSpeed(2)
                            .setDimAmount(0.5f)
                            .show();

                    new DownloadFileSecond().execute(imagesModelArrayList.get(item).getProductImageName(), titleStr + String.valueOf(item)+".jpeg");

                }else {
                    Toast.makeText(getContext(), "Go to Setting and Approve Storage Permission", Toast.LENGTH_SHORT).show();
                }

            }
        });
        mViewPager.setAdapter(viewPagerImages);
        worm_dots_indicator.setViewPager(mViewPager);




      /*  for (int i = 0; i < detailsModelArrayList.size(); i++){

            if (ProductID.equals(detailsModelArrayList.get(i).getDeal_id())){
                addToBag.setText("UPDATE ITEM");
            } else {

            }
        }
*/

        addToBag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    if (!addToBag.getText().toString().equals("UPDATE ITEM")) {

                        if (ProductVariantType.equals("with_product_variant")){

                            if(arraySize_pos==arraySize.size()-1){
                                Toast.makeText(getActivity(), "Please Select Size", Toast.LENGTH_SHORT).show();

                            }else if (availQuantity > 0) {

                                if (colorInt == 0) {
                                    Toast.makeText(getActivity(), "Select Color", Toast.LENGTH_SHORT).show();
                                }else if (arraySize.size() == 1){
                                    Toast.makeText(getActivity(), "Size variants not available!", Toast.LENGTH_SHORT).show();
                                } else {

                                    String IdUnique = UUID.randomUUID().toString();
                                    dbHelper.InsertDetails(IdUnique, imageCart, titleCart, ProductID, priceCart, color, colorId, size, sizeId, VariantID, "1", String.valueOf(availQuantity));
                                    Toast.makeText(getActivity(), "Product Added Successfully", Toast.LENGTH_SHORT).show();
                                    //        addToBag.setText("UPDATE ITEM");
                                }

                            } else {
                                Toast.makeText(getActivity(), "Item not available.", Toast.LENGTH_SHORT).show();
                            }

                        } else if (ProductVariantType.equals("simple_product")){

                            if (availQuantity > 0) {

                                if (colorInt == 0) {
                                    Toast.makeText(getActivity(), "Select Color", Toast.LENGTH_SHORT).show();
                                } else {
                                    String IdUnique = UUID.randomUUID().toString();
                                    dbHelper.InsertDetails(IdUnique, imageCart, titleCart, ProductID, priceCart, color, colorId, size, sizeId, VariantID, "1", String.valueOf(availQuantity));
                                    Toast.makeText(getActivity(), "Product Added Successfully", Toast.LENGTH_SHORT).show();
                                    //            addToBag.setText("UPDATE ITEM");
                                }

                            } else {
                                Toast.makeText(getActivity(), "Item not available.", Toast.LENGTH_SHORT).show();
                            }


                        } else if (ProductVariantType.equals("without_product_variant")){
                            if (availQuantity <= 0){
                                Toast.makeText(getActivity(), "Item not available!", Toast.LENGTH_SHORT).show();
                            } else {
                                String IdUnique = UUID.randomUUID().toString();
                                dbHelper.InsertDetails(IdUnique, imageCart, titleCart, ProductID, priceCart, color, colorId, size, sizeId, VariantID, "1", String.valueOf(availQuantity));
                                Toast.makeText(getActivity(), "Product Added Successfully", Toast.LENGTH_SHORT).show();
                        //        Toast.makeText(getActivity(), "Item not available now, please check back later", Toast.LENGTH_SHORT).show();
                                //         addToBag.setText("UPDATE ITEM");
                            }

                        } else {
                            Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }


                    } else {

                    }


                detailsModelArrayList = dbHelper.getAllUser();

                MainActivity.bagCount.setText(String.valueOf(detailsModelArrayList.size()));


            }
        });



        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();


                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                }
            }
        });



        colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ColorProductVariantID = arrayColor.get(position).getProductVariantOptionID();

                color = arrayColor.get(position).getProductVariantOption();

                    try {

                        if (ProductVariantType.equals("simple_product")){

                            colorInt = 1;
                            getProductSku(ProductVariantType, ColorProductVariantID, "", ProductID);

                        } else if (ProductVariantType.equals("with_product_variant")){
                            colorInt = 1;
                            getSizeVarient(arrayColor.get(position).getProductVariantOptionID(), ProductID);

                        }

                    }catch (Exception e){

                    }


               /* if (position != arrayColor.size() - 1) {
                    colorInt = 1;
                    color = arrayColor.get(position).getProductVariantOption();
                    colorId = arrayColor.get(position).getProductVariantOptionID();

                    if(availQuantity.isEmpty()){
                        availQuantity =  arrayColor.get(position).getQyt();
                    }else if(Integer.parseInt(availQuantity) >= Integer.parseInt(arrayColor.get(position).getQyt())) {
                        availQuantity =  arrayColor.get(position).getQyt();
                    } else {
                        availQuantity =  arrayColor.get(position).getQyt();
                    }

                    if (Integer.parseInt(availQuantity) == 0){
                        noStock.setVisibility(View.VISIBLE);
                    } else {
                        noStock.setVisibility(View.GONE);
                    }

                    Log.e("availQuantity", availQuantity + "--");

                }*/


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                arraySize_pos = position;

                if (arraySize_pos != arraySize.size() - 1) {
                    SizeProductVariantID = arraySize.get(position).getProductVariantOptionID();
                    size = arraySize.get(position).getProductVariantOption();



                    getProductSku(ProductVariantType, ColorProductVariantID,
                            arraySize.get(position).getProductVariantOptionID(), ProductID);


               /* if (colorInt == 1){
                    getProductSku(ProductVariantType, ColorProductVariantID,
                            arraySize.get(position).getProductVariantOptionID(), ProductID);
                } else {
                    getProductSku(ProductVariantType, "",
                            arraySize.get(position).getProductVariantOptionID(), ProductID);
                }*/

             /*   if (position != arraySize.size() - 1) {
                    sizeInt = 1;
                    size = arraySize.get(position).getProductVariantOption();
                    sizeId = arraySize.get(position).getProductVariantOptionID();
               //     availQuantity =  arraySize.get(position).getProductVariantOptionID();

                    if(availQuantity.isEmpty()){
                        availQuantity =  arraySize.get(position).getQyt();
                    }else if(Integer.parseInt(availQuantity) >= Integer.parseInt(arraySize.get(position).getQyt())) {
                        availQuantity =  arraySize.get(position).getQyt();
                    }

                    if (Integer.parseInt(availQuantity) == 0){
                        noStock.setVisibility(View.VISIBLE);
                    } else {
                        noStock.setVisibility(View.GONE);
                    }

                }*/
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        view.findViewById(R.id.imageWishlist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.isLoggedIn()){
                    setWishlist(ProductID);
                } else {
                    Me me = new Me("0");
                    replaceFragment(R.id.fragmentLinearHome, me, "me", null);

                    image1.setImageResource(R.drawable.home);
                    image2.setImageResource(R.drawable.search);
                    image3.setImageResource(R.drawable.wishlist);
                    image4.setImageResource(R.drawable.user_active);
                    image5.setImageResource(R.drawable.bag);


                    text1.setTextColor(getResources().getColor(R.color.dark_gray));
                    text2.setTextColor(getResources().getColor(R.color.dark_gray));
                    text3.setTextColor(getResources().getColor(R.color.dark_gray));
                    text4.setTextColor(getResources().getColor(R.color.pink));
                    text5.setTextColor(getResources().getColor(R.color.dark_gray));

                    MainActivity.tagAppName.setText("Fashion House");
                    MainActivity.tagAppName.setVisibility(View.VISIBLE);
                    MainActivity.exitBtnHome.setVisibility(View.GONE);

                }

            }
        });



        recMightAlsoLike = view.findViewById(R.id.recMightAlsoLike);
        recMightAlsoLike.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapterMightAlsoLike = new AdapterMightAlsoLike(getActivity(), productListModelArrayList, new AdapterMightAlsoLike.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayList.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome, fragobj, "Fragment", null);

            }

            @Override
            public void onItemClickWishList(int item) {
                if (session.isLoggedIn()){
                    setWishlist(productListModelArrayList.get(item).getProductID());
                } else {
                    Me me = new Me("0");
                    replaceFragment(R.id.fragmentLinearHome, me, "me", null);

                    image1.setImageResource(R.drawable.home);
                    image2.setImageResource(R.drawable.search);
                    image3.setImageResource(R.drawable.wishlist);
                    image4.setImageResource(R.drawable.user_active);
                    image5.setImageResource(R.drawable.bag);


                    text1.setTextColor(getResources().getColor(R.color.dark_gray));
                    text2.setTextColor(getResources().getColor(R.color.dark_gray));
                    text3.setTextColor(getResources().getColor(R.color.dark_gray));
                    text4.setTextColor(getResources().getColor(R.color.pink));
                    text5.setTextColor(getResources().getColor(R.color.dark_gray));

                    MainActivity.tagAppName.setText("Fashion House");
                    MainActivity.tagAppName.setVisibility(View.VISIBLE);
                    MainActivity.exitBtnHome.setVisibility(View.GONE);

                }

            }
        });
        recMightAlsoLike.setAdapter(adapterMightAlsoLike);




        recBuyTheLook = view.findViewById(R.id.recBuyTheLook);
        recBuyTheLook.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
     /*   adapterMightAlsoLike = new AdapterMightAlsoLike(getActivity(), new AdapterMightAlsoLike.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });*/
        recBuyTheLook.setAdapter(adapterMightAlsoLike);


        getDetails(ProductID);


        return view;

    }


    private boolean checkAndRequestPermissions() {
        int wtite = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private class DownloadFileSecond extends AsyncTask<String, Void, Void> {

        File dir = null;

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            //    File folder = new File(extStorageDirectory, "Snagpay");

            if (Build.VERSION_CODES.R > Build.VERSION.SDK_INT) {
                dir = new File(Environment.getExternalStorageDirectory().getPath()
                        + "//"+getResources().getString(R.string.app_name));
            } else {
                dir = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).getPath()
                        + "//"+getResources().getString(R.string.app_name));
            }

            if (!dir.exists())
                dir.mkdir();

            File pdfFile = new File(dir, fileName);

            try{
                pdfFile.createNewFile();

            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);

            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);

            Toast.makeText(getActivity(),"File Path : " + dir.getAbsolutePath().toString(),Toast.LENGTH_LONG).show();
            progressDialogDwnld.dismiss();
        }
    }



    private void getSizeVarient(String ColorProductVariantID, String ProductID) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
       //         .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "get-product-size",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                 //       progressDialog.dismiss();
                        arraySize.clear();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object1 = jsonArray.getJSONObject(i);

                                    SizeColorModel sizeColorModel = new SizeColorModel();
                                    sizeColorModel.setProductVariantOptionID(object1.getString("SizeProductVariantID"));
                                    sizeColorModel.setProductVariantOption(object1.getString("ProductVariantOption"));
                                    sizeColorModel.setQyt(object1.getString("Qty"));

                                    arraySize.add(sizeColorModel);
                                }
                                SizeColorModel sizeColorModel3 = new SizeColorModel();
                                sizeColorModel3.setProductVariantOptionID("");
                                sizeColorModel3.setProductVariantOption("Select Size");
                                arraySize.add(sizeColorModel3);


                                if (arraySize.size() == 1){
                                    SKU.setVisibility(View.GONE);
                                } else {
                                    SKU.setVisibility(View.GONE);
                                }


                                SelectCitySpinner adapter = new SelectCitySpinner(getActivity(),
                                        android.R.layout.simple_spinner_item,
                                        arraySize);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                                sizeSpinner.setAdapter(adapter);
                                sizeSpinner.setSelection(adapter.getCount());



                            }else {
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                //            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                //        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductID", ProductID);
                params.put("ColorProductVariantID", ColorProductVariantID);
           //     params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void getProductSku(String ProductVariantType, String ColorProductVariantID, String SizeProductVariantID, String ProductID) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
       //         .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "get-product-sku",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                 //       progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                JSONObject object = jsonObject.getJSONObject("data");

                                VariantID = object.getString("VariantID");

                                SKU.setText("SKU : " + object.getString("SKU"));
                                SKU.setVisibility(View.GONE);

                                availQuantity = object.getInt("Qty");

                                Log.e("availQunty", availQuantity + "--");


                                if (availQuantity <= 0){
                                    noStock.setVisibility(View.VISIBLE);
                                } else {
                                    noStock.setVisibility(View.GONE);
                                }



                            }else {
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                //            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                //        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductVariantType", ProductVariantType);
                params.put("ProductID", ProductID);
                params.put("ColorProductVariantID", ColorProductVariantID);
                params.put("SizeProductVariantID", SizeProductVariantID);
           //     params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void setWishlist(String productId) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
           //     .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                UserSession.BASEURL + "add-to-wishlist",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

              //          progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                if (currentStatusWishlist == 0){
                                    imageWishlist.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.heart_fill));
                                    currentStatusWishlist = 1;
                                } else if (currentStatusWishlist == 1){
                                    imageWishlist.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.heart_blank));
                                    currentStatusWishlist = 0;
                                }

                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                            }else {
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
              //          progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductID", productId);
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }




    private void getDetails(String ProductID) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "product-details",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        productListModelArrayList.clear();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseDetails", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                JSONObject object = jsonObject.getJSONObject("data");

                                Glide.with(getActivity()).load(object.getString("ProductImage")).into(imageProduct);

                                imageUrl = object.getString("ProductImage");

                                title.setText(object.getString("ProductTitle"));
                                titleStr = object.getString("ProductTitle");

                                discountPrice.setText(getResources().getString(R.string.currency_symbol) + object.getString("Price"));
                                actualPrice.setText(getResources().getString(R.string.currency_symbol) + object.getString("OriginalPrice"));
                                actualPrice.setPaintFlags(actualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                                double actual = Double.parseDouble(object.getString("OriginalPrice"));
                                double discountDouble = Double.parseDouble(object.getString("Price"));



                                if ((actual - discountDouble) == 0){
                                    disCountLayOut.setVisibility(View.GONE);
                                } else {
                                    disCountLayOut.setVisibility(View.VISIBLE);
                                    discountPrice.setTextColor(getResources().getColor(R.color.green));
                                }
                                double total = (actual - discountDouble) * 100/actual;
                                int disCount = (int) total;
                                discount.setText("(" + disCount + "% OFF)");
                                discount.setTextColor(getResources().getColor(R.color.red));

                                ProductVariantType = object.getString("ProductVariantType");


                                if (ProductVariantType.equals("with_product_variant")){
                                    colorRelative.setVisibility(View.VISIBLE);
                                } else if (ProductVariantType.equals("simple_product")){
                                    colorRelative.setVisibility(View.VISIBLE);
                                    sizeRelative.setVisibility(View.GONE);
                                } else if (ProductVariantType.equals("without_product_variant")){
                                    linearVarient.setVisibility(View.GONE);
                                    availQuantity = object.getInt("Quantity");

                                    if (availQuantity <= 0){
                                        noStock.setVisibility(View.VISIBLE);
                                    } else {
                                        noStock.setVisibility(View.GONE);
                                    }
                                }

                                Log.e("availQuantity", availQuantity + "--");



                                Description.setText(object.getString("Description"));
                                product_code.setText("PRODUCT CODE : "+object.getString("ProductCode"));
                                if (object.getString("Description").equals(null)){
                                    detailsLayout.setVisibility(View.GONE);
                                }
                                DeliveryReturnInfo.setText(object.getString("DeliveryReturnInfo"));

                                webViewProductDetails.loadData(object.getString("ProductDetails"), "text/html", "UTF-8");


                                if (object.getString("IsWishlist").equals("1")){
                                    imageWishlist.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.heart_fill));
                                    currentStatusWishlist = 1;
                                } else if (object.getString("IsWishlist").equals("0")){
                                    imageWishlist.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.heart_blank));
                                    currentStatusWishlist = 0;
                                }



                                imageCart = object.getString("ProductImage");
                                titleCart = object.getString("ProductTitle");
                                priceCart = object.getString("Price");




                                JSONArray jsonArray = object.getJSONArray("product_variants");


                                // for size drop down spinner

                              /*  JSONObject objectSize = jsonArray.getJSONObject(0);

                                JSONArray jsonArray1 = objectSize.getJSONArray("options");

                                for (int i = 0; i < jsonArray1.length(); i++){
                                    JSONObject object1 = jsonArray1.getJSONObject(i);
                                    SizeColorModel sizeColorModel = new SizeColorModel();
                                    sizeColorModel.setProductVariantOptionID(object1.getString("SizeProductVariantID"));
                                    sizeColorModel.setProductVariantOption(object1.getString("ProductVariantOption"));
                                    sizeColorModel.setQyt(object1.getString("Qty"));

                                    arraySize.add(sizeColorModel);
                                }
                                SizeColorModel sizeColorModel3 = new SizeColorModel();
                                sizeColorModel3.setProductVariantOptionID("");
                                sizeColorModel3.setProductVariantOption("Size");
                                arraySize.add(sizeColorModel3);


                                SelectCitySpinner adapter = new SelectCitySpinner(getActivity(),
                                        android.R.layout.simple_spinner_item,
                                        arraySize);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                                sizeSpinner.setAdapter(adapter);
                                sizeSpinner.setSelection(adapter.getCount());*/





                                JSONObject objectColor = jsonArray.getJSONObject(1);

                                JSONArray jsonArray2 = objectColor.getJSONArray("options");

                                for (int i = 0; i < jsonArray2.length(); i++){
                                    JSONObject object1 = jsonArray2.getJSONObject(i);
                                    SizeColorModel sizeColorModel = new SizeColorModel();
                                    sizeColorModel.setProductVariantOptionID(object1.getString("ColorProductVariantID"));
                                    sizeColorModel.setProductVariantOption(object1.getString("ProductVariantOption"));
                                    sizeColorModel.setQyt(object1.getString("Qty"));

                                    arrayColor.add(sizeColorModel);

                                    ColorStringArray.add(object1.getString("ColorProductVariantID"));
                                    VarientColorArray.add(object1.getString("ProductVariantOption"));
                                }
                                /*SizeColorModel sizeColorModel6 = new SizeColorModel();
                                sizeColorModel6.setProductVariantOptionID("");
                                sizeColorModel6.setProductVariantOption("Color");
                                arrayColor.add(sizeColorModel6);*/

                         //       Log.e("check", "arraySize" + arraySize.get(0).getQyt() + "--" + "arrayColor" + arrayColor.get(0).getQyt());


                               /* ArrayAdapter adapter1 = new ArrayAdapter(getActivity(),
                                        android.R.layout.simple_spinner_item,
                                        VarientColorArray);
                                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_item);
                                colorSpinner.setAdapter(adapter1);
                                colorSpinner.setSelection(adapter1.getCount());*/


                                ArrayAdapter<String> locAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, VarientColorArray);
                                locAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                                colorSpinner.setAdapter(locAdapter);
                                colorSpinner.setSelection(VarientColorArray.size() - 1); // Hidden item to appear in the spinner





                                JSONArray jsonArray3 = object.getJSONArray("you_might_also_like");

                                for (int i = 0; i < jsonArray3.length(); i++){

                                    JSONObject object1 = jsonArray3.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object1.getString("ProductID"));
                                    productListModel.setProductTitle(object1.getString("ProductTitle"));
                                    productListModel.setPrice(object1.getString("Price"));
                                    productListModel.setDescription(object1.getString("Description"));
                                    productListModel.setProductImage(object1.getString("ProductImage"));


                                    if (object1.getString("IsWishlist").equals("0")){
                                        productListModel.setWishlist(false);
                                    } else if (object1.getString("IsWishlist").equals("1")){
                                        productListModel.setWishlist(true);
                                    }

                                    productListModelArrayList.add(productListModel);
                                }

                                adapterMightAlsoLike.notifyDataSetChanged();


                                if (productListModelArrayList.isEmpty()){
                                    youAlsoLayout.setVisibility(View.GONE);
                                }


                                // for item details title
                                MainActivity.tagAppName.setVisibility(View.GONE);
                                productTitle.setText(object.getString("ProductTitle"));
                                MainActivity.exitBtnHome.setVisibility(View.GONE);




                                // for view pager of product images
                                JSONArray jsonArray1 = object.getJSONArray("images");

                                for (int i = 0; i < jsonArray1.length(); i++){
                                    JSONObject object1 = jsonArray1.getJSONObject(i);

                                    ImagesModel imagesModel = new ImagesModel();
                                    imagesModel.setProductImageID(object1.getString("ProductImageID"));
                                    imagesModel.setProductID(object1.getString("ProductID"));
                                    imagesModel.setProductImageName(object1.getString("ImageName"));
                                    imagesModel.setIsDefaultProductImage(object1.getString("IsDefaultProductImage"));

                                    imagesModelArrayList.add(imagesModel);
                                }
                                viewPagerImages.notifyDataSetChanged();

/*
                                  try {
                                    URL url = new URL(imageUrl);
                                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                    connection.setDoInput(true);
                                    connection.connect();
                                    InputStream input = connection.getInputStream();
                                    imageBit = BitmapFactory.decodeStream(input);

                                    Palette.generateAsync(imageBit, new Palette.PaletteAsyncListener() {
                                        public void onGenerated(Palette palette) {

                                            Log.e("testColor", palette.toString() +"--");
                                        }
                                    });


                                } catch (IOException e) {
                                    e.printStackTrace();
                                }*/
                                //      Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }else {
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductID", ProductID);
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //     params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }




    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }



}