package com.fashion.fashionhouse.Fragments;

import static com.fashion.fashionhouse.Activity.MainActivity.exitBtnHome;
import static com.fashion.fashionhouse.Activity.MainActivity.image1;
import static com.fashion.fashionhouse.Activity.MainActivity.image2;
import static com.fashion.fashionhouse.Activity.MainActivity.image3;
import static com.fashion.fashionhouse.Activity.MainActivity.image4;
import static com.fashion.fashionhouse.Activity.MainActivity.image5;
import static com.fashion.fashionhouse.Activity.MainActivity.tagAppName;
import static com.fashion.fashionhouse.Activity.MainActivity.text1;
import static com.fashion.fashionhouse.Activity.MainActivity.text2;
import static com.fashion.fashionhouse.Activity.MainActivity.text3;
import static com.fashion.fashionhouse.Activity.MainActivity.text4;
import static com.fashion.fashionhouse.Activity.MainActivity.text5;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Adapter.AdapterProduct;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.EndlessRecyclerViewScrollListener;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ProductListFragment extends Fragment {

    private RecyclerView recItemsList;
    private AdapterProduct adapterProduct;

    private UserSession session;
    private RequestQueue requestQueue;


    private ArrayList<ProductListModel> productListModelArrayList = new ArrayList<>();
    private String BrandID, SubCategoryID;


    private RelativeLayout cartEmpty;

    private String Title;
    public int last_size = 0;
    public int IntPage = 1;
    private GridLayoutManager gridLayoutManager;

    private String mRanomNumer = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.activity_product_list, container, false);


        cartEmpty = view.findViewById(R.id.cartEmpty);


        mRanomNumer = randomString(5);
        Log.e("stringRa", mRanomNumer + "--");


        try {
            BrandID = getArguments().getString("BrandID");
            SubCategoryID = getArguments().getString("SubCategoryID");
            Title = getArguments().getString("Title");

            tagAppName.setText("Products > " + Title);
            tagAppName.setVisibility(View.VISIBLE);
            exitBtnHome.setVisibility(View.GONE);
            Log.e("brand", BrandID + "-");

        } catch (Exception e) {

        }


        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                }

            }
        });


        session = new UserSession(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue


        recItemsList = view.findViewById(R.id.recItemsList);

        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recItemsList.setLayoutManager(gridLayoutManager);
        adapterProduct = new AdapterProduct(getActivity(), productListModelArrayList, new AdapterProduct.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

/* startActivity(new Intent(getActivity(), ItemsDetail.class)
.putExtra("ProductID", productListModelArrayList.get(item).getProductID()));
*/
                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayList.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome, fragobj, "ProductList", null);

            }

            @Override
            public void onItemClickWishList(int item) {

                if (session.isLoggedIn()) {
                    setWishlist(productListModelArrayList.get(item).getProductID());
                } else {
                    Me me = new Me("0");
                    replaceFragment(R.id.fragmentLinearHome, me, "me", null);

                    image1.setImageResource(R.drawable.home);
                    image2.setImageResource(R.drawable.search);
                    image3.setImageResource(R.drawable.wishlist);
                    image4.setImageResource(R.drawable.user_active);
                    image5.setImageResource(R.drawable.bag);


                    text1.setTextColor(getResources().getColor(R.color.dark_gray));
                    text2.setTextColor(getResources().getColor(R.color.dark_gray));
                    text3.setTextColor(getResources().getColor(R.color.dark_gray));
                    text4.setTextColor(getResources().getColor(R.color.pink));
                    text5.setTextColor(getResources().getColor(R.color.dark_gray));

                    tagAppName.setText("Fashion House");
                    tagAppName.setVisibility(View.VISIBLE);

                }

            }
        });
        recItemsList.setAdapter(adapterProduct);


        getProductList(SubCategoryID, BrandID, IntPage, mRanomNumer);


        recItemsList.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {


                if (page != last_size) {
                    int FinalOAgeSIze = page + 1;

                    getProductList(SubCategoryID, BrandID, FinalOAgeSIze, mRanomNumer);
                    Log.e("pageCheck", IntPage + "--" + last_size + "--" + FinalOAgeSIze);

                }
            }
        });


        return view;

    }


    private void setWishlist(String productId) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
// .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                UserSession.BASEURL + "add-to-wishlist",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

// progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                            } else {
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
// progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductID", productId);
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
// params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
//adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


    private void getProductList(String SubCategoryID, String BrandID, int intPage, String mRanomNumer) {

        Log.e("asData", SubCategoryID + "--" + BrandID);
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
// .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "search-products?"
                        + "SubCategoryID=" + SubCategoryID
                        + "&BrandID=" + BrandID + "&Token="
                        + session.getFirbaseDeviceToken() + "&Random=" + mRanomNumer + "&page=" + intPage,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

// progressDialog.dismiss();
                        Log.e("stringRa", mRanomNumer + "--");


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseItems", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");
                                last_size = object.getInt("last_page");

                                JSONArray jsonArray = object.getJSONArray("data");
// JSONArray jsonArray_new = shuffleJsonArray(jsonArray);

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object1 = jsonArray.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object1.getString("ProductID"));
                                    productListModel.setProductTitle(object1.getString("ProductTitle"));
                                    productListModel.setBrandID(object1.getString("BrandID"));
                                    productListModel.setPrice(object1.getString("Price"));
                                    productListModel.setOriginalPrice(object1.getString("OriginalPrice"));
                                    productListModel.setQuantity(object1.getString("Quantity"));
                                    productListModel.setProductImage(object1.getString("ProductImage"));
                                    productListModel.setIsNewIn(object1.getString("IsNewIn"));
                                    productListModel.setIsSale(object1.getString("IsSale"));


                                    double actual = Double.parseDouble(object1.getString("OriginalPrice"));
                                    double discountDouble = Double.parseDouble(object1.getString("Price"));


                                    double total = (actual - discountDouble) * 100 / actual;
                                    int disCount = (int) total;

                                    productListModel.setDisCount(String.valueOf(disCount));

                                    if (object1.getString("IsWishlist").equals("0")) {
                                        productListModel.setWishlist(false);
                                    } else if (object1.getString("IsWishlist").equals("1")) {
                                        productListModel.setWishlist(true);
                                    }

                                    productListModelArrayList.add(productListModel);


                                }
// shuffleList(productListModelArrayList);

                                adapterProduct.notifyDataSetChanged();


                                if (productListModelArrayList.isEmpty()) {
                                    cartEmpty.setVisibility(View.VISIBLE);
                                }


                            } else {
                                Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
// progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
// params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
// params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
//adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }

    static <ProductListModel> void shuffleList(ArrayList<ProductListModel> list) {
        ArrayList<ProductListModel> temp = new ArrayList<ProductListModel>(list);
        Random rand = new Random();

        for (int i = 0; i < list.size(); i++) {
            int newPos = rand.nextInt(list.size());
            while (newPos == i || temp.get(newPos) == null) {
                newPos = rand.nextInt(list.size());
            }
            list.set(i, temp.get(newPos));
            temp.set(newPos, null);
        }
    }

    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
// Implementing Fisher–Yates shuffle
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
// Simple swap
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }


    @Override
    public void onResume() {
        super.onResume();
        last_size = 0;
        IntPage = 1;
        productListModelArrayList.clear();

    //    mRanomNumer = randomString(5);
     //   Log.e("stringRa", mRanomNumer + "--");


/*image1.setImageResource(R.drawable.home);
image2.setImageResource(R.drawable.search_active);
image3.setImageResource(R.drawable.wishlist);
image4.setImageResource(R.drawable.user);
image5.setImageResource(R.drawable.bag);


text1.setTextColor(getResources().getColor(R.color.dark_gray));
text2.setTextColor(getResources().getColor(R.color.pink));
text3.setTextColor(getResources().getColor(R.color.dark_gray));
text4.setTextColor(getResources().getColor(R.color.dark_gray));
text5.setTextColor(getResources().getColor(R.color.dark_gray));*/


/* tagAppName.setVisibility(View.VISIBLE);
tagAppName.setText("Search");*/

        tagAppName.setText("Products > " + Title);
        tagAppName.setVisibility(View.VISIBLE);


    }

    static final String AB = "0123456789";
    static SecureRandom rnd = new SecureRandom();

    public String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


}