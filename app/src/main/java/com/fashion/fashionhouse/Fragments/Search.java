package com.fashion.fashionhouse.Fragments;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Activity.ChangePassword;
import com.fashion.fashionhouse.Activity.MainActivity;
import com.fashion.fashionhouse.Activity.PlaceOrder;
import com.fashion.fashionhouse.Activity.Splash;
import com.fashion.fashionhouse.Activity.WelcomeScreen;
import com.fashion.fashionhouse.Adapter.AdapterMainCategory;
import com.fashion.fashionhouse.Adapter.AdapterSearchList;
import com.fashion.fashionhouse.Adapter.AdapterSubCategory;
import com.fashion.fashionhouse.Model.InterestedModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.EndlessRecyclerViewScrollListener;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.fashion.fashionhouse.Utils.WrapContentLinearLayoutManager;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Search extends Fragment {

    private RecyclerView recSubCategory, recMainCategory;
    private AdapterSubCategory adapterSubCategory;
    private AdapterMainCategory adapterMainCategory;


    private RequestQueue requestQueue;
    private UserSession session;

    private ArrayList<InterestedModel> modelArrayList = new ArrayList<InterestedModel>();
    private ArrayList<InterestedModel> modelSubCatArray = new ArrayList<>();


    RecyclerView recSearch;
    ArrayList<String> TitleArray = new ArrayList<>();
    ArrayList<String> IdArray = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    EditText etSearch;

    private ImageView no_product_image;

    private String mainCategoryId = "";

    private AdapterSearchList adapterSearchList;

    private Timer timer = new Timer();
    private final long DELAY = 1500; // milliseconds


    private String Title;
    public int last_size = 0;
    public int IntPage = 1;
    private LinearLayoutManager linearLayoutManager;
    private WrapContentLinearLayoutManager wrapContentLinearLayoutManager;

    private String currentSearchText = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_search, container, false);

        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue


        no_product_image = view.findViewById(R.id.no_product_image);


        recMainCategory = view.findViewById(R.id.recMainCategory);
        recMainCategory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapterMainCategory = new AdapterMainCategory(getContext(), modelArrayList, new AdapterMainCategory.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
// getSubCategory(modelArrayList.get(item).getCategoryID());
                adapterMainCategory.filterList(item);

                mainCategoryId = modelArrayList.get(item).getCategoryID();

                session.setIsMenwomen(item +"");
            }
        });
        recMainCategory.setAdapter(adapterMainCategory);

        adapterMainCategory.notifyDataSetChanged();

        recSubCategory = view.findViewById(R.id.recSubCategory);
        recSubCategory.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterSubCategory = new AdapterSubCategory(getContext(), modelSubCatArray, new AdapterSubCategory.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                if (modelSubCatArray.get(item).getIsBrands().equals("0")) {

                    ProductListFragment fragobj = new ProductListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("BrandID", "");
                    bundle.putString("SubCategoryID", modelSubCatArray.get(item).getCategoryID());
                    bundle.putString("Title", modelSubCatArray.get(item).getCategoryName());
                    fragobj.setArguments(bundle);
                    replaceFragment(R.id.fragmentLinearHome,fragobj,"ProductListFragment",null);

                    Log.e("idCategory", modelSubCatArray.get(item).getCategoryID() + "--");

                } else if (modelSubCatArray.get(item).getIsBrands().equals("1")){

                    SearchInner searchInner = new SearchInner(modelSubCatArray.get(item).getCategoryID());
                    replaceFragment(R.id.fragmentLinearHome, searchInner, "search_inner", null);
                }

            }
        });
        recSubCategory.setAdapter(adapterSubCategory);



        getCategory();



        recSearch = view.findViewById(R.id.recSearch);
        etSearch = view.findViewById(R.id.etSearch);

        linearLayoutManager = new LinearLayoutManager(getContext());

    //    recSearch.setLayoutManager(linearLayoutManager);
        wrapContentLinearLayoutManager = new WrapContentLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recSearch.setLayoutManager(wrapContentLinearLayoutManager);

        adapterSearchList = new AdapterSearchList(getContext(), TitleArray, new AdapterSearchList.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", IdArray.get(position));
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"ItemsDetails",null);
            }
        });
        recSearch.setAdapter(adapterSearchList);
        adapterSearchList.notifyDataSetChanged();

        recSearch.addOnScrollListener(new EndlessRecyclerViewScrollListener(wrapContentLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {

                IntPage = page;
                Log.e("pageCheck", IntPage + "--" + last_size);

                if (page!=last_size){
                    int FinalOAgeSIze = page + 1;
                    getSearch(currentSearchText, FinalOAgeSIze);

                }
            }
        });

/* arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, TitleArray);
recSearch.setAdapter(arrayAdapter);

recSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
@Override
public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



}
});*/


        etSearch.addTextChangedListener(new TextWatcher() {

            boolean isTyping = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//arrayAdapter.getFilter().filter(s);

                Log.e("COunnnnt", count +"--" + start +"--" + before);

            }
            @Override
            public void afterTextChanged(Editable s) {

                Log.d("", "");
                if(!isTyping) {
// Log.d(TAG, "started typing");
// Send notification for start typing event
                    isTyping = true;
                }
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                isTyping = false;
                                Log.e("Chhhheckkk", "stopped typing");

                                TitleArray.clear();
                                IdArray.clear();
                                IntPage =1;
                                getSearch(s.toString(), IntPage);

                                currentSearchText = s.toString();

                            }
                        },
                        DELAY
                );

                if(s.toString().isEmpty()){
                    recSearch.setVisibility(View.GONE);
                }else {
                    recSearch.setVisibility(View.VISIBLE);
                }

            }
        });

        getSubCategory();

        return view;
    }


    private void getCategory() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
// .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-categories" +
                "?Token=" + session.getFirbaseDeviceToken(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

// progressDialog.dismiss();
                        modelArrayList.clear();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    InterestedModel interestedModel = new InterestedModel();
                                    interestedModel.setCategoryID(object.getString("CategoryID"));
                                    interestedModel.setCategoryName(object.getString("CategoryName"));

                                    modelArrayList.add(interestedModel);
                                }

                                mainCategoryId = modelArrayList.get(0).getCategoryID();


/* if (session.getIsMenwomen().equals("0")){
getSubCategory(modelArrayList.get(0).getCategoryID());
adapterMainCategory.filterList(0);

mainCategoryId = modelArrayList.get(0).getCategoryID();

session.setIsMenwomen(0 +"");
} else if (session.getIsMenwomen().equals("1")){
getSubCategory(modelArrayList.get(1).getCategoryID());
adapterMainCategory.filterList(1);

mainCategoryId = modelArrayList.get(1).getCategoryID();

session.setIsMenwomen(1 +"");
}*/


// getSubCategory("1");

                                adapterMainCategory.notifyDataSetChanged();




                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
// Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
// progressDialog.dismiss();
                        JSONObject data = null;


                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(getContext(), getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
// params.put("services[]", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
// params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
//adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void getSearch(String search, int intPage) {
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL
                + "search-products?Search=" + search
                + "&Token=" + session.getFirbaseDeviceToken()
                + "&page=" + intPage,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");
                                last_size = object.getInt("last_page");


                                JSONArray jsonArray = object.getJSONArray("data");

                                Log.e("sizeeChekki", last_size + "--" + jsonArray.length());

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object1 = jsonArray.getJSONObject(i);
                                    String[] strings = object1.getString("IDTitle").split("--");
                                    TitleArray.add(strings[1]);
                                    IdArray.add(strings[0]);
                                }


                                if (TitleArray.isEmpty()){
                                    no_product_image.setVisibility(View.VISIBLE);
                                    recSearch.setVisibility(View.GONE);
                                } else if (!TitleArray.isEmpty()){
                                    no_product_image.setVisibility(View.GONE);
                                }

                                adapterSearchList.notifyDataSetChanged();


                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(getContext(), getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
// params.put("services[]", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
// params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
//adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                3600,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void getSubCategory() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "get-sub-categories" + "?Token=" + session.getFirbaseDeviceToken(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        modelSubCatArray.clear();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    InterestedModel interestedModel = new InterestedModel();
                                    interestedModel.setCategoryID(object.getString("SubCategoryID"));
                                    interestedModel.setCategoryName(object.getString("SubCategoryName"));
                                    interestedModel.setIsBrands(object.getString("IsBrands"));

                                    modelSubCatArray.add(interestedModel);
                                }


// shuffleList(modelSubCatArray);

                                adapterSubCategory.notifyDataSetChanged();


                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(getContext(), getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
// params.put("services[]", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
// params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
//adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    static <InterestedModel> void shuffleList(ArrayList<InterestedModel> list) {
        ArrayList<InterestedModel> temp = new ArrayList<InterestedModel>(list);
        Random rand = new Random();

        for (int i = 0; i < list.size(); i++) {
            int newPos = rand.nextInt(list.size());
            while (newPos == i||temp.get(newPos)==null) {
                newPos = rand.nextInt(list.size());
            }
            list.set(i, temp.get(newPos));
            temp.set(newPos,null);
        }
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }


    @Override
    public void onResume() {
        super.onResume();

        MainActivity.tagAppName.setVisibility(View.VISIBLE);
        MainActivity.tagAppName.setText("Search");
        MainActivity.exitBtnHome.setVisibility(View.VISIBLE);

        TitleArray.clear();
        IdArray.clear();
        recSearch.setVisibility(View.GONE);
        etSearch.setText("");

    }


}