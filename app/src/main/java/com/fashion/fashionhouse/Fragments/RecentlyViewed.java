package com.fashion.fashionhouse.Fragments;

import static com.fashion.fashionhouse.Activity.MainActivity.image1;
import static com.fashion.fashionhouse.Activity.MainActivity.image2;
import static com.fashion.fashionhouse.Activity.MainActivity.image3;
import static com.fashion.fashionhouse.Activity.MainActivity.image4;
import static com.fashion.fashionhouse.Activity.MainActivity.image5;
import static com.fashion.fashionhouse.Activity.MainActivity.text1;
import static com.fashion.fashionhouse.Activity.MainActivity.text2;
import static com.fashion.fashionhouse.Activity.MainActivity.text3;
import static com.fashion.fashionhouse.Activity.MainActivity.text4;
import static com.fashion.fashionhouse.Activity.MainActivity.text5;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Activity.MainActivity;
import com.fashion.fashionhouse.Adapter.AdapterRecentlyVIewed;
import com.fashion.fashionhouse.Adapter.AdapterWishlist;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RecentlyViewed extends Fragment {


    private RecyclerView recWishList;
    private AdapterRecentlyVIewed adapterRecentlyVIewed;


    private UserSession session;
    private RequestQueue requestQueue;

    private ArrayList<ProductListModel> productListModelArrayList = new ArrayList<>();
    private TextView no_items;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_recently_viewed, container, false);


        MainActivity.tagAppName.setText("Recently Viewed");
        MainActivity.tagAppName.setVisibility(View.VISIBLE);


        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue



        no_items = view.findViewById(R.id.no_items);
        recWishList = view.findViewById(R.id.recWishList);
        recWishList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapterRecentlyVIewed = new AdapterRecentlyVIewed(getContext(), productListModelArrayList, new AdapterRecentlyVIewed.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayList.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);


               /* startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayList.get(item).getProductID()));
*/
            }

            @Override
            public void onItemClickRemove(int item) {
                removeFromWishlist(productListModelArrayList.get(item).getFavouriteProductID());

                productListModelArrayList.remove(item);

                adapterRecentlyVIewed.notifyDataSetChanged();

            }
        });
        recWishList.setAdapter(adapterRecentlyVIewed);

        Log.e("tokenAPI", session.getKeyApitoken() + " token");

        getWishlist();


        return view;

    }



    private void getWishlist() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "home?Token=" + session.getFirbaseDeviceToken(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        productListModelArrayList.clear();
                        Log.e("ResponseWish", response.toString() + "--");


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseHome", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                JSONArray jsonArray4 = object.getJSONArray("recently_viewed");


                                for (int i = 0; i < jsonArray4.length(); i++){
                                    JSONObject object1 = jsonArray4.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object1.getString("ProductID"));
                                    productListModel.setProductTitle(object1.getString("ProductTitle"));
                                    productListModel.setBrandID(object1.getString("BrandID"));
                                    productListModel.setPrice(object1.getString("Price"));
                                    productListModel.setOriginalPrice(object1.getString("OriginalPrice"));
                                    productListModel.setQuantity(object1.getString("Quantity"));
                                    productListModel.setProductImage(object1.getString("ProductImage"));
                                    productListModel.setIsNewIn(object1.getString("IsNewIn"));
                                    productListModel.setIsSale(object1.getString("IsSale"));


                                    double actual = Double.parseDouble(object1.getString("OriginalPrice"));
                                    double discountDouble = Double.parseDouble(object1.getString("Price"));


                                    double total = (actual - discountDouble) * 100/actual;
                                    int disCount = (int) total;


                                    productListModel.setDisCount(String.valueOf(disCount));

                                    productListModelArrayList.add(productListModel);
                                }
                                adapterRecentlyVIewed.notifyDataSetChanged();


                                if (productListModelArrayList.isEmpty()){
                                    no_items.setVisibility(View.VISIBLE);
                                }else {
                                    no_items.setVisibility(View.GONE);
                                }



                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
        //        params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void removeFromWishlist(String favouriteId) {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                UserSession.BASEURL + "remove-from-wishlist",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                if (productListModelArrayList.isEmpty()){
                                    no_items.setVisibility(View.VISIBLE);
                                }else {
                                    no_items.setVisibility(View.GONE);
                                }

                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();


                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("FavouriteProductID", favouriteId);
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
         //       params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }


}