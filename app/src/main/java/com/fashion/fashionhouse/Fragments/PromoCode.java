package com.fashion.fashionhouse.Fragments;

import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fashion.fashionhouse.R;

public class PromoCode extends Fragment {

    private TextView promoText1, promoText2, promoText3;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_promo_code, container, false);


        promoText1 = view.findViewById(R.id.promoText1);
        promoText2 = view.findViewById(R.id.promoText2);
        promoText3 = view.findViewById(R.id.promoText3);

        promoText1.setText(Html.fromHtml("<ul>\n" +
                "<li>\n" +
                "<pre> One discount/promo code per code order (applies to our free delivery codes too).</pre>\n" +
                "</li>\n" +
                "</ul>"));

        promoText2.setText(Html.fromHtml("<ul>\n" +
                "<li> Promo codes can't be used on Gift vouchers and Premier delivery.</li>\n" +
                "</ul>"));

        promoText3.setText(Html.fromHtml("<ul>\n" +
                "<li> Terms and conditions are vary per promo code.</li>\n" +
                "</ul>"));



        return view;
    }


}