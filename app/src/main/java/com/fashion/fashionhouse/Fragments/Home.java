package com.fashion.fashionhouse.Fragments;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.fashion.fashionhouse.Activity.MainActivity;
import com.fashion.fashionhouse.Activity.Splash;
import com.fashion.fashionhouse.Activity.WelcomeScreen;
import com.fashion.fashionhouse.Adapter.AdapterHorizontalHome;
import com.fashion.fashionhouse.Adapter.AdapterNewIn;
import com.fashion.fashionhouse.Adapter.AdapterNewInTrend;
import com.fashion.fashionhouse.Adapter.AdapterRecommended;
import com.fashion.fashionhouse.Adapter.ViewPagerRecommended;
import com.fashion.fashionhouse.Adapter.ViewPagerWeek;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.Model.SizeColorModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Home extends Fragment {

    private DotsIndicator indicator, worm_dots_indicator2;
    private ViewPagerRecommended viewPagerRecommended;
    private ViewPagerWeek viewPagerWeek;
    private ArrayList<SizeColorModel> mDatasetServices = new ArrayList<>();

    private RecyclerView recNewIn;
    private AdapterRecommended adapterRecommended;
    private AdapterHorizontalHome adapterHorizontalHome;
    private AdapterNewInTrend adapterTrending;
    private AdapterNewIn adapterNewIn;

    private RecyclerView recHome, rec2;


    private UserSession session;
    private RequestQueue requestQueue;

    private TextView bannerText, DiscountOffTitle, DiscountOffSubTitle, productName, HomeFooterText, TredingTitle, TrendingSubtitle;

    private TextView RecommendationSubTitle, RecommendationTitle;
    private ImageView imageHome, image1;


    private ArrayList<ProductListModel> productListModelArrayList = new ArrayList<>();
    private ArrayList<ProductListModel> productListModelArrayListTrending = new ArrayList<>();
    private ArrayList<ProductListModel> productListModelArrayListRecommended = new ArrayList<>();
    private ArrayList<ProductListModel> productListModelArrayListRecent = new ArrayList<>();


    private String productIdOneItem = "";

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_home, container, false);

        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue


        bannerText = view.findViewById(R.id.bannerText);
        DiscountOffTitle = view.findViewById(R.id.DiscountOffTitle);
        DiscountOffSubTitle = view.findViewById(R.id.DiscountOffSubTitle);
        imageHome = view.findViewById(R.id.imageHome);
        image1 = view.findViewById(R.id.image1);
        productName = view.findViewById(R.id.productName);
        HomeFooterText = view.findViewById(R.id.HomeFooterText);
        TredingTitle = view.findViewById(R.id.TredingTitle);
        TrendingSubtitle = view.findViewById(R.id.TrendingSubtitle);
        RecommendationTitle = view.findViewById(R.id.RecommendationTitle);
        RecommendationSubTitle = view.findViewById(R.id.RecommendationSubTitle);



        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        indicator = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator);
        viewPagerRecommended = new ViewPagerRecommended(getContext(), productListModelArrayList, new ViewPagerRecommended.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayList.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);
            }
        });
        mViewPager.setAdapter(viewPagerRecommended);
        indicator.setViewPager(mViewPager);


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == productListModelArrayList.size()) {
                    currentPage = 0;
                }
                mViewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);




        recHome = view.findViewById(R.id.recHome);
        LinearLayoutManager lay_man = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recHome.setLayoutManager(lay_man);
        adapterTrending = new AdapterNewInTrend(getContext(), productListModelArrayListTrending, new AdapterNewInTrend.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
/*
                startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayListTrending.get(item).getProductID()))*/;
/*

                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayListTrending.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);
*/


            }
        });
        recHome.setAdapter(adapterTrending);
        adapterTrending.notifyDataSetChanged();




        rec2 = view.findViewById(R.id.rec2);
        LinearLayoutManager lay_man2 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rec2.setLayoutManager(lay_man2);
        adapterNewIn = new AdapterNewIn(getContext(), productListModelArrayListRecommended, new AdapterNewIn.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

              /*  startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayListRecommended.get(item).getProductID()));*/


                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayListRecommended.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);

            }
        });
        rec2.setAdapter(adapterNewIn);
        adapterNewIn.notifyDataSetChanged();



        // for viewpager 2
        ViewPager viewPager2 = (ViewPager) view.findViewById(R.id.viewPager2);
        worm_dots_indicator2 = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator2);
        viewPagerWeek = new ViewPagerWeek(getContext(), productListModelArrayListRecommended, new ViewPagerWeek.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

               /* startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayListRecommended.get(item).getProductID()));
*/
                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayListRecommended.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);


            }
        });
        viewPager2.setAdapter(viewPagerWeek);
        worm_dots_indicator2.setViewPager(viewPager2);
        viewPagerWeek.notifyDataSetChanged();


        // for new items recycler
        recNewIn = view.findViewById(R.id.recNewIn);
        LinearLayoutManager lay_man3 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recNewIn.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapterHorizontalHome = new AdapterHorizontalHome(getContext(), productListModelArrayListRecent, new AdapterHorizontalHome.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

               /* startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productListModelArrayListRecent.get(item).getProductID()));*/

                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayListRecent.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);

            }
        });
        recNewIn.setAdapter(adapterHorizontalHome);




        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*
                startActivity(new Intent(getContext(), ItemsDetail.class)
                        .putExtra("ProductID", productIdOneItem));*/

                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productIdOneItem);
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);
            }
        });

        view.findViewById(R.id.shopNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productIdOneItem);
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);
            }
        });



        view.findViewById(R.id.recentlyViewed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                RecentlyViewed recentlyViewed = new RecentlyViewed();
                replaceFragment(R.id.fragmentLinearHome, recentlyViewed, "recently_viewed", null);

            }
        });


        getData();


        return view;

    }



    private void getData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
           //     .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "home?Token=" + session.getFirbaseDeviceToken(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        productListModelArrayList.clear();
                        productListModelArrayListTrending.clear();
                        productListModelArrayListRecommended.clear();
                        productListModelArrayListRecent.clear();
          //              progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseHome", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");


                                Glide.with(getContext()).load(object.getString("DiscountOffImage")).into(imageHome);
                                bannerText.setText(object.getString("ShippingText"));

                                if (object.getString("DiscountOffTitle").equals("null")){
                                    DiscountOffTitle.setVisibility(View.GONE);
                                } else {
                                    DiscountOffTitle.setText(object.getString("DiscountOffTitle"));
                                }

                                if (object.getString("DiscountOffSubTitle").equals("null")){
                                    DiscountOffSubTitle.setVisibility(View.GONE);
                                } else {
                                    DiscountOffSubTitle.setText(object.getString("DiscountOffSubTitle"));
                                }

                                DiscountOffTitle.setTextColor(Color.parseColor(object.getString("FontColor")));
                                DiscountOffSubTitle.setTextColor(Color.parseColor(object.getString("FontColor")));
                                String alpha = object.getString("Opacity");
                                imageHome.setAlpha(Float.parseFloat(alpha));


                               /* ValueAnimator animator = new ValueAnimator();
                                animator.setDuration(4000);
                                animator.setObjectValues("Ecl pse", "Eclipse");

                                animator.setEvaluator(new TypeEvaluator<CharSequence>()
                                {
                                    @Override
                                    public CharSequence evaluate(float fraction, CharSequence startValue, CharSequence endValue)
                                    {
                                        float relativeSize = 4 - 3 * fraction;
                                        Spannable span = new SpannableString("Eclipse");
                                        span.setSpan(new RelativeSizeSpan(relativeSize), 3, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                        return span;
                                    }
                                });
                                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
                                {
                                    @Override
                                    public void onAnimationUpdate(ValueAnimator animation)
                                    {
                                        DiscountOffTitle.setText((CharSequence)animation.getAnimatedValue());
                                    }
                                });
                                animator.start();*/





                                HomeFooterText.setText(object.getString("HomeFooterText"));
                                TredingTitle.setText(object.getString("TredingTitle"));
                                TrendingSubtitle.setText(object.getString("TrendingSubtitle"));
                                RecommendationSubTitle.setText(object.getString("RecommendationSubTitle"));
                                RecommendationTitle.setText(object.getString("RecommendationTitle"));


                                //
                                JSONArray jsonArray = object.getJSONArray("new_arrival");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object1 = jsonArray.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object1.getString("ProductID"));
                                    productListModel.setProductTitle(object1.getString("ProductTitle"));
                                    productListModel.setProductImage(object1.getString("ProductImage"));

                                    productListModelArrayList.add(productListModel);
                                }
                                viewPagerRecommended.notifyDataSetChanged();


                                //
                                JSONObject object1 = object.getJSONObject("product");

                                Glide.with(getContext()).load(object1.getString("ProductImage")).into(image1);
                                productName.setText(object1.getString("ProductTitle"));

                                productIdOneItem = object1.getString("ProductID");



                                JSONArray jsonArray1 = object.getJSONArray("trending");

                                for (int i = 0; i < jsonArray1.length(); i++){
                                    JSONObject object2 = jsonArray1.getJSONObject(0);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object2.getString("TrendingID"));
                        //            productListModel.setBrandName(object2.getString("BrandName"));
                                    productListModel.setProductTitle(object2.getString("TrendingName"));
                                    productListModel.setProductType(object2.getString("MediaType"));
                                    productListModel.setProductImage(object2.getString("TrendingImageVideo"));

                                    productListModelArrayListTrending.add(productListModel);
                                }
                                adapterTrending.notifyDataSetChanged();

                                Log.e("CheckItems", productListModelArrayListTrending.size() + "---");


                                //
/*
                                JSONArray jsonArray2 = object.getJSONArray("recommended_for_you");
*/
                                JSONArray jsonArray2 = object.getJSONArray("recommended_for_you");

                                for (int i = 0; i < jsonArray2.length(); i++){
                                    JSONObject object2 = jsonArray2.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object2.getString("ProductID"));
                                    productListModel.setProductImage(object2.getString("ProductImage"));
                                    productListModel.setProductTitle(object2.getString("ProductTitle"));
                                    productListModel.setBrandName(object2.getString("BrandName"));

                                    productListModelArrayListRecommended.add(productListModel);
                                }
                                viewPagerWeek.notifyDataSetChanged();




                                //
                                JSONArray jsonArray4 = object.getJSONArray("recently_viewed");

                                for (int i = 0; i < jsonArray4.length(); i++){
                                    JSONObject object2 = jsonArray4.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object2.getString("ProductID"));
                                    productListModel.setProductTitle(object2.getString("ProductTitle"));
                                    productListModel.setBrandID(object2.getString("BrandID"));
                                    productListModel.setPrice(object2.getString("Price"));
                                    productListModel.setOriginalPrice(object2.getString("OriginalPrice"));
                                    productListModel.setQuantity(object2.getString("Quantity"));
                                    productListModel.setProductImage(object2.getString("ProductImage"));
                                    productListModel.setIsNewIn(object2.getString("IsNewIn"));
                                    productListModel.setIsSale(object2.getString("IsSale"));

                                    productListModelArrayListRecent.add(productListModel);
                                }
                                adapterHorizontalHome.notifyDataSetChanged();
                                adapterNewIn.notifyDataSetChanged();



                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                 //           Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("messageE", e.getMessage() +"--");
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
               //         progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //   params.put("services[]", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }




    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }


    @Override
    public void onResume() {
        super.onResume();
        MainActivity.tagAppName.setVisibility(View.VISIBLE);
        MainActivity.tagAppName.setText(getResources().getString(R.string.app_name));
        MainActivity.exitBtnHome.setVisibility(View.VISIBLE);

    }

}