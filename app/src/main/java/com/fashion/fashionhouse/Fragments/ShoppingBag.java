package com.fashion.fashionhouse.Fragments;

import static com.fashion.fashionhouse.Activity.MainActivity.image1;
import static com.fashion.fashionhouse.Activity.MainActivity.image2;
import static com.fashion.fashionhouse.Activity.MainActivity.image3;
import static com.fashion.fashionhouse.Activity.MainActivity.image4;
import static com.fashion.fashionhouse.Activity.MainActivity.image5;
import static com.fashion.fashionhouse.Activity.MainActivity.text1;
import static com.fashion.fashionhouse.Activity.MainActivity.text2;
import static com.fashion.fashionhouse.Activity.MainActivity.text3;
import static com.fashion.fashionhouse.Activity.MainActivity.text4;
import static com.fashion.fashionhouse.Activity.MainActivity.text5;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Activity.MainActivity;
import com.fashion.fashionhouse.Activity.PlaceOrder;
import com.fashion.fashionhouse.Activity.AsGuestFrom;
import com.fashion.fashionhouse.Adapter.AdapterBag;
import com.fashion.fashionhouse.Model.CategoryDetailsModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.Database;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShoppingBag extends Fragment {

    private RecyclerView recListBag;
    private AdapterBag adapterBag;

    public ArrayList<CategoryDetailsModel> detailsModelArrayList = new ArrayList<>();


    private Database dbHelper;

    private UserSession session;
    private RequestQueue requestQueue;

    private TextView totalPayment;

    private RelativeLayout checkoutRelative, cartEmpty;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_shopping_bag, container, false);

        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue
        cartEmpty = view.findViewById(R.id.cartEmpty);
        checkoutRelative = view.findViewById(R.id.checkoutRelative);
        dbHelper = new Database(getContext());

        detailsModelArrayList = dbHelper.getAllUser();


        for (int h = 0; h < detailsModelArrayList.size(); h++) {
            Log.e("dataArrayCart", detailsModelArrayList.get(h).getAvailable_stock_qty() + "--");
        }


        totalPayment = view.findViewById(R.id.totalPayment);


        if (detailsModelArrayList.isEmpty()){
            cartEmpty.setVisibility(View.VISIBLE);
            checkoutRelative.setVisibility(View.GONE);
        }


        int totalInt1 = 0;

        for (int i = 0; i < detailsModelArrayList.size(); i++) {
            totalInt1 = totalInt1 +  (Integer.parseInt(detailsModelArrayList.get(i).getBought()) *
                    Integer.parseInt(detailsModelArrayList.get(i).getSell_price()));
        }

        Log.e("price", String.valueOf(totalInt1) +"");

        totalPayment.setText(getContext().getResources().getString(R.string.currency_symbol) + totalInt1);




        view.findViewById(R.id.btnCheckOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          //      startActivity(new Intent(getContext(), PlaceOrder.class));

                detailsModelArrayList = dbHelper.getAllUser();

                if (session.isLoggedIn()) {
                    startActivity(new Intent(getContext(), PlaceOrder.class));

                } else {
                    BottomSheetDialog dialog = new BottomSheetDialog(getContext(), R.style.BottomSheetDialog);
                    dialog.setContentView(R.layout.bottom_dialog_sign_gs);

                    TextView btnSignIn = dialog.findViewById(R.id.btnSignIn);
                    TextView btnAsAGuest = dialog.findViewById(R.id.btnAsAGuest);

                    btnSignIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            MainActivity.IsFromShopingBag = true;
                            Me me = new Me("1");
                            replaceFragment(R.id.fragmentLinearHome, me, "me", null);


                            image1.setImageResource(R.drawable.home);
                            image2.setImageResource(R.drawable.search);
                            image3.setImageResource(R.drawable.wishlist);
                            image4.setImageResource(R.drawable.user_active);
                            image5.setImageResource(R.drawable.bag);


                            text1.setTextColor(getResources().getColor(R.color.dark_gray));
                            text2.setTextColor(getResources().getColor(R.color.dark_gray));
                            text3.setTextColor(getResources().getColor(R.color.dark_gray));
                            text4.setTextColor(getResources().getColor(R.color.pink));
                            text5.setTextColor(getResources().getColor(R.color.dark_gray));

                            MainActivity.tagAppName.setText("Fashion House");
                            MainActivity.tagAppName.setVisibility(View.VISIBLE);

                        }
                    });

                    btnAsAGuest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            startActivity(new Intent(getContext(), AsGuestFrom.class));
                        }
                    });

                    dialog.show();
                }

                /*for (int i = 0; i < detailsModelArrayList.size(); i++) {
                    Log.e("plus", detailsModelArrayList.get(i).getBought() +"" + detailsModelArrayList.get(i).getDeal_id());
                }*/
            }
        });



        recListBag = view.findViewById(R.id.recListBag);
        recListBag.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterBag = new AdapterBag(getContext(), detailsModelArrayList, new AdapterBag.OnItemClickListener() {

            @Override
            public void onItemClick(int pos) {

                Log.e("getData", detailsModelArrayList.get(pos).getDeal_id() + "--" + detailsModelArrayList.get(pos).getVariantID() + "--" +
                        detailsModelArrayList.get(pos).getQuantity());

                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", detailsModelArrayList.get(pos).getDeal_id());
                bundle.putString("InBag", "1");
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);
            }


            @Override
            public void onItemClickSaveLater(String productId, int pos) {
                moveToWishlist(productId);
                detailsModelArrayList = dbHelper.getAllUser();

                dbHelper.removeCart(productId);
                detailsModelArrayList.remove(pos);
                detailsModelArrayList.clear();
                adapterBag.notifyDataSetChanged();

                detailsModelArrayList = dbHelper.getAllUser();
                int totalInt = 0;

                detailsModelArrayList = dbHelper.getAllUser();

                for (int i = 0; i < detailsModelArrayList.size(); i++) {
                    totalInt = totalInt +  (Integer.parseInt(detailsModelArrayList.get(i).getBought()) *
                            Integer.parseInt(detailsModelArrayList.get(i).getSell_price()));
                }

                Log.e("price", String.valueOf(totalInt) +"");

                totalPayment.setText(getContext().getResources().getString(R.string.currency_symbol) + totalInt);

                if (detailsModelArrayList.isEmpty()){
                    cartEmpty.setVisibility(View.VISIBLE);
                    checkoutRelative.setVisibility(View.GONE);
                }


                detailsModelArrayList = dbHelper.getAllUser();

                MainActivity.bagCount.setText(String.valueOf(detailsModelArrayList.size()));
            }

            @Override
            public void onItemClickPlus(int position, String quantity) {
                dbHelper.Update(detailsModelArrayList.get(position).getDeal_id(), quantity);

                int totalInt = 0;

                detailsModelArrayList = dbHelper.getAllUser();

                for (int i = 0; i < detailsModelArrayList.size(); i++) {
                    totalInt = totalInt +  (Integer.parseInt(detailsModelArrayList.get(i).getBought()) *
                            Integer.parseInt(detailsModelArrayList.get(i).getSell_price()));
                }

                Log.e("price", String.valueOf(totalInt) +"");

                totalPayment.setText(getContext().getResources().getString(R.string.currency_symbol) + totalInt);
            }

            @Override
            public void onItemClickMinus(int position, String quantity) {
                dbHelper.Update(detailsModelArrayList.get(position).getDeal_id(), quantity);

                int totalInt = 0;

                detailsModelArrayList = dbHelper.getAllUser();

                for (int i = 0; i < detailsModelArrayList.size(); i++) {
                    totalInt = totalInt +  (Integer.parseInt(detailsModelArrayList.get(i).getBought()) *
                            Integer.parseInt(detailsModelArrayList.get(i).getSell_price()));
                }


                Log.e("price", String.valueOf(totalInt) +"");

                totalPayment.setText(getContext().getResources().getString(R.string.currency_symbol) + totalInt);
            }
            @Override
            public void onItemDelete(String dealId, int pos) {

                detailsModelArrayList = dbHelper.getAllUser();

                dbHelper.removeCart(dealId);

                detailsModelArrayList.remove(pos);

                detailsModelArrayList.clear();

                adapterBag.notifyDataSetChanged();

                detailsModelArrayList = dbHelper.getAllUser();


                int totalInt = 0;

                for (int i = 0; i < detailsModelArrayList.size(); i++) {
                    totalInt = totalInt + (Integer.parseInt(detailsModelArrayList.get(i).getBought()) *
                            Integer.parseInt(detailsModelArrayList.get(i).getSell_price()));
                }

                Log.e("price", String.valueOf(totalInt) +"");

                totalPayment.setText(getContext().getResources().getString(R.string.currency_symbol) + totalInt);


                if (detailsModelArrayList.isEmpty()){
                    cartEmpty.setVisibility(View.VISIBLE);
                    checkoutRelative.setVisibility(View.GONE);
                }

                adapterBag.notifyDataSetChanged();

                MainActivity.bagCount.setText(""+detailsModelArrayList.size());
            }



        });
        recListBag.setAdapter(adapterBag);



        return view;

    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }



    private void moveToWishlist(String productId) {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                UserSession.BASEURL + "move-to-wishlist",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {



                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductID", productId);
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    @Override
    public void onResume() {
        super.onResume();

        detailsModelArrayList = dbHelper.getAllUser();
     //   bagCount.setText(String.valueOf(detailsModelArrayList.size()));
        adapterBag.notifyDataSetChanged();

        MainActivity.tagAppName.setVisibility(View.VISIBLE);
        MainActivity.tagAppName.setText("Shopping Bag");

    }


}