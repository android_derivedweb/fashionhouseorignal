package com.fashion.fashionhouse.Fragments;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Activity.MainActivity;
import com.fashion.fashionhouse.Adapter.AdapterSearchInner;
import com.fashion.fashionhouse.Model.InterestedModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchInner extends Fragment {

    private RecyclerView recSearchInner;
    private AdapterSearchInner adapterSearchInner;
    private String subId;
    private ArrayList<InterestedModel> interestedModelArrayList = new ArrayList<>();


    private UserSession session;
    private RequestQueue requestQueue;


    public SearchInner(String subId) {
        this.subId = subId;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_search_inner, container, false);

        MainActivity.tagAppName.setVisibility(View.VISIBLE);
        MainActivity.tagAppName.setText("Brands");



        session = new UserSession(getContext());
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue



        recSearchInner = view.findViewById(R.id.recSearchInner);
        recSearchInner.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterSearchInner = new AdapterSearchInner(getContext(), interestedModelArrayList, new AdapterSearchInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

               // startActivity(new Intent(getContext(), ProductList.class).putExtra("BrandID", interestedModelArrayList.get(item).getCategoryID()));



                ProductListFragment fragobj = new ProductListFragment();
                Bundle bundle = new Bundle();
                bundle.putString("BrandID", interestedModelArrayList.get(item).getCategoryID());
                bundle.putString("SubCategoryID", subId);

                bundle.putString("Title", interestedModelArrayList.get(item).getCategoryName());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);



            }
        });
        recSearchInner.setAdapter(adapterSearchInner);


        getSubCategory(subId);


        return view;

    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }



    private void getSubCategory(String subId) {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "get-brands",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        interestedModelArrayList.clear();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    InterestedModel interestedModel = new InterestedModel();
                                    interestedModel.setCategoryID(object.getString("BrandID"));
                                    interestedModel.setCategoryName(object.getString("BrandName"));

                                    interestedModelArrayList.add(interestedModel);
                                }
                                adapterSearchInner.notifyDataSetChanged();




                            }else {
                                Toast.makeText(getContext(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(getContext(), getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //   params.put("services[]", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



}