package com.fashion.fashionhouse.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;

import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class CheckoutPaymentPlan extends AppCompatActivity {

    private UserSession session;
    private RequestQueue requestQueue;

    private RadioButton fullPayment, partPayment;
    private EditText amountTotal;

    private String paymentType = "FullPayment";
    private TextView forTextpartPayment, textPart;

    private float amountFloat = 0;
    private float amountAfter30 = 0;

    private String BankAccountName = "";
    private String BankAccountNo = "";
    private String mOrderPlaceId = "";
    private String totalPayFinal = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_payment_plan);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        session = new UserSession(CheckoutPaymentPlan.this);
        requestQueue = Volley.newRequestQueue(CheckoutPaymentPlan.this);//Creating the RequestQueue


        BankAccountName = getIntent().getStringExtra("BankAccountName");
        BankAccountNo = getIntent().getStringExtra("BankAccountNo");
        mOrderPlaceId = getIntent().getStringExtra("mOrderPlaceId");
        totalPayFinal = getIntent().getStringExtra("totalPayFinal");


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        fullPayment = findViewById(R.id.fullPayment);
        partPayment = findViewById(R.id.partPayment);
        amountTotal = findViewById(R.id.amountTotal);
        forTextpartPayment = findViewById(R.id.forTextpartPayment);
        textPart = findViewById(R.id.textPart);

        fullPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    textPart.setVisibility(View.GONE);
                    partPayment.setChecked(false);
                    paymentType = "FullPayment";

                    amountTotal.setEnabled(false);
                }
            }
        });

        partPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    textPart.setVisibility(View.VISIBLE);
                    paymentType = "PartPayment";

                    amountTotal.setEnabled(true);
                    fullPayment.setChecked(false);

                    amountFloat = 90;

                    amountAfter30 = amountFloat * 30/100;

                    amountTotal.setText(new DecimalFormat("##.##").format(amountAfter30));
                }
            }
        });


        forTextpartPayment.setMovementMethod(LinkMovementMethod.getInstance());

        forTextpartPayment.setText(Html.fromHtml("<p>Customer agrees to pay the balance prior to<br />delivery or at the point of delivery. See our<br /><strong>Terms &amp; Condition</strong> for more information</p>"));

        findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                if (!amountTotal.getText().toString().isEmpty()){
                    float checkFlt = Float.parseFloat(amountTotal.getText().toString());

                    if (checkFlt < amountAfter30){
                        Toast.makeText(CheckoutPaymentPlan.this, "Part Payment must be at least N" + amountAfter30, Toast.LENGTH_SHORT).show();
                    } else {
                        openDialog();
                    }
                } else {
                    Toast.makeText(CheckoutPaymentPlan.this, "Please enter amount!", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void openDialog() {
        Dialog dialogForCity;
        dialogForCity = new Dialog(CheckoutPaymentPlan.this);
        dialogForCity.setContentView(R.layout.custom_dailog_bank_detail);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        TextView nameBankAccount = dialogForCity.findViewById(R.id.nameBankAccount);
        TextView numberBankAccount = dialogForCity.findViewById(R.id.numberBankAccount);
        TextView placeOrderId = dialogForCity.findViewById(R.id.placeOrderId);
        TextView textDialog1 = dialogForCity.findViewById(R.id.textDialog1);
        TextView whtaspp1Text = dialogForCity.findViewById(R.id.whtaspp1Text);
        TextView whtaspp2Text = dialogForCity.findViewById(R.id.whtaspp2Text);
        TextView email = dialogForCity.findViewById(R.id.email);
        TextView text = dialogForCity.findViewById(R.id.text);
        TextView timer = dialogForCity.findViewById(R.id.timer);

        text.setText(Html.fromHtml("<p>A confirmation e-mail wil be sent when your order has been confirmed and processed.<br />For inquiries regarding this transaction, please contact our<strong> Customer Service</strong> via any of the<br />chennels above.</p>"));

        nameBankAccount.setText(BankAccountName);
        numberBankAccount.setText(BankAccountNo);

        // for timer code
        LocalTime localTime = LocalTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm a");
        timer.setText(localTime.format(dateTimeFormatter));

        String time = localTime.format(dateTimeFormatter);
        String substring = time.substring(Math.max(time.length() - 2, 0));

        String[] merIdArray = time.split(":");

        int hour = Integer.parseInt(merIdArray[0]);;

        String minutes = String.valueOf(localTime.getMinute());

        if (hour == 11){
            if (substring.equals("PM")){
                substring = "AM";
                timer.setText("Pay before (" + "00:" + minutes + " " + substring + ")");
            } else if (substring.equals("AM")){
                substring = "PM";
                timer.setText("Pay before (" + "12:" + minutes + " " + substring + ")");
            }
        } else {
            timer.setText("Pay before (" + (hour + 1) + ":" + minutes + " " + substring + ")");
        }
        // timer code end


        dialogForCity.findViewById(R.id.emailCopied).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", email.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });

        dialogForCity.findViewById(R.id.whatsApp1Copied).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", whtaspp1Text.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });

        dialogForCity.findViewById(R.id.whatsApp2Copied).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", whtaspp2Text.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });

        dialogForCity.findViewById(R.id.copyBank).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", numberBankAccount.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });
        placeOrderId.setText("Order # "+ mOrderPlaceId);
        textDialog1.setText(totalPayFinal);


        dialogForCity.findViewById(R.id.btnSubmitBankDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(CheckoutPaymentPlan.this);
                builder1.setMessage("Are you sure you want to close?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        Html.fromHtml("<font color='#000000'>Yes</font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                startActivity(new Intent(CheckoutPaymentPlan.this,MainActivity.class));
                                finish();
                                dialogForCity.dismiss();
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        Html.fromHtml("<font color='#000000'>No</font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();



            }
        });

        dialogForCity.findViewById(R.id.copyOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", mOrderPlaceId);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();

            }
        });


        dialogForCity.show();
    }


}