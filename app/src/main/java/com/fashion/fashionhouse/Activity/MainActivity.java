package com.fashion.fashionhouse.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.fashion.fashionhouse.Fragments.Home;
import com.fashion.fashionhouse.Fragments.Me;
import com.fashion.fashionhouse.Fragments.Profile;
import com.fashion.fashionhouse.Fragments.Search;
import com.fashion.fashionhouse.Fragments.ShoppingBag;
import com.fashion.fashionhouse.Fragments.Wishlist;
import com.fashion.fashionhouse.Model.CategoryDetailsModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.Database;
import com.fashion.fashionhouse.Utils.UserSession;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private LinearLayout navLinear1, navLinear2, navLinear3, navLinear4, navLinear5;
    public static ImageView image1, image2, image3, image4, image5;
    public static TextView text1, text2, text3, text4, text5;

    public static TextView tagAppName;
    public static ImageView exitBtnHome;

    private UserSession session;
    public static CallbackManager mCallbackManager;
    public static boolean IsFromShopingBag = false;
    public static boolean IsForWishList = false;


    public static TextView bagCount;

    private Database database;
    private ArrayList<CategoryDetailsModel> categoryDetailsModels = new ArrayList<>();



    private String isUnauthenticate = "";

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        mCallbackManager = CallbackManager.Factory.create();
        session = new UserSession(MainActivity.this);

        Log.e("Token",session.getFirbaseDeviceToken());


        exitBtnHome = findViewById(R.id.exitBtnHome);
        exitBtnHome.setVisibility(View.VISIBLE);

        database = new Database(MainActivity.this);

        categoryDetailsModels = database.getAllUser();

        Log.e("deviceToken", session.getKeyApitoken() + "-");
        Log.e("firebaseToken", session.getFirbaseDeviceToken() + "-");

        bagCount = findViewById(R.id.bagCount);
        bagCount.setText(String.valueOf(categoryDetailsModels.size()));


        exitBtnHome.findViewById(R.id.exitBtnHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, WelcomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


      /*  // for unauthenticate
        isUnauthenticate = getIntent().getStringExtra("isUnAuthenticate");
        if (isUnauthenticate.equals("1")){
            Me me = new Me("1");
            replaceFragment(R.id.fragmentLinearHome, me, "me", null);
        } else if (isUnauthenticate.equals("0")){

        }*/

        Home home = new Home();
        replaceFragment(R.id.fragmentLinearHome, home, "home", null);

        setFooterNavigation();

    }




    private void setFooterNavigation() {

        navLinear1 = findViewById(R.id.navLinear1);
        navLinear2 = findViewById(R.id.navLinear2);
        navLinear3 = findViewById(R.id.navLinear3);
        navLinear4 = findViewById(R.id.navLinear4);
        navLinear5 = findViewById(R.id.navLinear5);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        text5 = findViewById(R.id.text5);


        tagAppName = findViewById(R.id.tagAppName);


        navLinear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Home home = new Home();
                replaceFragment(R.id.fragmentLinearHome, home, "home", null);

                image1.setImageResource(R.drawable.home_active);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.pink));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText(getResources().getString(R.string.app_name));
                exitBtnHome.setVisibility(View.VISIBLE);

            }
        });

        navLinear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Search search = new Search();
                replaceFragment(R.id.fragmentLinearHome, search, "search", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search_active);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.pink));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Search");
                exitBtnHome.setVisibility(View.VISIBLE);

            }
        });

        navLinear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Wishlist wishlist = new Wishlist();
                replaceFragment(R.id.fragmentLinearHome, wishlist, "wishlist", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist_active);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.pink));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Wishlist");
                exitBtnHome.setVisibility(View.VISIBLE);

            }
        });

        navLinear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (session.isLoggedIn()) {
                    Profile profile = new Profile();
                    replaceFragment(R.id.fragmentLinearHome, profile, "profile", null);

                } else {
                    Me me = new Me("0");
                    replaceFragment(R.id.fragmentLinearHome, me, "me", null);
                }

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user_active);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.pink));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText(getResources().getString(R.string.app_name));
                exitBtnHome.setVisibility(View.VISIBLE);

            }
        });


        navLinear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShoppingBag shoppingBag = new ShoppingBag();
                replaceFragment(R.id.fragmentLinearHome, shoppingBag, "Shopping_bag", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag_active);

                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.pink));

                tagAppName.setVisibility(View.VISIBLE);
                tagAppName.setText("Shopping Bag");
                exitBtnHome.setVisibility(View.VISIBLE);

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        categoryDetailsModels = database.getAllUser();
        bagCount.setText(String.valueOf(categoryDetailsModels.size()));

        tagAppName.setText(getResources().getString(R.string.app_name));
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

}