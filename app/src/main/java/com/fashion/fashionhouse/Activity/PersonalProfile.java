package com.fashion.fashionhouse.Activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PersonalProfile extends AppCompatActivity {



    private RequestQueue requestQueue;
    private UserSession mUsersession;
    private static TextView date;
    private TextView email;
    private EditText last_name;
    private EditText fs_name;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalprofile);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        requestQueue = Volley.newRequestQueue(PersonalProfile.this);//Creating the RequestQueue

        mUsersession = new UserSession(PersonalProfile.this);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        fs_name = findViewById(R.id.fs_name);
        last_name = findViewById(R.id.last_name);
        email = findViewById(R.id.email);
        date = findViewById(R.id.date);


        fs_name.setText(mUsersession.getKeyFirstName());
        last_name.setText(mUsersession.getKeyLastName());
        email.setText(mUsersession.getKeyEmail());
        date.setText(mUsersession.getKeyDateOfBirth());


        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });


        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fs_name.getText().toString().isEmpty()){
                    Toast.makeText(PersonalProfile.this,"Please enter your first name",Toast.LENGTH_LONG).show();
                }else if(last_name.getText().toString().isEmpty()){
                    Toast.makeText(PersonalProfile.this,"Please enter your last name",Toast.LENGTH_LONG).show();
                }else if(date.getText().toString().isEmpty()){
                    Toast.makeText(PersonalProfile.this,"Please enter your date",Toast.LENGTH_LONG).show();
                }else {
                    PersonalProfile(fs_name.getText().toString(),last_name.getText().toString(),date.getText().toString());
                }

            }
        });



    }


    private void PersonalProfile(String fs, String ls,String date) {
        final KProgressHUD progressDialog = KProgressHUD.create(PersonalProfile.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "update-profile",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {



                                mUsersession.createRegistrationSession(
                                        jsonObject.getJSONObject("data").getString("UserID"),
                                        jsonObject.getJSONObject("data").getString("InterestedIn"),
                                        jsonObject.getJSONObject("data").getString("ProfilePic"),
                                        jsonObject.getJSONObject("data").getString("FirstName"),
                                        jsonObject.getJSONObject("data").getString("LastName"),
                                        jsonObject.getJSONObject("data").getString("Email"),
                                        jsonObject.getJSONObject("data").getString("MobileNo"),
                                        jsonObject.getJSONObject("data").getString("GoogleID"),
                                        jsonObject.getJSONObject("data").getString("FacebookID"),
                                        "",
                                        jsonObject.getJSONObject("data").getString("TwitterID"),
                                        jsonObject.getJSONObject("data").getString("Address"),
                                        jsonObject.getJSONObject("data").getString("City"),
                                        jsonObject.getJSONObject("data").getString("State"),
                                        jsonObject.getJSONObject("data").getString("Country"),
                                        jsonObject.getJSONObject("data").getString("Pincode"),
                                        jsonObject.getJSONObject("data").getString("DateOfBirth"),
                                        jsonObject.getJSONObject("data").getString("IsEnablePushNotification"),
                                        jsonObject.getJSONObject("data").getString("OTP"),
                                        jsonObject.getJSONObject("data").getString("stripe_customer_id"),
                                        jsonObject.getJSONObject("data").getString("DeviceToken"),
                                        jsonObject.getJSONObject("data").getString("DeviceType"),
                                        jsonObject.getJSONObject("data").getString("APIToken"));

                                Toast.makeText(PersonalProfile.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                            }else {
                                Toast.makeText(PersonalProfile.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(PersonalProfile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(PersonalProfile.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(PersonalProfile.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(PersonalProfile.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            mUsersession.logout();
                            Toast.makeText(PersonalProfile.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PersonalProfile.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("FirstName", fs);
                params.put("LastName", ls);
                params.put("Email", mUsersession.getKeyEmail());
                params.put("DateOfBirth", date);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUsersession.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
            return  dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(calendar.getTime());

            date.setText(dateString);
           /* if(!time.getText().toString().equals("   Select  Date:")){

                try {
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
                    Date date11=new SimpleDateFormat("dd-MM-yyyy").parse(time.getText().toString());
                    printDifference(date2,date11);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }*/
        }
    }

}