package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Adapter.AdapterShippingAddress;
import com.fashion.fashionhouse.Model.ShippingAddressModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddressList extends AppCompatActivity {


    private RecyclerView resShippingAddress;
    private AdapterShippingAddress adapterShippingAddress;
    private ArrayList<ShippingAddressModel> shippingAddressModelArrayList = new ArrayList<>();

    private UserSession session;
    private RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark



        session = new UserSession(AddressList.this);
        requestQueue = Volley.newRequestQueue(AddressList.this);//Creating the RequestQueue


        findViewById(R.id.backToPaymentInfo1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        resShippingAddress = findViewById(R.id.resShippingAddress);
        resShippingAddress.setLayoutManager(new LinearLayoutManager(AddressList.this));
        adapterShippingAddress = new AdapterShippingAddress(AddressList.this, shippingAddressModelArrayList, new AdapterShippingAddress.OnItemClickListener() {
            @Override
            public void onItemClickRemove(int position, int item) {
                Log.e("Remove",shippingAddressModelArrayList.get(position).getIsDefault());
                if(shippingAddressModelArrayList.get(position).getIsDefault().equals("1")){
                    Log.e("Remove",shippingAddressModelArrayList.get(position).getIsDefault());
                    Toast.makeText(AddressList.this,"You Can't remove selected Address!", Toast.LENGTH_SHORT).show();
                }else {
                    Log.e("Remove",shippingAddressModelArrayList.get(position).getIsDefault());
                    removeShippingAddress(item);
                }


            }

            @Override
            public void onItemClickEdit(int item) {
                Intent intent = new Intent(AddressList.this, EditAddress.class);
                intent.putExtra("idShippingAddress", String.valueOf(item));
                startActivity(intent);
            }

            @Override
            public void onItemClickRadio(int item) {

                selectShippingAdress(item);

            }
        });
        resShippingAddress.setAdapter(adapterShippingAddress);



        findViewById(R.id.btnAddNewAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddressList.this, AddAddress.class));
            }
        });



        getAddress();

    }


    private void selectShippingAdress(int item) {
        final KProgressHUD progressDialog = KProgressHUD.create(AddressList.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "set-default-shipping-address",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data).toString());


                            Log.e("Response",jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {

                                    shippingAddressModelArrayList.clear();
                                    getAddress();

                                    Toast.makeText(AddressList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                } catch (JSONException e){
                                    e.printStackTrace();
                                    Toast.makeText(AddressList.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }

                            else if (jsonObject.getString("ResponseCode").equals("422")){
                                Toast.makeText(AddressList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                            else if(jsonObject.getString("ResponseCode").equals("401")){

                              /*  session.logout();
                                Intent intent = new Intent(AddressList.this, Activity_SelectCity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();*/
                            }

                            Toast.makeText(AddressList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            Toast.makeText(AddressList.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                           /* session.logout();
                            Intent intent = new Intent(Activity_ShippingAddress.this, Activity_SelectCity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof ServerError)
                            Toast.makeText(AddressList.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(AddressList.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(AddressList.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(AddressList.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AddressList.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ShippingAddressID", String.valueOf(item));

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(AddressList.this).add(volleyMultipartRequest);
    }



    private void removeShippingAddress(int item) {
        final KProgressHUD progressDialog = KProgressHUD.create(AddressList.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "delete-shipping-address",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data).toString());


                            Log.e("Response",jsonObject.toString());
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {
                                    getAddress();
                                    Toast.makeText(AddressList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                } catch (JSONException e){
                                    e.printStackTrace();
                                    Toast.makeText(AddressList.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }

                            else if (jsonObject.getString("ResponseCode").equals("422")){
                                Toast.makeText(AddressList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                            else if(jsonObject.getString("ResponseCode").equals("401")){

                              /*  session.logout();
                                Intent intent = new Intent(AddressList.this, Activity_SelectCity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();*/
                            }

                            Toast.makeText(AddressList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            Toast.makeText(AddressList.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                           /* session.logout();
                            Intent intent = new Intent(Activity_ShippingAddress.this, Activity_SelectCity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof ServerError)
                            Toast.makeText(AddressList.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(AddressList.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(AddressList.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(AddressList.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AddressList.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                        Toast.makeText(AddressList.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ShippingAddressID", String.valueOf(item));

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(AddressList.this).add(volleyMultipartRequest);
    }



    private void getAddress() {
        final KProgressHUD progressDialog = KProgressHUD.create(AddressList.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-shipping-addresses",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        shippingAddressModelArrayList.clear();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    ShippingAddressModel shippingAddressModel = new ShippingAddressModel();
                                    shippingAddressModel.setShippingAddressID(object.getString("ShippingAddressID"));
                                    shippingAddressModel.setFirstName(object.getString("FirstName"));
                                    shippingAddressModel.setLastName(object.getString("LastName"));
                                    shippingAddressModel.setMobileNumber(object.getString("MobileNumber"));
                                    shippingAddressModel.setShippingHouseNo(object.getString("ShippingHouseNo"));
                                    shippingAddressModel.setShippingAddress(object.getString("ShippingAddress"));
                                    shippingAddressModel.setShippingCity(object.getString("ShippingCity"));
                                    shippingAddressModel.setShippingCountry(object.getString("ShippingCountry"));
                                    //shippingAddressModel.setShippingState(object.getString("StateName"));
                                    shippingAddressModel.setShippingPinCode(object.getString("ShippingPinCode"));
                                    shippingAddressModel.setIsDefault(object.getString("IsDefault"));
                                    shippingAddressModel.setShippingState(object.getString("StateName"));

                                    shippingAddressModelArrayList.add(shippingAddressModel);
                                }

                                adapterShippingAddress.notifyDataSetChanged();



                          //      Toast.makeText(AddressList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }else {
                                Toast.makeText(AddressList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(AddressList.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(AddressList.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(AddressList.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(AddressList.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(AddressList.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AddressList.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    @Override
    protected void onResume() {
        super.onResume();

        getAddress();
    }


}