package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CheckOtp extends AppCompatActivity {


    private EditText et1,et2,et3,et4;
    private String OTP ="";


    private RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_otp);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        requestQueue = Volley.newRequestQueue(CheckOtp.this);//Creating the RequestQueue



        String email = getIntent().getStringExtra("emailForOtp");

        et1 = findViewById(R.id.editText1);
        et2 = findViewById(R.id.editText2);
        et3 = findViewById(R.id.editText3);
        et4 = findViewById(R.id.editText4);


        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));


        findViewById(R.id.checkOtp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et1.getText().toString().isEmpty()){
                    Toast.makeText(CheckOtp.this,"Please enter OTP",Toast.LENGTH_LONG).show();

                }else if(et2.getText().toString().isEmpty()){
                    Toast.makeText(CheckOtp.this,"Please enter OTP",Toast.LENGTH_LONG).show();

                }else if(et3.getText().toString().isEmpty()){
                    Toast.makeText(CheckOtp.this,"Please enter OTP",Toast.LENGTH_LONG).show();

                }else if(et4.getText().toString().isEmpty()){
                    Toast.makeText(CheckOtp.this,"Please enter OTP",Toast.LENGTH_LONG).show();

                }else {
                    OTP = et1.getText().toString()+
                            et2.getText().toString()+
                            et3.getText().toString()+
                            et4.getText().toString();

                    checkOtp(email, OTP);
                }
            }
        });

    }

    private void checkOtp(String email, String otp) {
        final KProgressHUD progressDialog = KProgressHUD.create(CheckOtp.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "check-otp",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                Toast.makeText(CheckOtp.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(CheckOtp.this, NewPassword.class).putExtra("Email",email));

                            }else {
                                Toast.makeText(CheckOtp.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(CheckOtp.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(CheckOtp.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(CheckOtp.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(CheckOtp.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Email", email);
                params.put("OTP", otp);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }




    public class GenericTextWatcher implements TextWatcher {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @SuppressLint("NonConstantResourceId")
        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {

                case R.id.editText1:
                    if(text.length()==1)
                        et2.requestFocus();
                    break;
                case R.id.editText2:
                    if(text.length()==1)
                        et3.requestFocus();
                    else if(text.length()==0)
                        et1.requestFocus();
                    break;
                case R.id.editText3:
                    if(text.length()==1)
                        et4.requestFocus();
                    else if(text.length()==0)
                        et2.requestFocus();
                    break;
                case R.id.editText4:
                    if(text.length()==0) {
                        et3.requestFocus();
                    }else {
                        View view = getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }


}