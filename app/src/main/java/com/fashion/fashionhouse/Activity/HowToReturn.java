package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Model.CountryModel;
import com.fashion.fashionhouse.Model.SelectCountrySpinner;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HowToReturn extends AppCompatActivity {

    private TextView textView1, textView2,status,check;
    private Spinner mItem_1;
    private String mCountryName = "";
    private UserSession session;
    private RequestQueue requestQueue;
    private ArrayList<CountryModel> mCountryList = new ArrayList<>();
    private String mCountryStatus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_return);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        session = new UserSession(HowToReturn.this);
        requestQueue = Volley.newRequestQueue(HowToReturn.this);//Creating the RequestQueue



        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        status = findViewById(R.id.status);
        check = findViewById(R.id.check);
        mItem_1 = findViewById(R.id.item_1);



        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCountryName.equals("Select Country...")){
                    Toast.makeText(HowToReturn.this,"Please select Contry",Toast.LENGTH_LONG);
                }else if(mCountryStatus.equals("1")){
                    status.setText("Returns Available in "+mCountryName);
                    status.setTextColor(getResources().getColor(R.color.green));
                    status.setVisibility(View.VISIBLE);
                }else {
                    status.setText("Returns Not Available in "+mCountryName);
                    status.setTextColor(getResources().getColor(R.color.red));
                    status.setVisibility(View.VISIBLE);
                }
            }
        });

        textView1.setMovementMethod(LinkMovementMethod.getInstance());
        textView1.setText(Html.fromHtml("<p>SHIPPING &amp; <span style=\"color: #222222;\"><strong><a href=\"http://www.topman.com/en/tmuk/category/terms-and-conditions-140316/home?cat2=278567&amp;intcmpid=footer_text_termsconditions#fragment-6\">RETURNS</a> </strong></span></p>\n" +
                "<p>Are you not happy with the item(s) you purchased from us? Do not worry because you can return it for exchange with another item(s) when you return it to us in saleable condition.</p>\n" +
                "<ul>\n" +
                "<li>Items are eligible for exchange with another item on our website if returned within 5 days you receive the item (order)</li>\n" +
                "</ul>\n" +
                "<p>Please also note that items must be returned in their original packing, including hangers, dust bags, cases, boxes, barcode tags, and stickers when applicable. Also, items must not be worn, damaged altered, or stained.</p>\n" +
                "<p><strong>Some Exceptions</strong></p>\n" +
                "<p><strong>Consumable Products</strong>: We do not accept return on consumable products such as perfume, cosmetics, and other beauty products.</p>\n" +
                "<p><strong>Swimwear and underwear&rsquo;s:</strong> In the interests of hygiene we do not offer refunds on underwear or swimwear if the hygiene seal has been removed, or pierced jewellery or cosmetic products if they have been used or the hygiene seal is broken, unless they are of unsatisfactory quality or unfit for purpose.</p>\n" +
                "<p><strong>Shipping Charges:</strong> Shipping charges are not refundable unless the products sent to you are of unsatisfactory quality or unfit for purpose.</p>\n" +
                "<p><strong>Shoes:</strong> Shoes scuff very easily on hard surfaces. It is therefore, strongly recommended that you always try on shoes on carpet. Shoes boxes are an important part of the product presentation and must be returned in good condition, along with the shoes.</p>\n" +
                "<p><strong>Others:</strong> Items marked Final Sale or Non-returnable</p>\n" +
                "<p><strong>Tops &amp; Dresses:</strong> Please, be careful of makeup or deodorant rubbing off on the garments.</p>\n" +
                "<p><span style=\"color: #ff0000;\">Notice: </span>Our Return &amp; Exchange may not be available in your state. However, be assured that we are making every efforts to bring this services to all states in Nigeria.</p>\n" +
                "<p>If, however, the product you received is of unsatisfactory quality or unfit for purpose, please contact our <a href=\"mailto:support@fashionhouse.ng\" style=\"color: blue;\">customer's support</a> for assistance.</p>\n" +
                "<p>To find out if your country or region is eligible for Refund &amp; Exchange, please use the below Dropdown List to check.</p>"));


        textView2.setMovementMethod(LinkMovementMethod.getInstance());
        textView2.setText(Html.fromHtml("<p><strong>HOW TO SEND YOUR RETURNS</strong></p>\n" +
                "<p>To return an item, please click <a href=\"mailto:support@fashionhouse.ng\" style=\"color: blue;\">here</a> and follow the instructions or contact our <a href=\"mailto:support@fashionhouse.ng\" style=\"color: blue;\">customer's support</a> for assistance</p>"));



        findViewById(R.id.textView2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(HowToReturn.this, HowToReturn.class));
            }
        });

        getCountry();

    }


    private void getCountry(){
        final KProgressHUD progressDialog = KProgressHUD.create(HowToReturn.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        //   .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-state",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        mCountryList.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString() + " dd");
                            if (jsonObject.getString("ResponseCode").equals("200")){

                                try {


                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){
                                        JSONObject object = jsonArray.getJSONObject(i);

                                        CountryModel countryModel = new CountryModel();
                                        countryModel.setName(object.getString("StateName"));
                                        countryModel.setStatus(object.getString("IsAvailable"));

                                        mCountryList.add(countryModel);
                                    }

                                    CountryModel countryModel = new CountryModel();
                                    countryModel.setName("Select State...");
                                    countryModel.setStatus("0");
                                    mCountryList.add(countryModel);

                                    SelectCountrySpinner adapter = new SelectCountrySpinner(HowToReturn.this,
                                            android.R.layout.simple_spinner_item,
                                            mCountryList);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mItem_1.setAdapter(adapter);
                                    mItem_1.setSelection(adapter.getCount());


                                    mItem_1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            if(position != mCountryList.size()-1){
                                                try {
                                                    mCountryName = mCountryList.get(position).getName();
                                                    mCountryStatus = mCountryList.get(position).getStatus();
                                                }catch (Exception e){
                                                    //	GetStudnet("0","0");

                                                }
                                            }else {
                                                status.setVisibility(View.GONE);
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(HowToReturn.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("401")){


                            }

                        } catch (Exception e) {
                            Toast.makeText(HowToReturn.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                            /*session.logout();
                            Intent intent = new Intent(getActivity(), Activity_SelectCity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            getActivity().finish();*/

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof ServerError)
                            Toast.makeText(HowToReturn.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(HowToReturn.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(HowToReturn.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(HowToReturn.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(HowToReturn.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
              //  params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(HowToReturn.this).add(volleyMultipartRequest);
    }

}