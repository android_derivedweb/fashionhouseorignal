package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Adapter.AdapterViewAllBag;
import com.fashion.fashionhouse.Model.CategoryDetailsModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.Database;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.google.android.material.snackbar.Snackbar;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PlaceOrder extends AppCompatActivity {

    private RecyclerView recViewBag;
    private AdapterViewAllBag adapterViewAllBag;
    private TextView termsText;

    private ArrayList<CategoryDetailsModel> imageArray = new ArrayList<>();

    private UserSession session;
    private RequestQueue requestQueue;

    private RadioButton paypalRadioBtn, cashRadioBtn;

    private int optionPayment = 0;

    private TextView placeOrder;

    private Database dbHelper;
    private ArrayList<CategoryDetailsModel> detailsModelArrayList = new ArrayList<>();

    private TextView addressText, subTotalText, totalPayFinal;

    private RelativeLayout postalAddress;

    private EditText couponEditT;
    private String couponCode = "";

    private String ShippingAddressID = "";
    private String CouponID = "";
    private String OrderAmount = "";
    private String Tax = "0";
    private String Discount = "";
    private String ShippingCharge = "";
    private String TotalAmount = "";
    private String PaymentMode = "COD";
    private String paypalTransId = "";
    private String PaymentStatus = "Unpaid";

    private static PayPalConfiguration config = new PayPalConfiguration()
// Start with mock environment. When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
// or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)
            .clientId("AXtKVBUiNOkC4-o-8BPUMOjwV10ONyMIu0o9pnlnezSWJmd57WzcUNWHjnD4H0R1we8eE9XKE-SY2tqe");

    private RadioButton bankTransfer, creditOption;
    private boolean isAgreeWithDeposit = false;
    private String mOrderPlaceId;

    private TextView shipCharges, editAddress, daysDelivery, coupenDiscount;
    private String shippingChargesValue;

    private String BankAccountName = "";
    private String BankAccountNo = "";


    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        session = new UserSession(PlaceOrder.this);
        requestQueue = Volley.newRequestQueue(PlaceOrder.this);//Creating the RequestQueue


        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        dbHelper = new Database(PlaceOrder.this);
        detailsModelArrayList = dbHelper.getAllUser();


     /*   mOrderPlaceId = randomString(12);
        Log.e("stringRa", mOrderPlaceId + "--");*/

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        termsText = findViewById(R.id.termsText);
        paypalRadioBtn = findViewById(R.id.paypalRadioBtn);
        cashRadioBtn = findViewById(R.id.cashRadioBtn);
        placeOrder = findViewById(R.id.placeOrder);
        addressText = findViewById(R.id.addressText);
        subTotalText = findViewById(R.id.subTotalText);
        totalPayFinal = findViewById(R.id.totalPayFinal);
        postalAddress = findViewById(R.id.postalAddress);
        couponEditT = findViewById(R.id.couponEditT);
        bankTransfer = findViewById(R.id.bankTransfer);
        shipCharges = findViewById(R.id.shipCharges);
        editAddress = findViewById(R.id.editAddress);
        daysDelivery = findViewById(R.id.daysDelivery);
        coupenDiscount = findViewById(R.id.coupenDiscount);
        creditOption = findViewById(R.id.creditOption);


        editAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PlaceOrder.this, AddressList.class));
            }
        });


        bankTransfer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              //  openDialog();
                isAgreeWithDeposit = isChecked;
                bankTransfer.setChecked(true);

            }
        });
        findViewById(R.id.applyCodeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (couponEditT.getText().toString().isEmpty()){
                    Toast.makeText(PlaceOrder.this, "Please enter code!", Toast.LENGTH_SHORT).show();
                } else {
                    getPlaceOrderDetail();
                }
            }
        });


        paypalRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paypalRadioBtn.setChecked(true);
                cashRadioBtn.setChecked(false);

                optionPayment = 1;

                PaymentMode  = "Paypal";
            }
        });

        cashRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paypalRadioBtn.setChecked(false);
                cashRadioBtn.setChecked(true);
                optionPayment = 2;
                PaymentMode  = "COD";
                PaymentStatus = "Unpaid";
               // openDialog();
            }
        });



        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (session.isLoggedIn()) {

                    /*if (optionPayment == 0) {
                        Toast.makeText(PlaceOrder.this, "Please select payment option", Toast.LENGTH_SHORT).show();
                    } else if (optionPayment == 1) {
                        onBuyPressed(TotalAmount);
                    } else if (optionPayment == 2) {
                      //  onBuyPressed(TotalAmount);
                        PlaceOrder();
                    }*/

                    if(isAgreeWithDeposit){
                      //  orderPlace();

                        Intent intent = new Intent(PlaceOrder.this, CheckoutPaymentPlan.class);
                        intent.putExtra("BankAccountName", BankAccountName);
                        intent.putExtra("BankAccountNo", BankAccountNo);
                        intent.putExtra("mOrderPlaceId", mOrderPlaceId);
                        intent.putExtra("totalPayFinal", totalPayFinal.getText().toString());
                        startActivity(intent);

                    }else {
                     //   Toast.makeText(PlaceOrder.this, "Please select payment method", Toast.LENGTH_SHORT).show();

                        Snackbar.make(findViewById(android.R.id.content), "Please select valid payment method!", Snackbar.LENGTH_SHORT).show();


                    }
                }
                else {
                    Toast.makeText(PlaceOrder.this, "Required sign in", Toast.LENGTH_SHORT).show();
                }

            }
        });


        termsText.setText(Html.fromHtml("<pre>By placing your order you agree to our <span style=\"color: #0000ff;\">terms and conditions</span>, <span style=\"color: #0000ff;\">privacy</span> and <span style=\"color: #0000ff;\">returns policies</span> and consent to some of your data being stored by FASHION HOUSE NIGERIA which may be used to make future shopping experiences better for you.</pre>"));



        findViewById(R.id.postalAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PlaceOrder.this, AddAddress.class));
            }
        });


        recViewBag = findViewById(R.id.recViewBag);
        recViewBag.setLayoutManager(new LinearLayoutManager(PlaceOrder.this, LinearLayoutManager.HORIZONTAL, false));
        adapterViewAllBag = new AdapterViewAllBag(PlaceOrder.this, imageArray, new AdapterViewAllBag.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recViewBag.setAdapter(adapterViewAllBag);


        SpannableString ss = new SpannableString("By placing your order you agree to our terms and conditions, privacy and returns policies and consent to some of your data being stored by FASHION HOUSE NIGERIA which may be used to make future shopping experiences better for you.");
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
         //       Toast.makeText(PlaceOrder.this, "terms and conditions", Toast.LENGTH_SHORT).show();

                startActivity(new Intent(PlaceOrder.this, TermsCondition.class));
            }
        };

        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
        //        Toast.makeText(PlaceOrder.this, "privacy", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(PlaceOrder.this, PrivacyPolicy.class));

            }
        };

       ClickableSpan span3 = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
          //          Toast.makeText(PlaceOrder.this, "returns policies", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(PlaceOrder.this, HowToReturn.class));

                }
            };

        ss.setSpan(span1, 39, 59, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, 61, 68, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span3, 73, 89, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termsText.setText(ss);
        termsText.setMovementMethod(LinkMovementMethod.getInstance());


        getPlaceOrderDetail();


        Log.e("token", session.getKeyApitoken() +"");


        bankTransfer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    bankTransfer.setChecked(true);
                    creditOption.setChecked(false);
                    isAgreeWithDeposit = true;
                }

            }
        });
        creditOption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    creditOption.setChecked(true);
                    bankTransfer.setChecked(false);
                    isAgreeWithDeposit = false;

                    creditCardDialog();
                }

            }
        });


    }


    private void openDialog() {

        Dialog dialogForCity;
        dialogForCity = new Dialog(PlaceOrder.this);
        dialogForCity.setContentView(R.layout.custom_dailog_bank_detail);
        dialogForCity.setCancelable(false);
        dialogForCity.setCanceledOnTouchOutside(false);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        TextView nameBankAccount = dialogForCity.findViewById(R.id.nameBankAccount);
        TextView numberBankAccount = dialogForCity.findViewById(R.id.numberBankAccount);
        TextView placeOrderId = dialogForCity.findViewById(R.id.placeOrderId);
        TextView textDialog1 = dialogForCity.findViewById(R.id.textDialog1);
        TextView whtaspp1Text = dialogForCity.findViewById(R.id.whtaspp1Text);
        TextView whtaspp2Text = dialogForCity.findViewById(R.id.whtaspp2Text);
        TextView email = dialogForCity.findViewById(R.id.email);

        nameBankAccount.setText(BankAccountName);
        numberBankAccount.setText(BankAccountNo);


        dialogForCity.findViewById(R.id.emailCopied).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", email.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });

        dialogForCity.findViewById(R.id.whatsApp1Copied).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", whtaspp1Text.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });

        dialogForCity.findViewById(R.id.whatsApp2Copied).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", whtaspp2Text.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });

        dialogForCity.findViewById(R.id.copyBank).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", numberBankAccount.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });
        placeOrderId.setText("Order # "+ mOrderPlaceId);
        textDialog1.setText(totalPayFinal.getText().toString());


        dialogForCity.findViewById(R.id.btnSubmitBankDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //getInterestedIn();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(PlaceOrder.this);
                builder1.setMessage("Are you sure you want to close?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        Html.fromHtml("<font color='#000000'>Yes</font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                startActivity(new Intent(PlaceOrder.this,MainActivity.class));
                                finish();
                                dialogForCity.dismiss();
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        Html.fromHtml("<font color='#000000'>No</font>"),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();



            }
        });

        dialogForCity.findViewById(R.id.copyOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", mOrderPlaceId);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();

            }
        });


        dialogForCity.show();
    }


    private void creditCardDialog() {

        Dialog dialogForCity;
        dialogForCity = new Dialog(PlaceOrder.this);
        dialogForCity.setContentView(R.layout.custom_credit_info);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        dialogForCity.show();
    }

    private void getPlaceOrderDetail() {
        final KProgressHUD progressDialog = KProgressHUD.create(PlaceOrder.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        String url = "";

        for (int i = 0; i < detailsModelArrayList.size(); i++){

            url = url + "&ProductID[]=" + detailsModelArrayList.get(i).getDeal_id() +
                    "&Qty[]=" + detailsModelArrayList.get(i).getBought() +
                    "&Color[]=" + detailsModelArrayList.get(i).getColorId() +
                    "&Size[]=" + detailsModelArrayList.get(i).getSizeId();
        }

        String urlFinal = url + "&CouponCode=" + couponEditT.getText().toString();

        Log.e("checkUrl", UserSession.BASEURL + "get-place-order?" + url);

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-place-order?" + urlFinal,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        imageArray.clear();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseGetPlaceOrder", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                              //  JSONObject object1 = object.getJSONObject("shipping_address");

                                JSONArray jsonArray = object.getJSONArray("products");

                                for (int i = 0; i < jsonArray.length(); i++){

                                    JSONObject object2 = jsonArray.getJSONObject(i);
                                    CategoryDetailsModel categoryDetailsModel = new CategoryDetailsModel();
                                    categoryDetailsModel.setDeal_image(object2.getString("ProductImage"));

                                    imageArray.add(categoryDetailsModel);
                                }
                                adapterViewAllBag.notifyDataSetChanged();


                                subTotalText.setText(getResources().getString(R.string.currency_symbol) + object.getString("OrderAmount"));
                                shipCharges.setText(getResources().getString(R.string.currency_symbol) + object.getString("ShippingCharge"));

                                shippingChargesValue = object.getString("ShippingCharge");

                                mOrderPlaceId = object.getString("OrderCode");

                                totalPayFinal.setText("NGN " + object.getString("TotalAmount"));


                                TotalAmount = object.getString("TotalAmount");
                                OrderAmount = object.getString("OrderAmount");
                                CouponID = object.getString("CouponID");

                                Discount = object.getString("Discount").replace(",", "");
                                coupenDiscount.setText("congratulation, you got " + getResources().getString(R.string.currency_symbol) + Discount + " Discount");
                                coupenDiscount.setTextColor(getResources().getColor(R.color.green));


                                if (Float.parseFloat(Discount) > 0){
                                    coupenDiscount.setVisibility(View.VISIBLE);
                                } else {
                                    coupenDiscount.setVisibility(View.GONE);
                                }



                                ShippingCharge = object.getString("ShippingCharge");


                                try {
                                    JSONObject object1 = jsonObject.getJSONObject("data").getJSONObject("shipping_address");

                                    postalAddress.setVisibility(View.GONE);
                                    addressText.setVisibility(View.VISIBLE);

                                    ShippingAddressID = object1.getString("ShippingAddressID");
                                    daysDelivery.setText("(Estimated time of delivery: " + object1.getString("NoOfDays") + " days)");


                                    addressText.setText(" "+object1.getString("FirstName") + " " + object1.getString("LastName")+"\n " +
                                            object1.getString("ShippingHouseNo") + ", " +
                                            object1.getString("ShippingAddress") + ", " + "\n " +
                                            object1.getString("ShippingCity") + ", " +
                                            object1.getString("StateName") + ", " +
                                            object1.getString("ShippingCountry") + "." + "\n " +
                                            object1.getString("MobileNumber"));


                                    // for open dialog after order create

                                    JSONObject object2 = object.getJSONObject("setting");

                                    BankAccountName = object2.getString("BankAccountName");
                                    BankAccountNo = object2.getString("BankAccountNo");



                                }catch (Exception e){
                                    postalAddress.setVisibility(View.VISIBLE);
                                    addressText.setVisibility(View.GONE);
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("422")){

                                new AlertDialog.Builder(PlaceOrder.this)
                                        .setTitle("Alert...")
                                        .setMessage(jsonObject.getString("ResponseMsg"))

                                        // Specifying a listener allows you to take an action before dismissing the dialog.
                                        // The dialog is automatically dismissed when a dialog button is clicked.
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Continue with delete operation
                                                dbHelper = new Database(PlaceOrder.this);
                                                dbHelper.deleteAll();
                                                if(session.getG_GuestLogin()){
                                                    if(session.getG_WantToStoreIt().equals("1")){

                                                        String deviceToken = session.getFirbaseDeviceToken();
                                                        session.logout();
                                                        session.setFirbaseDeviceToken(deviceToken);
                                                 //       Toast.makeText(PlaceOrder.this,"Session Clear",Toast.LENGTH_LONG).show();
                                                    }else {
                                                  //      Toast.makeText(PlaceOrder.this,"Session Not Clear",Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                                startActivity(new Intent(PlaceOrder.this,MainActivity.class));
                                                finish();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();


                            }else if(jsonObject.getString("ResponseCode").equals("421")){

                                coupenDiscount.setVisibility(View.VISIBLE);
                                coupenDiscount.setTextColor(getResources().getColor(R.color.red));
                                coupenDiscount.setText(jsonObject.getString("ResponseMsg"));
                                couponEditT.setText("");

                                getPlaceOrderDetailCoupen();

                            }else {
                                Toast.makeText(PlaceOrder.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(PlaceOrder.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(PlaceOrder.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(PlaceOrder.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(PlaceOrder.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(PlaceOrder.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlaceOrder.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);
    }


    private void getPlaceOrderDetailCoupen() {
        final KProgressHUD progressDialog = KProgressHUD.create(PlaceOrder.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
           //     .show();

        String url = "";

        for (int i = 0; i < detailsModelArrayList.size(); i++){

            url = url + "&ProductID[]=" + detailsModelArrayList.get(i).getDeal_id() +
                    "&Qty[]=" + detailsModelArrayList.get(i).getBought() +
                    "&Color[]=" + detailsModelArrayList.get(i).getColorId() +
                    "&Size[]=" + detailsModelArrayList.get(i).getSizeId();
        }

        String urlFinal = url + "&CouponCode=" + "";

        Log.e("checkUrl", UserSession.BASEURL + "get-place-order?" + url);

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-place-order?" + urlFinal,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                    @Override
                    public void onResponse(NetworkResponse response) {

          //              progressDialog.dismiss();
                        imageArray.clear();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                              //  JSONObject object1 = object.getJSONObject("shipping_address");

                                JSONArray jsonArray = object.getJSONArray("products");

                                for (int i = 0; i < jsonArray.length(); i++){

                                    JSONObject object2 = jsonArray.getJSONObject(i);
                                    CategoryDetailsModel categoryDetailsModel = new CategoryDetailsModel();
                                    categoryDetailsModel.setDeal_image(object2.getString("ProductImage"));

                                    imageArray.add(categoryDetailsModel);
                                }
                                adapterViewAllBag.notifyDataSetChanged();


                                subTotalText.setText(getResources().getString(R.string.currency_symbol) + object.getString("OrderAmount"));
                                shipCharges.setText(getResources().getString(R.string.currency_symbol) + object.getString("ShippingCharge"));

                                shippingChargesValue = object.getString("ShippingCharge");

                                mOrderPlaceId = object.getString("OrderCode");

                                totalPayFinal.setText("NGN " + object.getString("TotalAmount"));


                                TotalAmount = object.getString("TotalAmount");
                                OrderAmount = object.getString("OrderAmount");
                                CouponID = object.getString("CouponID");

                                ShippingCharge = object.getString("ShippingCharge");


                                try {
                                    JSONObject object1 = jsonObject.getJSONObject("data").getJSONObject("shipping_address");

                                    postalAddress.setVisibility(View.GONE);
                                    addressText.setVisibility(View.VISIBLE);

                                    ShippingAddressID = object1.getString("ShippingAddressID");
                                    daysDelivery.setText("(Estimated time of delivery: " + object1.getString("NoOfDays") + " days)");


                                    addressText.setText(object1.getString("ShippingHouseNo") + ", " +
                                            object1.getString("ShippingAddress") + ", " +
                                            object1.getString("ShippingCity") + ", " +
                                            object1.getString("ShippingCountry") + ", " +
                                            object1.getString("ShippingPinCode") + "\n " +
                                            object1.getString("MobileNumber"));

                                }catch (Exception e){
                                    postalAddress.setVisibility(View.VISIBLE);
                                    addressText.setVisibility(View.GONE);
                                }
                            }else if(jsonObject.getString("ResponseCode").equals("422")){

                                new AlertDialog.Builder(PlaceOrder.this)
                                        .setTitle("Alert...")
                                        .setMessage(jsonObject.getString("ResponseMsg"))

                                        // Specifying a listener allows you to take an action before dismissing the dialog.
                                        // The dialog is automatically dismissed when a dialog button is clicked.
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Continue with delete operation
                                                dbHelper = new Database(PlaceOrder.this);
                                                dbHelper.deleteAll();
                                                if(session.getG_GuestLogin()){
                                                    if(session.getG_WantToStoreIt().equals("1")){
                                                        String deviceToken = session.getFirbaseDeviceToken();
                                                        session.logout();
                                                        session.setFirbaseDeviceToken(deviceToken);                                                 //       Toast.makeText(PlaceOrder.this,"Session Clear",Toast.LENGTH_LONG).show();
                                                    }else {
                                                  //      Toast.makeText(PlaceOrder.this,"Session Not Clear",Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                                startActivity(new Intent(PlaceOrder.this,MainActivity.class));
                                                finish();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();


                            }else {
                                Toast.makeText(PlaceOrder.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            Toast.makeText(PlaceOrder.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
            //            progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(PlaceOrder.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(PlaceOrder.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(PlaceOrder.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(PlaceOrder.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlaceOrder.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void orderPlace() {
        final KProgressHUD progressDialog = KProgressHUD.create(PlaceOrder.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "create-order",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint({"NotifyDataSetChanged", "SetTextI18n"})
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponsePlaceOrder", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                if(session.getG_GuestLogin()){
                                    if(session.getG_WantToStoreIt().equals("0")){
                                        String deviceToken = session.getFirbaseDeviceToken();
                                        session.logout();
                                        session.setFirbaseDeviceToken(deviceToken);                                   //     Toast.makeText(PlaceOrder.this,"Session Clear",Toast.LENGTH_LONG).show();
                                    }else {
                                     //   Toast.makeText(PlaceOrder.this,"Session Not Clear",Toast.LENGTH_LONG).show();
                                    }
                                }

                                openDialog();

                                dbHelper.deleteAll();


                                Toast.makeText(PlaceOrder.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }else if(jsonObject.getString("ResponseCode").equals("422")) {

                                String mErrorMsg = "";
                                Iterator<?> keys = jsonObject.keys();

                                while (keys.hasNext()) {
                                    String key = (String) keys.next();
                                    Log.d("res1", key.toString());
                                    if (jsonObject.get(key) instanceof JSONObject) {
                                        JSONObject xx = new JSONObject(jsonObject.get(key).toString());
                                        Log.e("value", xx.toString());
                                        Iterator<?> iterator = xx.keys();
                                        while (iterator.hasNext()) {
                                            String next = (String) iterator.next();
                                            Log.e("value", next);

                                            if(mErrorMsg.isEmpty()){
                                                mErrorMsg = xx.getString(next);
                                            }else {
                                                mErrorMsg = mErrorMsg + " , \n" + xx.getString(next);
                                            }
                                        }

                                    }

                                }

                                String toast = mErrorMsg.replace("[\"", "");
                                String toast1 = toast.replace("\"]", "");
                                Toast.makeText(PlaceOrder.this, toast1, Toast.LENGTH_LONG).show();
                            }

                            } catch (Exception e) {
                            Toast.makeText(PlaceOrder.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(PlaceOrder.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(PlaceOrder.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(PlaceOrder.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(PlaceOrder.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PlaceOrder.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                Log.e("Response",ShippingAddressID+"--"
                        +CouponID+"--"
                        +OrderAmount+"--"
                        +Tax+"--"
                        +Discount+"--"
                        +TotalAmount+"--"
                        +PaymentMode+"--"
                        +PaymentStatus+"--"
                        +paypalTransId+"--"
                        +mOrderPlaceId+"--"
                        +shippingChargesValue+"--");

                params.put("ShippingAddressID", ShippingAddressID.replace(",", ""));
                params.put("CouponID", CouponID.replace(",", ""));
                params.put("OrderAmount", OrderAmount.replace(",", ""));
                params.put("Tax", Tax.replace(",", ""));
                params.put("Discount", Discount.replace(",", ""));
                params.put("ShippingCharge", shippingChargesValue.replace(",", ""));
                params.put("TotalAmount", TotalAmount.replace(",", ""));
                params.put("PaymentMode", PaymentMode.replace(",", ""));
                params.put("PaymentStatus", PaymentStatus.replace(",", ""));
                params.put("PaypalTransactionID", paypalTransId.replace(",", ""));
                params.put("PlaceOrderId", mOrderPlaceId.replace(",", ""));

                for (int i = 0; i < detailsModelArrayList.size(); i++){
                    params.put("ProductID["+i+"]", detailsModelArrayList.get(i).getDeal_id());
                    params.put("Qty["+i+"]", detailsModelArrayList.get(i).getBought());
                    params.put("VariantID["+i+"]", detailsModelArrayList.get(i).getVariantID());

                    Log.e("Response_Product_id",detailsModelArrayList.get(i).getDeal_id()+"--"+
                            detailsModelArrayList.get(i).getBought()+"--"+detailsModelArrayList.get(i).getVariantID());
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


    /*private class PlaceOrderToServer extends AsyncTask<Void, Integer, String> {
        private  String FILE_UPLOAD_URL = UserSession.BASEURL + "create-order";
        private KProgressHUD progressDialog;

        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            super.onPreExecute();
            progressDialog = KProgressHUD.create(PlaceOrder.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Please wait")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);

            progressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(FILE_UPLOAD_URL);
            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });


                // Extra parameters if you want to pass to server
                entity.addPart("ShippingAddressID", new StringBody(ShippingAddressID));
                entity.addPart("CouponID", new StringBody(CouponID));
                entity.addPart("OrderAmount", new StringBody(OrderAmount));
                entity.addPart("Tax", new StringBody(Tax));
                entity.addPart("Discount", new StringBody(Discount));
                entity.addPart("ShippingCharge", new StringBody(ShippingCharge));
                entity.addPart("TotalAmount", new StringBody(TotalAmount));
                entity.addPart("PaymentMode", new StringBody(PaymentMode));
                entity.addPart("PaymentStatus", new StringBody(PaymentStatus));
                entity.addPart("PaypalTransactionID", new StringBody(paypalTransId));


                for (int i = 0; i < detailsModelArrayList.size(); i++) {
                    entity.addPart("ProductID[]", new StringBody(detailsModelArrayList.get(i).getDeal_id()));
                    entity.addPart("Qty[]", new StringBody(detailsModelArrayList.get(i).getBought()));
                    entity.addPart("ColorID[]", new StringBody(detailsModelArrayList.get(i).getColorId()));
                    entity.addPart("SizeID[]", new StringBody(detailsModelArrayList.get(i).getSizeId()));
                }
                httppost.setEntity(entity);
                httppost.addHeader("Accept", "application/json");
                httppost.addHeader("Authorization", "Bearer " + session.getKeyApitoken());
                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("TAG", "Response from server: " + result);
            progressDialog.dismiss();
            // showing the server response in an alert categoryDialog
            //showAlert(result);
            try {

                JSONObject jsonObject = new JSONObject(result);
                Log.e("ResponsePlaceOrder", jsonObject.toString() + "--");

                if (jsonObject.getString("ResponseCode").equals("200")) {

                    finish();

                    Toast.makeText(PlaceOrder.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                }else {
                    Toast.makeText(PlaceOrder.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                }

            } catch (Exception e) {
                Toast.makeText(PlaceOrder.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                AlertDialog.Builder builder = new AlertDialog.Builder(PlaceOrder.this);
                builder.setMessage("Something went wrong please try again later!").setTitle("Error!")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                // do nothing

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
            super.onPostExecute(result);
        }

    }

*/

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4));

                    JSONObject object =   new JSONObject(confirm.toJSONObject().toString(4));

                    paypalTransId = object.getJSONObject("response").getString("id");

                    if(object.getJSONObject("response").getString("state").equals("approved")){
                        PaymentStatus = "Paid";
                    }

                    Log.e("chckPayment", paypalTransId + "-" + PaymentStatus);

                    orderPlace();
                  //  new PlaceOrderToServer().execute();


                    Toast.makeText(PlaceOrder.this, "Payment Success", Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
            Toast.makeText(PlaceOrder.this, "The user canceled.", Toast.LENGTH_SHORT).show();

        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            Toast.makeText(PlaceOrder.this, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.", Toast.LENGTH_SHORT).show();

        }
    }

    public void onBuyPressed(String amount) {

        PayPalPayment payment = new PayPalPayment(new BigDecimal(amount.replace(",","")), "GBP", "sample item",
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, 0);
    }


    @Override
    protected void onResume() {
        super.onResume();

        getPlaceOrderDetail();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(session.getG_GuestLogin()){
            if(session.getG_WantToStoreIt().equals("0")){

                String deviceToken = session.getFirbaseDeviceToken();
                session.logout();
                session.setFirbaseDeviceToken(deviceToken);
                //       Toast.makeText(PlaceOrder.this,"Session Clear",Toast.LENGTH_LONG).show();
            }else {
                //      Toast.makeText(PlaceOrder.this,"Session Not Clear",Toast.LENGTH_LONG).show();
            }
        }
        finish();

    }


    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public String randomString(int len){
        StringBuilder sb = new StringBuilder(len);
        for(int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


}