package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.fashion.fashionhouse.R;

public class AboutPeperempe extends AppCompatActivity {


    private TextView textView1;
    private TextView textView2;
    private TextView textView3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_fashion_house);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);

       // textView1.setText(Html.fromHtml("<p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>In the midst of thought is the UNIQUENESS.</strong><br /><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;In the fullest of sight is the BEAUTY. </strong><br /><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;In the shadow of credence, is the POWERS</strong></p>"));
      //  textView2.setText(Html.fromHtml("<p style=\"text-align: center;\"><strong>~Fashion House</strong> (<em>Reveal your inner POWER</em>) created 2015</p>"));
        textView3.setText(Html.fromHtml(
                "<p style=\"text-align: center;\"><strong>About Us.</strong></p>\n" +
                        "<p>Fashion House is a Multi-Brand Store with the biggest variety of fashion products, exclusive brand selections and collaboration.</p>\n" +
                        "<p>Our Belgrade brilliant team overseas the selection of items and brands to bring to you, ranging from clothing, shoes, accessories, and gift for both men and women.</p>\n" +
                        "<p><span lang=\"\\&quot;en-US\\&quot;\">From the mainstream to alternate lifestyle, we rethink fashion differently </span>and o<span lang=\"\\&quot;en-US\\&quot;\">ffer fashion styles to thrill our fans and consumers. </span></p>\n" +
                        "<p><span lang=\"\\&quot;en-US\\&quot;\">Built on the idea that beauty comes with freedom, Fashion House has only one rule and that&rsquo;s</span>:<span lang=\"\\&quot;en-US\\&quot;\"> &ldquo;there isn&rsquo;t a rule&rdquo;.</span></p>"));


    }


}