package com.fashion.fashionhouse.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Adapter.AdapterProduct;
import com.fashion.fashionhouse.Fragments.ItemsDetails;
import com.fashion.fashionhouse.Model.ProductListModel;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductList extends AppCompatActivity {

    private RecyclerView recItemsList;
    private AdapterProduct adapterProduct;

    private UserSession session;
    private RequestQueue requestQueue;


    private ArrayList<ProductListModel> productListModelArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        String BrandID = getIntent().getStringExtra("BrandID");

        session = new UserSession(ProductList.this);
        requestQueue = Volley.newRequestQueue(ProductList.this);//Creating the RequestQueue


        recItemsList = findViewById(R.id.recItemsList);
        recItemsList.setLayoutManager(new GridLayoutManager(ProductList.this, 2));
        adapterProduct = new AdapterProduct(ProductList.this, productListModelArrayList, new AdapterProduct.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

              /*  startActivity(new Intent(ProductList.this, ItemsDetail.class)
                .putExtra("ProductID", productListModelArrayList.get(item).getProductID()));
*/
                ItemsDetails fragobj = new ItemsDetails();
                Bundle bundle = new Bundle();
                bundle.putString("ProductID", productListModelArrayList.get(item).getProductID());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.fragmentLinearHome,fragobj,"Fragment",null);

            }

            @Override
            public void onItemClickWishList(int item) {

                setWishlist(productListModelArrayList.get(item).getProductID());

            }
        });
        recItemsList.setAdapter(adapterProduct);




        getProductList(BrandID);


    }



    private void setWishlist(String productId) {
        final KProgressHUD progressDialog = KProgressHUD.create(ProductList.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                UserSession.BASEURL + "add-to-wishlist",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {



                                Toast.makeText(ProductList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                            }else {
                                Toast.makeText(ProductList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(ProductList.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(ProductList.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ProductList.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ProductList.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ProductID", productId);
                params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
        //        params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void getProductList(String BrandID) {
        final KProgressHUD progressDialog = KProgressHUD.create(ProductList.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
              //  .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "search-products?BrandID=" + BrandID + "&Token=" + session.getFirbaseDeviceToken(),
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                //        progressDialog.dismiss();
                        productListModelArrayList.clear();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                JSONObject object = jsonObject.getJSONObject("data");

                                JSONArray jsonArray = object.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){
                                    JSONObject object1 = jsonArray.getJSONObject(i);

                                    ProductListModel productListModel = new ProductListModel();
                                    productListModel.setProductID(object1.getString("ProductID"));
                                    productListModel.setProductTitle(object1.getString("ProductTitle"));
                                    productListModel.setBrandID(object1.getString("BrandID"));
                                    productListModel.setPrice(object1.getString("Price"));
                                    productListModel.setQuantity(object1.getString("Quantity"));
                                    productListModel.setProductImage(object1.getString("ProductImage"));
                                    productListModel.setIsNewIn(object1.getString("IsNewIn"));
                                    productListModel.setIsSale(object1.getString("IsSale"));


                                    if (object1.getString("IsWishlist").equals("0")){
                                        productListModel.setWishlist(false);
                                    } else if (object1.getString("IsWishlist").equals("1")){
                                        productListModel.setWishlist(true);

                                    }

                                    productListModelArrayList.add(productListModel);


                                    Toast.makeText(ProductList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                                }
                                adapterProduct.notifyDataSetChanged();




                            }else {
                                Toast.makeText(ProductList.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(ProductList.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
             //           progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(ProductList.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ProductList.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ProductList.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
       //         params.put("Token", session.getFirbaseDeviceToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
         //       params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }





}