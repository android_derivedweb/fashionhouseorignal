package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.fashion.fashionhouse.R;

public class ShippingNotice extends AppCompatActivity {


    private TextView textView1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_notice);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark



        textView1 = findViewById(R.id.textView1);

        textView1.setText(Html.fromHtml("<p><strong>Tracking:</strong> All Paid shipping option (including, some free shipping option) include tracking. Please, look for the tracking link in your confirmation e-mail to track your package. Orders placed late on Fridays, Holidays or during the weekend, will be processed by our shipper on the next business day.</p>"));


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}