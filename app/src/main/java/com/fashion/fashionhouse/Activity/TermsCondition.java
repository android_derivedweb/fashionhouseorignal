package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.fashion.fashionhouse.R;

public class TermsCondition extends AppCompatActivity {


    private TextView textView1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        textView1 = findViewById(R.id.textView1);

        textView1.setMovementMethod(LinkMovementMethod.getInstance());


        textView1.setText(Html.fromHtml("<!DOCTYPE html>\n" +
                "<!--[if lt IE 7 ]><html class=\"ie ie6\" lang=\"en\"> <![endif]-->\n" +
                "<!--[if IE 7 ]><html class=\"ie ie7\" lang=\"en\"> <![endif]-->\n" +
                "<!--[if IE 8 ]><html class=\"ie ie8\" lang=\"en\"> <![endif]-->\n" +
                "<!--[if (gte IE 9)|!(IE)]><!-->\n" +
                "<html lang=\"en\">\n" +
                "<!--<![endif]-->\n" +
                "<head>\n" +
                "<meta charset=\"UTF-8\">\n" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "<meta name=\"viewport\" content=\"width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0\">\n" +
                "<meta name=\"keywords\" content=\"\">\n" +
                "<meta name=\"description\" content=\"\">\n" +
                "<meta name=\"author\" content=\"\">\n" +
                "\n" +
                "<!--Favicon -->\n" +
                "\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<h3>PRICING AND PAYMENT</h3>\n" +
                "\n" +
                "<p>The prices shown on FASHIONHOUSE.NG website & Apps shall include any VAT (or similar sales tax) at the prevailing rate for which we are responsible as the seller. The amount paid by customers will be the aforesaid price (prices indicated on our website) plus any delivery charges. The delivery charges will depend on the delivery option that customers choose and will be shown during the checkout process. <br/>\n" +
                "All prices, charges and fees are indicated in NGN (₦) and charged as such.</p>\n" +
                "\n" +
                "\n" +
                "<h3>PRODUCT INFORMATION</h3>\n" +
                "\n" +
                "<p>We make every effort to display accurate colours of our products on the website. However, the colours you see on your monitor may vary from the actual colour due to your monitor display.</p>\n" +
                "<p>All sizes and measurements are approximate, however we make every effort to ensure they are as accurate as possible. Please use our size chart for help and feel free to contact our support team at <a href=\"mailto:support@fashionhouse.ng\" style=\"color: blue;\">support@fashionhouse.ng</a> for assistance with sizing. </p>\n" +
                "<p>We will take all reasonable care to ensure that all details, descriptions and prices of products appearing on the website are correct at the time when the relevant information was entered onto the system. We reserve the right to refuse orders where product information has been mis-published, including prices and promotions.</p>\n" +
                "\n" +
                "\n" +
                "<h3>PURCHASE OF PRODUCTS</h3>\n" +
                "\n" +
                "<p>When you place an order on our website we shall email you an order confirmation email. Our acceptance of your order does not take place until despatch of the order, at which point the contract for the purchase of goods will be made.</p>\n" +
                "<p>Once you have checked out and you have received your order confirmation email you will not be able to make any changes to your order so please make sure that everything is correct before placing your order.</p>\n" +
                "<p>We reserve the right to refuse an order. Non-acceptance of an order may, for example, result from one of the following:</p>\n" +
                "<p>* The product ordered being unavailable from stock</p>\n" +
                "<p>* The identification of an error within the product information, including price or promotion</p>\n" +
                "<p>* If we suspect any fraudulent activity</p>\n" +
                "<p>If there are any problems with your order we shall contact you. We reserve the right to reject any offer to purchase by you at any time. We will take all necessary measures to keep the details of your order and payment secure. Prices are subject to change without notice. These changes will not affect orders that have already been despatched. </p>\n" +
                "<p>Goods are subject to availability. As there is a delay between the time when the order is placed and the time when the order is accepted, the stock position relating to particular items may change. If an item you have ordered becomes out of stock before we accept the order we shall notify you as soon as possible and you will not be charged for the out of stock items. However, we make every effort to continually update our app and website from time to time. </p>\n" +
                "<p>The delivery period quoted or stated on the checkout section is an estimate only and does not guarantee the actual delivery time. If you wish to use an item in your order for a particular date, please contact our customer support at <a href=\"mailto:support@fashionhouse.ng\" style=\"color: blue;\">support@fashionhouse.ng</a> to confirm that we have the item in-stock in our Nigerian warehouse, and can deliver as quoted in the checkout page, before placing your order. </p>\n" +
                "<p>For item not already in our warehouse, the delivery time may vary with 10 – 15 days. However, we are continuously making every effort within our reach to improve our delivery time. </p>\n" +
                "<p>Also, please note that some App and website promotions may not be available to customers in particular jurisdictions.</p>\n" +
                "\n" +
                "\n" +
                "</body>\n" +
                "</html>"));



       /* textView1.setText(Html.fromHtml("<p align=\"center\"><span style=\"color: #222222;\"><strong>Terms &amp; Conditions</strong></span></p>\n" +
                "<p><span style=\"color: #222222;\"><strong><a href=\"http://www.topman.com/en/tmuk/category/terms-and-conditions-140316/home?cat2=278567&amp;intcmpid=footer_text_termsconditions#fragment-4\">PRICING AND PAYMENT </a></strong></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">The prices shown on FASHIONHOUSE.NG website &amp; Apps shall include any VAT (or similar sales tax) at the prevailing rate for which we are responsible as the seller. The amount paid by customers will be the aforesaid price (prices indicated on our website) plus any delivery charges. The delivery charges will depend on the delivery option that customers choose and will be shown during the checkout process. &nbsp;</span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">All prices, charges and fees are indicated in NGN (₦) and charged as such. </span></span></span></span></p>\n" +
                "<p><span style=\"color: #222222;\"><strong><a href=\"http://www.topman.com/en/tmuk/category/terms-and-conditions-140316/home?cat2=278567&amp;intcmpid=footer_text_termsconditions#fragment-7\">PRODUCT INFORMATION</a></strong></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">We make every effort to display accurate colours of our products on the website. However, the colours you see on your monitor may vary from the actual colour due to your monitor display.</span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">All sizes and measurements are approximate, however we make every effort to ensure they are as accurate as possible. Please use our size chart for help and feel free to contact our </span></span><span style=\"color: #5b9bd5;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\"><u>customer&rsquo;s support</u></span></span></span><u> </u><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">for assistance with sizing. </span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">We will take all reasonable care to ensure that all details, descriptions and prices of products appearing on the website are correct at the time when the relevant information was entered onto the system. We reserve the right to refuse orders where product information has been mis-published, including prices and promotions.</span></span></span></span></p>\n" +
                "<p><span style=\"color: #222222;\"><strong><a href=\"http://www.topman.com/en/tmuk/category/terms-and-conditions-140316/home?cat2=278567&amp;intcmpid=footer_text_termsconditions#fragment-8\">PURCHASE OF PRODUCTS</a></strong></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">When you place an order on our website we shall email you an order confirmation email. Our acceptance of your order does not take place until despatch of the order, at which point the contract for the purchase of goods will be made.</span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">Once you have checked out and you have received your order confirmation email you will not be able to make any changes to your order so please make sure that everything is correct before placing your order.</span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">We reserve the right to refuse an order. Non-acceptance of an order may, for example, result from one of the following:</span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">* The product ordered being unavailable from stock</span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">* The identification of an error within the product information, including price or promotion</span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">* If we suspect any fraudulent activity</span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">If there are any problems with your order we shall contact you. We reserve the right to reject any offer to purchase by you at any time. We will take all necessary measures to keep the details of your order and payment secure. Prices are subject to change without notice. These changes will not affect orders that have already been despatched. </span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">Goods are subject to availability. As there is a delay between the time when the order is placed and the time when the order is accepted, the stock position relating to particular items may change. If an item you have ordered becomes out of stock before we accept the order we shall notify you as soon as possible and you will not be charged for the out of stock items. However, we make every effort to continually update our website from time to time. </span></span></span></span></p>\n" +
                "<p><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: medium;\"><span style=\"font-family: Calibri, serif;\"><span style=\"font-size: small;\">Also, please note that some website promotions may not be available to customers in particular jurisdictions.</span></span></span></span></p>"));
*/

    }




}