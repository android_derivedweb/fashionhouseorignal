package com.fashion.fashionhouse.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePassword extends AppCompatActivity {


    private RequestQueue requestQueue;
    private EditText oldPassword;
    private EditText newPassword;
    private EditText confirmPassword;
    private UserSession mUsersession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        requestQueue = Volley.newRequestQueue(ChangePassword.this);//Creating the RequestQueue

        mUsersession = new UserSession(ChangePassword.this);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        newPassword = findViewById(R.id.newPassword);
        oldPassword = findViewById(R.id.oldPassword);
        confirmPassword = findViewById(R.id.confirmPassword);




        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(oldPassword.getText().toString().isEmpty()){
                    Toast.makeText(ChangePassword.this,"Please enter your old password",Toast.LENGTH_LONG).show();
                }else if(newPassword.getText().toString().isEmpty()){
                    Toast.makeText(ChangePassword.this,"Please enter your new password",Toast.LENGTH_LONG).show();
                }else if(confirmPassword.getText().toString().isEmpty()){
                    Toast.makeText(ChangePassword.this,"Please enter your confirm password",Toast.LENGTH_LONG).show();
                }else if(!confirmPassword.getText().toString().equals(newPassword.getText().toString())){
                    Toast.makeText(ChangePassword.this,"Password does not match",Toast.LENGTH_LONG).show();
                }else {
                    ChangePassword(oldPassword.getText().toString(),confirmPassword.getText().toString());
                }

            }
        });
    }


    private void ChangePassword(String old, String newp) {
        final KProgressHUD progressDialog = KProgressHUD.create(ChangePassword.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "change-password",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                Toast.makeText(ChangePassword.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                            }else {
                                Toast.makeText(ChangePassword.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(ChangePassword.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(ChangePassword.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ChangePassword.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ChangePassword.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            mUsersession.logout();
                            Toast.makeText(ChangePassword.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ChangePassword.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Password", newp);
                params.put("ConfirmPassword", newp);
                params.put("OldPassword", old);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUsersession.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

}