package com.fashion.fashionhouse.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NewPassword extends AppCompatActivity {


    private EditText et1,et2,et3,et4;
    private String OTP ="";


    private RequestQueue requestQueue;
    private EditText newPassword;
    private EditText confirmPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newpassword);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        requestQueue = Volley.newRequestQueue(NewPassword.this);//Creating the RequestQueue


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        String newString;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString= null;
            } else {
                newString= extras.getString("Email");
            }
        } else {
            newString= (String) savedInstanceState.getSerializable("Email");
        }

        newPassword = findViewById(R.id.newPassword);
        confirmPassword = findViewById(R.id.confirmPassword);




        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newPassword.getText().toString().isEmpty()){
                    Toast.makeText(NewPassword.this,"Please enter your new password",Toast.LENGTH_LONG).show();
                }else if(confirmPassword.getText().toString().isEmpty()){
                    Toast.makeText(NewPassword.this,"Please enter your new password",Toast.LENGTH_LONG).show();
                }else if(!confirmPassword.getText().toString().equals(newPassword.getText().toString())){
                    Toast.makeText(NewPassword.this,"Password does not match",Toast.LENGTH_LONG).show();
                }else {
                    NewPassword(newString,confirmPassword.getText().toString());
                }

            }
        });

    }

    private void NewPassword(String email, String otp) {
        final KProgressHUD progressDialog = KProgressHUD.create(NewPassword.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "new-password",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                Toast.makeText(NewPassword.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(NewPassword.this, Splash.class));

                            }else {
                                Toast.makeText(NewPassword.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(NewPassword.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;
                        if (error instanceof ServerError)
                            Toast.makeText(NewPassword.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(NewPassword.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(NewPassword.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Email", email);
                params.put("NewPassword", otp);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //      params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

}