package com.fashion.fashionhouse.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Model.CityModel;
import com.fashion.fashionhouse.Model.SelectCitySpinner1;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AsGuestFrom extends AppCompatActivity {


    private TextView mFirstName,mLastName,mMobile,mShippingHouseNo,mShippingAddress,mShippingCity,mShippingCountry,mShippingPinCode,mEmail,mPassword,mSubmit;
    private CheckBox isPasswordCheck;
    private String SaveDetails = "0";
    private UserSession mUserSession;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private RequestQueue requestQueue;
    private LinearLayout password_layout;
    private Spinner mSpinnerCity;


    private ArrayList<CityModel> mDataState = new ArrayList<>();

    private String state_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order_as_guest);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        requestQueue = Volley.newRequestQueue(AsGuestFrom.this);//Creating the RequestQueue
        mUserSession = new UserSession(AsGuestFrom.this);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        password_layout = findViewById(R.id.password_layout);
        mFirstName = findViewById(R.id.mFirstName);
        mLastName = findViewById(R.id.mLastName);
        mMobile = findViewById(R.id.mMobile);
        mShippingHouseNo = findViewById(R.id.mShippingHouseNo);
        mShippingAddress = findViewById(R.id.mShippingAddress);
        mShippingCity = findViewById(R.id.mShippingCity);
        mShippingCountry = findViewById(R.id.mShippingCountry);
        mShippingPinCode = findViewById(R.id.mShippingPinCode);
        mEmail = findViewById(R.id.mEmail);
        isPasswordCheck = findViewById(R.id.isPasswordCheck);
        mPassword = findViewById(R.id.mPassword);
        mSubmit = findViewById(R.id.mSubmit);


        mSpinnerCity = (Spinner) findViewById(R.id.physical_location_spinner);

        SelectCitySpinner1 adapterState = new SelectCitySpinner1(AsGuestFrom.this,
                android.R.layout.simple_spinner_item,
                mDataState);
        adapterState.setDropDownViewResource(android.R.layout.simple_spinner_item);
        mSpinnerCity.setAdapter(adapterState);
        mSpinnerCity.setSelection(adapterState.getCount());



        mSpinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != mDataState.size()-1){
                    try {
                        state_id = mDataState.get(position).getCityId();
                        Log.e("state_id", state_id);
                    }catch (Exception e){
                        //	GetStudnet("0","0");

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        isPasswordCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SaveDetails = "1";
                    password_layout.setVisibility(View.VISIBLE);
                }else {
                    SaveDetails = "0";
                    password_layout.setVisibility(View.GONE);
                }
            }
        });


        Log.e("GuestLogin",""+mUserSession.getG_GuestLogin());


        if(mUserSession.getG_GuestLogin()){
            mFirstName.setText(mUserSession.getG_FirstName());
            mLastName.setText(mUserSession.getG_LastName());
            mMobile.setText(mUserSession.getG_MobileNumber());
            mShippingHouseNo.setText(mUserSession.getG_ShippingHouseNo());
            mShippingAddress.setText(mUserSession.getG_ShippingAddress());
            mShippingCity.setText(mUserSession.getG_ShippingCity());
            mShippingCountry.setText(mUserSession.getG_ShippingCountry());
            mShippingPinCode.setText(mUserSession.getG_ShippingPinCode());
            mEmail.setText(mUserSession.getG_Email());
            mPassword.setText(mUserSession.getG_Password());
            isPasswordCheck.setChecked(mUserSession.getG_WantToStoreIt().equals("1"));
            Toast.makeText(AsGuestFrom.this, "Saved From Session", Toast.LENGTH_SHORT).show();
        }

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mFirstName.getText().toString().isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your First Name", Toast.LENGTH_SHORT).show();
                }else if(mLastName.getText().toString().isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your Last Name", Toast.LENGTH_SHORT).show();
                }else if(mMobile.getText().toString().isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your Mobile Number", Toast.LENGTH_SHORT).show();
                }else if(mShippingHouseNo.getText().toString().isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your House Number", Toast.LENGTH_SHORT).show();
                }else if(mShippingAddress.getText().toString().isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your Address", Toast.LENGTH_SHORT).show();
                }else if(mShippingCity.getText().toString().isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your City", Toast.LENGTH_SHORT).show();
                }else if(state_id.isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your State", Toast.LENGTH_SHORT).show();
                }else if(mShippingCountry.getText().toString().isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your Country", Toast.LENGTH_SHORT).show();
                }else if(state_id.isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your State", Toast.LENGTH_SHORT).show();
                }else if(mEmail.getText().toString().isEmpty()){
                    Toast.makeText(AsGuestFrom.this, "Please Enter Your Email", Toast.LENGTH_SHORT).show();
                }else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(AsGuestFrom.this, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
                }else if (SaveDetails.equals("1")){
                    if(mPassword.getText().toString().isEmpty()){
                        Toast.makeText(AsGuestFrom.this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                    }else {
                        LoginAsGuest();
                    }
                }else {
                    LoginAsGuest();
                }

            }
        });

        getCities();

    }



    private void getCities() {
        final KProgressHUD progressDialog = KProgressHUD.create(AsGuestFrom.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-state",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){

                                        JSONObject object = jsonArray.getJSONObject(i);
                                        CityModel BatchModel = new CityModel();
                                        BatchModel.setCityname(object.getString("StateName"));
                                        BatchModel.setCityId(object.getString("StateID"));
                                        mDataState.add(BatchModel);

                                    }

                                    CityModel BatchModel = new CityModel();
                                    BatchModel.setCityId("");
                                    BatchModel.setCityname("Please select State");
                                    mDataState.add(BatchModel);
                                    SelectCitySpinner1 adapter = new SelectCitySpinner1(AsGuestFrom.this,
                                            android.R.layout.simple_spinner_item,
                                            mDataState);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                                    mSpinnerCity.setAdapter(adapter);
                                    mSpinnerCity.setSelection(adapter.getCount());



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }else {
                                Toast.makeText(AsGuestFrom.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(AsGuestFrom.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(AsGuestFrom.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(AsGuestFrom.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(AsGuestFrom.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            mUserSession.logout();
                            Toast.makeText(AsGuestFrom.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AsGuestFrom.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //   params.put("FirstName", firstName.getText().toString());


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }


    private void LoginAsGuest() {
        Log.e("Document",mEmail.getText().toString()+ "--"+
                mLastName.getText().toString()+"--"+
                mMobile.getText().toString()+"--"+
                mShippingHouseNo.getText().toString()+"--"+
                mShippingAddress.getText().toString()+"--"+
                mShippingCity.getText().toString()+"--"+
                mShippingCountry.getText().toString()+"--"+
                mShippingPinCode.getText().toString()+"--"+
                mEmail.getText().toString()+"--"+
                SaveDetails+"--"+
                state_id+"--"+
                mPassword.getText().toString());
        final KProgressHUD progressDialog = KProgressHUD.create(AsGuestFrom.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "check-out-as-guest",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + " --");
                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                mUserSession.createGuestSession(mFirstName.getText().toString(),
                                        mLastName.getText().toString(),
                                        mMobile.getText().toString(),
                                        mShippingHouseNo.getText().toString(),
                                        mShippingAddress.getText().toString(),
                                        mShippingCity.getText().toString(),
                                        mShippingCountry.getText().toString(),
                                        mShippingPinCode.getText().toString(),
                                        mEmail.getText().toString(),
                                        SaveDetails,
                                        mPassword.getText().toString());

                                mUserSession.createRegistrationSession(
                                        jsonObject.getJSONObject("data").getString("UserID"),
                                        jsonObject.getJSONObject("data").getString("InterestedIn"),
                                        jsonObject.getJSONObject("data").getString("ProfilePic"),
                                        jsonObject.getJSONObject("data").getString("FirstName"),
                                        jsonObject.getJSONObject("data").getString("LastName"),
                                        jsonObject.getJSONObject("data").getString("Email"),
                                        jsonObject.getJSONObject("data").getString("MobileNo"),
                                        jsonObject.getJSONObject("data").getString("GoogleID"),
                                        jsonObject.getJSONObject("data").getString("FacebookID"),
                                        "",
                                        jsonObject.getJSONObject("data").getString("TwitterID"),
                                        jsonObject.getJSONObject("data").getString("Address"),
                                        jsonObject.getJSONObject("data").getString("City"),
                                        jsonObject.getJSONObject("data").getString("State"),
                                        jsonObject.getJSONObject("data").getString("Country"),
                                        jsonObject.getJSONObject("data").getString("Pincode"),
                                        jsonObject.getJSONObject("data").getString("DateOfBirth"),
                                        jsonObject.getJSONObject("data").getString("IsEnablePushNotification"),
                                        jsonObject.getJSONObject("data").getString("OTP"),
                                        jsonObject.getJSONObject("data").getString("stripe_customer_id"),
                                        jsonObject.getJSONObject("data").getString("DeviceToken"),
                                        jsonObject.getJSONObject("data").getString("DeviceType"),
                                        jsonObject.getJSONObject("data").getString("APIToken"));
                                startActivity(new Intent(AsGuestFrom.this, PlaceOrder.class));
                                finish();
                              ///  Toast.makeText(AsGuestFrom.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }else if (jsonObject.getString("ResponseCode").equals("422")) {
                                finish();
                                Toast.makeText(AsGuestFrom.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(AsGuestFrom.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(AsGuestFrom.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(AsGuestFrom.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(AsGuestFrom.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(AsGuestFrom.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            mUserSession.logout();
                            Toast.makeText(AsGuestFrom.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AsGuestFrom.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> params = new HashMap<>();
                params.put("FirstName", mFirstName.getText().toString());
                params.put("LastName", mLastName.getText().toString());
                params.put("MobileNumber", mMobile.getText().toString());
                params.put("ShippingHouseNo", mShippingHouseNo.getText().toString());
                params.put("ShippingAddress", mShippingAddress.getText().toString());
                params.put("ShippingCity", mShippingCity.getText().toString());
                params.put("ShippingCountry", mShippingCountry.getText().toString());
                params.put("ShippingPinCode", mShippingPinCode.getText().toString());
                params.put("Email", mEmail.getText().toString());
                params.put("WantToStoreIt", SaveDetails);
                params.put("ShippingState", state_id);
                params.put("Password", mPassword.getText().toString());





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
              /*  long imagename = System.currentTimeMillis();
                params.put("profile_image", new DataPart(imagename + ".png", getFileDataFromDrawable(profile_bitmap)));*/

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }

}