package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Model.CityModel;
import com.fashion.fashionhouse.Model.SelectCitySpinner1;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.UserSession;
import com.fashion.fashionhouse.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class EditAddress extends AppCompatActivity {

    private EditText firstName, lastName, mobileNumber, houseNumber, shippingAddress, city, postCode;

    private UserSession session;
    private RequestQueue requestQueue;

    private ArrayList<CityModel> mDataState = new ArrayList<>();

    private Spinner citySpinner, spinnerCountry;

    private String state_id, countryName = "";

    private String shippingId;
    private String[] countryList = {"Nigeria"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(EditAddress.this);
        requestQueue = Volley.newRequestQueue(EditAddress.this);//Creating the RequestQueue

        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        mobileNumber = findViewById(R.id.mobileNumber);
        houseNumber = findViewById(R.id.houseNumber);
        shippingAddress = findViewById(R.id.shippingAddress);
        city = findViewById(R.id.city);
        postCode = findViewById(R.id.postCode);
        citySpinner = findViewById(R.id.citySpinner);
        spinnerCountry = findViewById(R.id.spinnerCountry);


        ArrayAdapter arrayAdapter = new ArrayAdapter(EditAddress.this, android.R.layout.simple_spinner_item, countryList);
        spinnerCountry.setAdapter(arrayAdapter);

        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                countryName = countryList[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        shippingId = getIntent().getStringExtra("idShippingAddress");


        SelectCitySpinner1 adapterState = new SelectCitySpinner1(EditAddress.this,
                android.R.layout.simple_spinner_item,
                mDataState);
        adapterState.setDropDownViewResource(android.R.layout.simple_spinner_item);
        citySpinner.setAdapter(adapterState);
        citySpinner.setSelection(adapterState.getCount());


        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != mDataState.size()-1){
                    try {
                        state_id = mDataState.get(position).getCityId();
                        Log.e("state_id", state_id);
                    }catch (Exception e){
                        //	GetStudnet("0","0");

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        findViewById(R.id.btnSubmitEditAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (firstName.getText().toString().isEmpty()){
                    Toast.makeText(EditAddress.this, "enter first name", Toast.LENGTH_SHORT).show();
                } else if (lastName.getText().toString().isEmpty()){
                    Toast.makeText(EditAddress.this, "enter last name", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.getText().toString().isEmpty()){
                    Toast.makeText(EditAddress.this, "enter mobile number", Toast.LENGTH_SHORT).show();
                } else if (countryName.isEmpty()){
                    Toast.makeText(EditAddress.this, "enter country", Toast.LENGTH_SHORT).show();
                } else if (houseNumber.getText().toString().isEmpty()){
                    Toast.makeText(EditAddress.this, "enter house / flat name", Toast.LENGTH_SHORT).show();
                } else if (shippingAddress.getText().toString().isEmpty()){
                    Toast.makeText(EditAddress.this, "enter street name", Toast.LENGTH_SHORT).show();
                } else if (city.getText().toString().isEmpty()){
                    Toast.makeText(EditAddress.this, "enter city", Toast.LENGTH_SHORT).show();
                } else {
                    EditAddress(shippingId);
                }
            }
        });



        getAddress(shippingId);


    }


    private void getCities() {
        final KProgressHUD progressDialog = KProgressHUD.create(EditAddress.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, UserSession.BASEURL + "get-state",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                try {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0 ; i<jsonArray.length() ; i++){

                                        JSONObject object = jsonArray.getJSONObject(i);
                                        CityModel BatchModel = new CityModel();
                                        BatchModel.setCityname(object.getString("StateName"));
                                        BatchModel.setCityId(object.getString("StateID"));
                                        mDataState.add(BatchModel);

                                    }

                                    CityModel BatchModel = new CityModel();
                                    BatchModel.setCityId(state_id);
                                    for (int i = 0; i < mDataState.size(); i++) {
                                        if (mDataState.get(i).getCityId().equals(state_id)){
                                            BatchModel.setCityname(mDataState.get(i).getCityname());
                                        }
                                    }
                                    mDataState.add(BatchModel);
                                    SelectCitySpinner1 adapter = new SelectCitySpinner1(EditAddress.this,
                                            android.R.layout.simple_spinner_item,
                                            mDataState);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                                    citySpinner.setAdapter(adapter);
                                    citySpinner.setSelection(adapter.getCount());






                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }else {
                                Toast.makeText(EditAddress.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(EditAddress.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(EditAddress.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(EditAddress.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(EditAddress.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(EditAddress.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EditAddress.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //   params.put("FirstName", firstName.getText().toString());


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void getAddress(String shippingId) {
        final KProgressHUD progressDialog = KProgressHUD.create(EditAddress.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                UserSession.BASEURL + "get-shipping-address?ShippingAddressID=" + shippingId,
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                JSONObject object = jsonObject.getJSONObject("data");

                                firstName.setText(object.getString("FirstName"));
                                lastName.setText(object.getString("LastName"));
                                mobileNumber.setText(object.getString("MobileNumber"));
                                houseNumber.setText(object.getString("ShippingHouseNo"));
                                shippingAddress.setText(object.getString("ShippingAddress"));
                                city.setText(object.getString("ShippingCity"));
                           //     country.setText(object.getString("ShippingCountry"));
                                state_id = object.getString("ShippingState");
                                if (object.getString("ShippingPinCode").equals("null")){
                                    postCode.setText("");
                                } else {
                                    postCode.setText(object.getString("ShippingPinCode"));
                                }


                                getCities();




                            }else {
                                Toast.makeText(EditAddress.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(EditAddress.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(EditAddress.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(EditAddress.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(EditAddress.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(EditAddress.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EditAddress.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }



    private void EditAddress(String shippingId) {
        final KProgressHUD progressDialog = KProgressHUD.create(EditAddress.this)
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, UserSession.BASEURL + "edit-shipping-address",
                new Response.Listener<NetworkResponse>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")) {


                                finish();
                                Toast.makeText(EditAddress.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }else {
                                Toast.makeText(EditAddress.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(EditAddress.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        JSONObject data = null;

                        if (error instanceof ServerError)
                            Toast.makeText(EditAddress.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(EditAddress.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(EditAddress.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                        else if (error.networkResponse.statusCode == 401){
                            session.logout();
                            Toast.makeText(EditAddress.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EditAddress.this, MainActivity.class);
                            intent.putExtra("isUnAuthenticate", "1");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("ShippingAddressID", shippingId);
                params.put("FirstName", firstName.getText().toString());
                params.put("LastName", lastName.getText().toString());
                params.put("MobileNumber", mobileNumber.getText().toString());
                params.put("ShippingHouseNo", houseNumber.getText().toString());
                params.put("ShippingAddress", shippingAddress.getText().toString());
                params.put("ShippingCity", city.getText().toString());
                params.put("ShippingCountry", countryName);
                params.put("ShippingPinCode", postCode.getText().toString());
                params.put("ShippingState", state_id);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + session.getKeyApitoken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(volleyMultipartRequest);

    }





}