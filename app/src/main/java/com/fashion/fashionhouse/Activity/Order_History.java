package com.fashion.fashionhouse.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fashion.fashionhouse.Adapter.CurrentProductAdapter;
import com.fashion.fashionhouse.Model.OrderModel;
import com.fashion.fashionhouse.Model.Product;
import com.fashion.fashionhouse.R;
import com.fashion.fashionhouse.Utils.EndlessRecyclerViewScrollListener;
import com.fashion.fashionhouse.Utils.GetCurrentOrderRequest;
import com.fashion.fashionhouse.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Order_History extends AppCompatActivity {


    private RecyclerView category_view;
    private CurrentProductAdapter mAdapter;
    private ArrayList<OrderModel> orderModels =  new ArrayList<>();

    private UserSession session;
    private RequestQueue requestQueue;


    private LinearLayoutManager linearLayout;
    private int IntPage = 1;
    private int last_size = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        session = new UserSession(Order_History.this);
        requestQueue = Volley.newRequestQueue(Order_History.this);//Creating the RequestQueue


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        category_view = findViewById(R.id.category_view);
        mAdapter = new CurrentProductAdapter(Order_History.this, orderModels, new CurrentProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item, View v) {

                mypopupWindow.showAsDropDown(v,0,0);

            }

            @Override
            public void onItemClickSUB(int position, int item) {

              /*  int Id = Integer.parseInt(orderModels.get(position).getProducts().get(item).getProductID());
                Intent intent = new Intent(Order_History.this, GeneralPracticeActivity.class);
                intent.putExtra("ProductId",Id);
                startActivity(intent);*/
            }
        });
        category_view.setHasFixedSize(true);
        linearLayout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        category_view.setLayoutManager(linearLayout);
        category_view.setAdapter(mAdapter);
        category_view.setNestedScrollingEnabled(false);

        category_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                IntPage = page;
                if (page!=last_size){
                    int FinalOAgeSIze = page + 1;
                    GetAddress("get-orders",FinalOAgeSIze);
                }
            }
        });


        GetAddress("get-orders", IntPage);

        setPopUpWindow();


    }

    PopupWindow mypopupWindow;



    private void setPopUpWindow() {
        LayoutInflater inflater = (LayoutInflater)
                getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.info_dialog, null);


        mypopupWindow = new PopupWindow(view, 800, 600, true);

    }

    private void GetAddress(String MethodName,int page)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Order_History.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetCurrentOrderRequest loginRequest = new GetCurrentOrderRequest(MethodName,page,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    last_size = jsonObject1.getInt("last_page");

                    JSONArray object = jsonObject1.getJSONArray("data");

                    for (int i= 0 ; i < object.length();i++){

                        JSONObject object1 = object.getJSONObject(i);
                        OrderModel orderModel  = new OrderModel();

                        orderModel.setUserOrderID(String.valueOf(object1.getInt("UserOrderID")));
                        orderModel.setOrderCode(object1.getString("OrderCode"));
                        orderModel.setOrderAmount(String.valueOf(object1.getInt("OrderAmount")));
                        orderModel.setTax(String.valueOf(object1.getInt("Tax")));
                        orderModel.setDiscount(String.valueOf(object1.getInt("Discount")));
                        orderModel.setShippingCharge(String.valueOf(object1.getInt("ShippingCharge")));
                        orderModel.setTotalAmount(String.valueOf(object1.getInt("TotalAmount")));
                        orderModel.setShippingCharge(object1.getString("ShippingCharge"));
                        orderModel.setOrderStatus(object1.getString("OrderStatus"));
                        orderModel.setDate(object1.getString("date"));

                        ArrayList<Product> ProductModels = new ArrayList<>();

                        JSONArray products = object.getJSONObject(i).getJSONArray("products");

                        for (int j= 0 ; j < products.length();j++){
                            JSONObject products_details = products.getJSONObject(j);

                            Product product  = new Product();
                            product.setProductID(String.valueOf(products_details.getInt("ProductID")));
                            product.setProductTitleEn(products_details.getString("ProductTitle"));
                            product.setProductCode(products_details.getString("ProductCode"));
                            product.setPrice(String.valueOf(products_details.getInt("Price")));
                            product.setProductImage(products_details.getString("ProductImage"));
                            product.setQty(products_details.getString("Qty"));
                            ProductModels.add(product);
                        }

                        orderModel.setProducts(ProductModels);
                        orderModels.add(orderModel);

                    }

                    mAdapter.notifyDataSetChanged();
                    //  Toast.makeText(ManageOrderActivity.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Order_History.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();

                if (error instanceof ServerError)
                    Toast.makeText(Order_History.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Order_History.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Order_History.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                else if (error.networkResponse.statusCode == 401){
                    session.logout();
                    Toast.makeText(Order_History.this, getResources().getString(R.string.unAuthenticate), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Order_History.this, MainActivity.class);
                    intent.putExtra("isUnAuthenticate", "1");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ session.getKeyApitoken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);

        requestQueue.add(loginRequest);

    }




}