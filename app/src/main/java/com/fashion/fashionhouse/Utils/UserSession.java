package com.fashion.fashionhouse.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

  //  public static String BASEURL = "http://peperempe.uk/api/";
    public static String BASEURL = "https://fashionhouse.ng/api/";
    private static final String PREF_NAME = "fashionhouse_v_1.13";

    private static final String IS_LOGIN = "IsLogin";
    private static final String FIREBASE_DEVICE_TOKEN = "firebase_device_token";


    private static final String KEY_USERID = "KEY_USERID";
    private static final String KEY_INTERESTED_IN = "KEY_INTERESTED_IN";
    private static final String KEY_PROFILEPIC = "KEY_PROFILEPIC";
    private static final String KEY_FIRST_NAME = "FIRST_NAME";
    private static final String KEY_LAST_NAME = "KEY_LAST_NAME";
    private static final String KEY_EMAIL = "KEY_EMAIL";
    private static final String KEY_MOBILE_NO = "KEY_MOBILE_NO";
    private static final String KEY_GOOGLE_ID = "KEY_GOOGLE_ID";
    private static final String KEY_FACEBOOK_ID = "KEY_FACEBOOK_ID";
    private static final String KEY_APPLE_ID = "KEY_APPLE_ID";
    private static final String KEY_TWITTER_ID = "KEY_TWITTER_ID";
    private static final String KEY_ADDRESS = "KEY_ADDRESS";
    private static final String KEY_CITY = "KEY_CITY";
    private static final String KEY_STATE = "KEY_STATE";
    private static final String KEY_COUNTRY = "KEY_COUNTRY";
    private static final String KEY_PINCODE = "KEY_PINCODE";
    private static final String KEY_DATE_OF_BIRTH = "KEY_DATE_OF_BIRTH";
    private static final String KEY_ENABLE_PUSH_NOTIFICATION = "KEY_ENABLE_PUSH_NOTIFICATION";
    private static final String KEY_OTP = "KEY_OTP";
    private static final String KEY_STRIPE_CUSTOMER_ID = "KEY_STRIPE_CUSTOMER_ID";
    private static final String KEY_DEVICETOKEN = "KEY_DEVICETOKEN";
    private static final String KEY_DEVICETYPE = "KEY_DEVICETYPE";
    private static final String KEY_APITOKEN = "KEY_APITOKEN";



    private static final String G_GUEST_LOGIN = "G_GUEST_LOGIN";
    private static final String G_FirstName = "G_FirstName";
    private static final String G_LastName = "G_LastName";
    private static final String G_MobileNumber = "G_MobileNumber";
    private static final String G_ShippingHouseNo = "G_ShippingHouseNo";
    private static final String G_ShippingAddress = "G_ShippingAddress";
    private static final String G_ShippingCity = "G_ShippingCity";
    private static final String G_ShippingCountry = "G_ShippingCountry";
    private static final String G_ShippingPinCode = "G_ShippingPinCode";
    private static final String G_Email = "G_Email";
    private static final String G_WantToStoreIt = "G_WantToStoreIt";
    private static final String G_Password = "G_Password";


    private static final String IS_MENWOMEN = "ISMENWOMEN";





    private String firbaseDeviceToken;


    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }


    public String getIsMenwomen() {
        return sharedPreferences.getString(IS_MENWOMEN, "0");
    }

    public void setIsMenwomen(String stripeCustomerId) {
        editor.putString(IS_MENWOMEN, stripeCustomerId);
        editor.commit();
    }



    public void setFirbaseDeviceToken(String firbaseDeviceToken) {
        editor.putString(FIREBASE_DEVICE_TOKEN, firbaseDeviceToken);
        editor.commit();
    }


    public String getFirbaseDeviceToken() {
        return sharedPreferences.getString(FIREBASE_DEVICE_TOKEN, "");
    }


    public void setKeyInterestedIn(String interestedIn) {
        editor.putString(KEY_INTERESTED_IN, interestedIn);
        editor.commit();
    }

    public String getKeyInterestedIn() {
        return sharedPreferences.getString(KEY_INTERESTED_IN, "");
    }

    public void setKeyProfilepic(String profilepic) {
        editor.putString(KEY_PROFILEPIC, profilepic);
        editor.commit();
    }

    public String getKeyProfilepic() {
        return sharedPreferences.getString(KEY_PROFILEPIC, "");
    }

    public void setKeyFirstName(String firstName) {
        editor.putString(KEY_FIRST_NAME, firstName);
        editor.commit();
    }

    public String getKeyFirstName() {
        return sharedPreferences.getString(KEY_FIRST_NAME, "");
    }


    public void setKeyLastName(String lastName) {
        editor.putString(KEY_LAST_NAME, lastName);
        editor.commit();
    }

    public String getKeyLastName() {
        return sharedPreferences.getString(KEY_LAST_NAME, "");
    }

    public void setKeyEmail(String keyEmail) {
        editor.putString(KEY_EMAIL, keyEmail);
        editor.commit();
    }

    public String getKeyEmail() {
        return sharedPreferences.getString(KEY_EMAIL, "");
    }

    public void setKeyMobileNo(String mobileNo) {
        editor.putString(KEY_MOBILE_NO, mobileNo);
        editor.commit();
    }

    public String getKeyMobileNo() {
        return sharedPreferences.getString(KEY_MOBILE_NO, "");
    }

   public void setKeyGoogleId(String googleId) {
        editor.putString(KEY_GOOGLE_ID, googleId);
        editor.commit();
    }

    public String getKeyGoogleId() {
        return sharedPreferences.getString(KEY_GOOGLE_ID, "");
    }

   public void setKeyFacebookId(String facebookId) {
        editor.putString(KEY_FACEBOOK_ID, facebookId);
        editor.commit();
    }

    public String getKeyFacebookId() {
        return sharedPreferences.getString(KEY_FACEBOOK_ID, "");
    }

    public void setKeyAppleId(String appleId) {
        editor.putString(KEY_APPLE_ID, appleId);
        editor.commit();
    }

    public String getKeyAppleId() {
        return sharedPreferences.getString(KEY_APPLE_ID, "");
    }

    public void setKeyTwitterId(String appleId) {
        editor.putString(KEY_TWITTER_ID, appleId);
        editor.commit();
    }

    public String getKeyTwitterId() {
        return sharedPreferences.getString(KEY_TWITTER_ID, "");
    }

  public void setKeyAddress(String address) {
        editor.putString(KEY_ADDRESS, address);
        editor.commit();
    }

    public String getKeyAddress() {
        return sharedPreferences.getString(KEY_ADDRESS, "");
    }


    public void setKeyCity(String city) {
        editor.putString(KEY_CITY, city);
        editor.commit();
    }

    public String getKeyCity() {
        return sharedPreferences.getString(KEY_CITY, "");
    }

    public void setKeyState(String state) {
        editor.putString(KEY_STATE, state);
        editor.commit();
    }

    public String getKeyState() {
        return sharedPreferences.getString(KEY_STATE, "");
    }

  public void setKeyCountry(String country) {
        editor.putString(KEY_COUNTRY, country);
        editor.commit();
    }

    public String getKeyCountry() {
        return sharedPreferences.getString(KEY_COUNTRY, "");
    }

public void setKeyPincode(String pincode) {
        editor.putString(KEY_PINCODE, pincode);
        editor.commit();
    }

    public String getKeyPincode() {
        return sharedPreferences.getString(KEY_PINCODE, "");
    }


    public void setKeyDateOfBirth(String dateOfBirth) {
        editor.putString(KEY_DATE_OF_BIRTH, dateOfBirth);
        editor.commit();
    }

    public String getKeyDateOfBirth() {
        return sharedPreferences.getString(KEY_DATE_OF_BIRTH, "");
    }


    public void setKeyEnablePushNotification(String notification) {
        editor.putString(KEY_ENABLE_PUSH_NOTIFICATION, notification);
        editor.commit();
    }

    public String getKeyEnablePushNotification() {
        return sharedPreferences.getString(KEY_ENABLE_PUSH_NOTIFICATION, "");
    }

    public void setKeyOtp(String otp) {
        editor.putString(KEY_OTP, otp);
        editor.commit();
    }

    public String getKeyOtp() {
        return sharedPreferences.getString(KEY_OTP, "");
    }

    public void setKeyStripeCustomerId(String stripeCustomerId) {
        editor.putString(KEY_STRIPE_CUSTOMER_ID, stripeCustomerId);
        editor.commit();
    }

    public String getKeyStripeCustomerId() {
        return sharedPreferences.getString(KEY_STRIPE_CUSTOMER_ID, "");
    }

    public void setKeyDevicetoken(String devicetoken) {
        editor.putString(KEY_DEVICETOKEN, devicetoken);
        editor.commit();
    }

    public String getKeyDevicetoken() {
        return sharedPreferences.getString(KEY_DEVICETOKEN, "");
    }

    public void setKeyDevicetype(String devicetype) {
        editor.putString(KEY_DEVICETYPE, devicetype);
        editor.commit();
    }

    public String getKeyDevicetype() {
        return sharedPreferences.getString(KEY_DEVICETYPE, "");
    }


    public void setKeyApitoken(String KEY_APITOKEN) {
        editor.putString(KEY_APITOKEN, KEY_APITOKEN);
        editor.commit();
    }

    public String getKeyApitoken() {
        return sharedPreferences.getString(KEY_APITOKEN, "");
    }




    public void createRegistrationSession(String UserID,
                                   String InterestedIn,
                                   String ProfilePic,
                                   String FirstName,
                                   String LastName,
                                   String Email,
                                   String MobileNo,
                                   String GoogleID,
                                   String FacebookID,
                                   String AppleID,
                                   String TwitterID,
                                   String Address,
                                   String City,
                                   String State,
                                   String Country,
                                   String Pincode,
                                   String DateOfBirth,
                                   String IsEnablePushNotification,
                                   String OTP,
                                   String stripe_customer_id,
                                   String DeviceToken,
                                   String DeviceType,
                                   String APIToken
    ) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing name in pref
        editor.putString(KEY_USERID, UserID);
        editor.putString(KEY_INTERESTED_IN, InterestedIn);
        editor.putString(KEY_PROFILEPIC, ProfilePic);
        editor.putString(KEY_FIRST_NAME, FirstName);
        editor.putString(KEY_LAST_NAME, LastName);
        editor.putString(KEY_EMAIL, Email);
        editor.putString(KEY_MOBILE_NO, MobileNo);
        editor.putString(KEY_GOOGLE_ID, GoogleID);
        editor.putString(KEY_FACEBOOK_ID, FacebookID);
        editor.putString(KEY_APPLE_ID, AppleID);
        editor.putString(KEY_TWITTER_ID, TwitterID);
        editor.putString(KEY_ADDRESS, Address);
        editor.putString(KEY_CITY, City);
        editor.putString(KEY_STATE, State);
        editor.putString(KEY_COUNTRY, Country);
        editor.putString(KEY_PINCODE, Pincode);
        editor.putString(KEY_DATE_OF_BIRTH, DateOfBirth);
        editor.putString(KEY_ENABLE_PUSH_NOTIFICATION, IsEnablePushNotification);
        editor.putString(KEY_OTP, OTP);
        editor.putString(KEY_STRIPE_CUSTOMER_ID, stripe_customer_id);
        editor.putString(KEY_DEVICETOKEN, DeviceToken);
        editor.putString(KEY_DEVICETYPE, DeviceType);
        editor.putString(KEY_APITOKEN, APIToken);


        editor.commit();
    }



    public String getKeyUserid() {
        return sharedPreferences.getString(KEY_USERID, "");
    }



    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }


    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }


    public void createGuestSession(String FirstName,
                                          String LastName,
                                          String MobileNumber,
                                          String ShippingHouseNo,
                                          String ShippingAddress,
                                          String ShippingCity,
                                          String ShippingCountry,
                                          String ShippingPinCode,
                                          String Email,
                                          String WantToStoreIt,
                                          String Password

    ) {
        // Storing login value as TRUE
        editor.putBoolean(G_GUEST_LOGIN, true);
        // Storing name in pref
        editor.putString(G_FirstName, FirstName);
        editor.putString(G_LastName, LastName);
        editor.putString(G_MobileNumber, MobileNumber);
        editor.putString(G_ShippingHouseNo, ShippingHouseNo);
        editor.putString(G_ShippingAddress, ShippingAddress);
        editor.putString(G_ShippingCity, ShippingCity);
        editor.putString(G_ShippingCountry, ShippingCountry);
        editor.putString(G_ShippingPinCode, ShippingPinCode);
        editor.putString(G_Email, Email);
        editor.putString(G_WantToStoreIt, WantToStoreIt);
        editor.putString(G_Password, Password);
        editor.commit();
    }

    public boolean getG_GuestLogin() {
        return sharedPreferences.getBoolean(G_GUEST_LOGIN, false);
    }
    public String getG_FirstName() {
        return sharedPreferences.getString(G_FirstName, "");
    }
    public String getG_LastName() {
        return sharedPreferences.getString(G_LastName, "");
    }
    public String getG_MobileNumber() {
        return sharedPreferences.getString(G_MobileNumber, "");
    }
    public String getG_ShippingHouseNo() { return sharedPreferences.getString(G_ShippingHouseNo, "");
    }
    public String getG_ShippingAddress() {
        return sharedPreferences.getString(G_ShippingAddress, "");
    }
    public String getG_ShippingCity() {
        return sharedPreferences.getString(G_ShippingCity, "");
    }
    public String getG_ShippingCountry() {
        return sharedPreferences.getString(G_ShippingCountry, "");
    }
    public String getG_ShippingPinCode() {
        return sharedPreferences.getString(G_ShippingPinCode, "");
    }
    public String getG_Email() {
        return sharedPreferences.getString(G_Email, "");
    }
    public String getG_WantToStoreIt() {
        return sharedPreferences.getString(G_WantToStoreIt, "0");
    }
    public String getG_Password() {
        return sharedPreferences.getString(G_Password, "");
    }

}
