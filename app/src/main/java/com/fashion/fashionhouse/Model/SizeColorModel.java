package com.fashion.fashionhouse.Model;

public class SizeColorModel {

    String ProductVariantOptionID;
    String ProductVariantOption;
    String Qyt;

    public String getQyt() {
        return Qyt;
    }

    public void setQyt(String qyt) {
        Qyt = qyt;
    }

    public String getProductVariantOptionID() {
        return ProductVariantOptionID;
    }

    public void setProductVariantOptionID(String productVariantOptionID) {
        ProductVariantOptionID = productVariantOptionID;
    }

    public String getProductVariantOption() {
        return ProductVariantOption;
    }

    public void setProductVariantOption(String productVariantOption) {
        ProductVariantOption = productVariantOption;
    }
}
