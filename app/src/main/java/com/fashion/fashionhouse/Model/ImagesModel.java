package com.fashion.fashionhouse.Model;

import java.util.ArrayList;

public class ImagesModel {

    String ProductImageID;
    String ProductID;
    String ProductImageName;
    String IsDefaultProductImage;


    public String getIsDefaultProductImage() {
        return IsDefaultProductImage;
    }

    public void setIsDefaultProductImage(String isDefaultProductImage) {
        IsDefaultProductImage = isDefaultProductImage;
    }

    public String getProductImageID() {
        return ProductImageID;
    }

    public void setProductImageID(String productImageID) {
        ProductImageID = productImageID;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getProductImageName() {
        return ProductImageName;
    }

    public void setProductImageName(String productImageName) {
        ProductImageName = productImageName;
    }
}
