package com.fashion.fashionhouse.Model;

public class ShippingAddressModel {

    private String ShippingAddressID;
    private String MobileNumber;
    private String ShippingHouseNo;
    private String ShippingAddress;
    private String ShippingCity;
    private String ShippingCountry;
    private String ShippingPinCode;
    private String IsDefault;
    private String FirstName;
    private String LastName;
    private String shippingState;


    public String getShippingAddressID() {
        return ShippingAddressID;
    }

    public void setShippingAddressID(String shippingAddressID) {
        ShippingAddressID = shippingAddressID;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getShippingHouseNo() {
        return ShippingHouseNo;
    }

    public void setShippingHouseNo(String shippingHouseNo) {
        ShippingHouseNo = shippingHouseNo;
    }

    public String getShippingAddress() {
        return ShippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        ShippingAddress = shippingAddress;
    }

    public String getShippingCity() {
        return ShippingCity;
    }

    public void setShippingCity(String shippingCity) {
        ShippingCity = shippingCity;
    }

    public String getShippingCountry() {
        return ShippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        ShippingCountry = shippingCountry;
    }

    public String getShippingPinCode() {
        return ShippingPinCode;
    }

    public void setShippingPinCode(String shippingPinCode) {
        ShippingPinCode = shippingPinCode;
    }


    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getIsDefault() {
        return IsDefault;
    }

    public void setIsDefault(String isDefault) {
        IsDefault = isDefault;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public String getShippingState() {
        return shippingState;
    }
}
