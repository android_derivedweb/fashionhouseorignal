package com.fashion.fashionhouse.Model;

public class InterestedModel {

    private String CategoryID;
    private String CategoryImage;
    private String CategoryName;
    private String IsBrands;

    public String getIsBrands() {
        return IsBrands;
    }

    public void setIsBrands(String isBrands) {
        IsBrands = isBrands;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryImage() {
        return CategoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        CategoryImage = categoryImage;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

}
